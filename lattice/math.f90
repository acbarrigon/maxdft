module math_m
  implicit none

  private
  public ::     &
    kdelta,     &
    choose,     &
    eigensolver

  real(8), parameter, public :: &
    M_PI = 4.0_8 * atan(1.0_8)

  contains

  function kdelta(i, j) result(res)
    integer, intent(in) :: i, j
    integer :: res
    res = 0
    if(i == j) res = 1
  end function kdelta

  ! Binomial coefficient (n over k)
  recursive function choose(n, k) result(res)
    real (8) :: res
    integer, intent(in) :: n, k
    if (k == n) then
      res = 1.0_8
    else if (k == 1) then
      res = real(n, 8)
    else if ((k /= 1) .and. (k /= n)) then
      res = choose(n-1,k-1) + choose(n-1,k)
    end if      
  end function

  subroutine eigensolver( hdim , h0 , eval , evec )
    integer, intent (in) :: hdim
    complex(8), intent (in) :: h0(1:hdim, 1:hdim)
    real(8), intent (out) :: eval(1:hdim)
    complex(8), intent (out) :: evec(1:hdim, 1:hdim)

    integer :: info
    complex(8), allocatable :: wrk(:)
    real(8), allocatable :: rwrk(:)
    integer :: lwrk

    evec = h0
    eval = 0.0_8
    lwrk = 2*hdim-1
    allocate(wrk(lwrk))
    allocate(rwrk(3*hdim-2))
    call zheev( 'V', 'U', hdim, evec, hdim, eval, wrk, lwrk, rwrk, info)

    deallocate(rwrk)
    deallocate(wrk)
  end subroutine eigensolver

end module math_m
