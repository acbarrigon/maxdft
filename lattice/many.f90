!> This module contains routines to solve Schrödinger's equation
!! for the many-electron problem.
module many_m
  use basis_m
  use input_m
  use grid_m
  use operator_m
  use math_m

  implicit none

  private
  public ::         &
    many_run,       &
    many_solve,     &
    density_init,   &
    compute_density


  contains


  !> Solves Schrödinger's equation; it reads the external potential and the
  !! electron-electron interaction from the input file.
  subroutine many_run()
    type(operator_t) :: kin, v, w
    real(8), allocatable :: vvalues(:), gamma(:)
    real(8) :: energy
    real(8), allocatable :: density(:)

    call operator_init_kinetic(kin)
    call operator_write_info(kin, 'kinetic')

    allocate(vvalues(grid%p))
    allocate(density(grid%p))
    allocate(gamma(0:grid%p-1))

    vvalues = 0.0_8; call read_variable('extpot', avar = vvalues)
    call operator_init_extpot(v, vvalues)

    gamma = 0.0_8; call read_variable('gamma', avar = gamma(0:grid%p-1))
    call operator_init_w(w, gamma)

    call many_solve(kin, v, w, energy, density)
    write(*, *) 'Energy = ', energy

    deallocate(vvalues)
    deallocate(density)
    deallocate(gamma)
  end subroutine many_run


  subroutine density_init(npoints, nsteps, dn, n0)
    integer, intent(in) :: npoints, nsteps
    real(8), intent(in) :: dn
    real(8), intent(inout) :: n0(1:nsteps,1:basis%p)

    integer :: i, j, k


    select case(basis%p)

    case(3)
      i = 1
      j = 0
      do while (i .le. nsteps)
        j = j+1
        do k = j, npoints
          n0(i, 1) = dn * (j-1)
          n0(i, 2) = basis%N - dn * (k-1)
          n0(i, 3) = basis%N - sum(n0(i,1:2))
          i = i+1
        end do
      end do
    case(2)
      do i = 1, npoints
        n0(i, 1) = dn * (i-1)
        n0(i, 2) = basis%N - n0(i,1)
      end do
    end select

  end subroutine density_init

  ! Computes density(m) = <st|\hat{n}_m|st>
  function compute_density(st) result(density)
    complex(8), intent(in) :: st(:)
    real(8) :: density(1:basis%p)

    integer :: m, ii

    do m = 1, basis%p
      density(m) = 0.0_8
      do ii = 1, basis%dim
        density(m) = density(m) + abs(st(ii))**2 * basis%nm(ii, m)
      end do
    end do

  end function compute_density




  subroutine many_solve(kin, v, w, energy, density, zpsi)
    type(operator_t), intent(in) :: kin, v, w
    real(8), intent(inout) :: energy
    real(8), intent(inout) :: density(:)
    complex(8), optional, intent(out) :: zpsi(:)

    integer :: dim
    complex(8), allocatable :: ham(:, :), eigenvectors(:, :)
    real(8), allocatable :: eigenvalues(:)

    dim = basis%dim

    allocate(ham(1:dim, 1:dim))
    allocate(eigenvectors(1:dim, 1:dim))
    allocate(eigenvalues(1:dim))


    ham = kin%mat2 + v%mat2 + w%mat2
    eigenvectors = (0.0_8, 0.0_8)
    eigenvalues = 0.0_8

    call eigensolver( dim, ham , eigenvalues , eigenvectors )

    energy = eigenvalues(1)
    density = compute_density(eigenvectors(:, 1))

    if(present(zpsi)) then
      zpsi = eigenvectors(:, 1)
    end if
!!$    write(*, *) 
!!$    write(*, *) 'Energies :'
!!$    do ii = 1, dim
!!$      write(*, *) ii, eigenvalues(ii)
!!$    end do
!!$
!!$    write(*, *) 
!!$    write(*, *) 'Densities of the first excited state:'
!!$    write(*, *) compute_density(basis, eigenvectors(:, 1))

  end subroutine many_solve


end module many_m
