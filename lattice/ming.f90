module ming_m
  use input_m
  use grid_m
  use operator_m
  use xc_m
  use gfunction_m
  use basis_m
  use minimizeg_m
  use potential_m
  
  implicit none

  private
  public :: &
    ming_run

  contains

  subroutine ming_run

  integer :: npots, k, ggpoints, runmode
  type(operator_t) :: kin, w
  type(operator_t), allocatable :: v(:)
  type(xc_t) :: xc
  real(8) :: val, t, minimum
  real(8), allocatable :: gamma(:), vvalues(:), lower_bounds(:), upper_bounds(:)!, x(:)
  integer, parameter ::        &
    RUNMODE_MINIMIZEG     = 0, &
    RUNMODE_GRADIENTCHECK = 1
  include 'nlopt.f'

  t = 0.5_8; call read_variable('t', rvar = t)

  runmode = RUNMODE_GRADIENTCHECK

  call operator_init_kinetic(kin)
  call operator_write_info(kin, 'kinetic')

  allocate(gamma(0:grid%p-1))
  gamma = 0.0_8
  gamma(0) = 1.5_8

  call operator_init_w(w, gamma)
  ! This would set the xc functional to EXX
  ! call xc_init(xc, gamma = gamma, mode = XC_EXX)
  ! And this is for the exact functional.
  call xc_init(xc, gamma = gamma, mode = XC_EXACT, kin = kin, w = w)
  call operator_end(w)

  npots = 50
  allocate(v(npots))
  allocate(vvalues(grid%p))
  do k = 1, npots
    call v_init(vvalues, k = 0.01_8*k)
    call operator_init_extpot(v(k), vvalues)
  end do

  select case(runmode)
  case(RUNMODE_MINIMIZEG)

    allocate(lower_bounds(grid%p-1))
    allocate(upper_bounds(grid%p-1))
    lower_bounds =  0.0_8
    upper_bounds =  3.0_8

    ggpoints = 51
    do k = 1, ggpoints
      gamma(0) = lower_bounds(1) + (k-1) * (upper_bounds(1) - lower_bounds(1)) / (ggpoints - 1)
      call operator_init_w(w, gamma)
      call gfunction(w, v, xc, kin, gamma, val)
      call operator_end(w)
      write(51, '(3f24.12)') gamma(0), val
    end do

    call minimizeg(kin, xc, v, lower_bounds, upper_bounds, gamma, minimum)

    write(*, *) 'minimum = ', minimum
    write(*, *) 'gamma0 = ', gamma

  case(RUNMODE_GRADIENTCHECK)

    call gradient_check(kin, xc, v, gamma)

  end select

  deallocate(vvalues)
  deallocate(gamma)
  end subroutine ming_run

end module ming_m
