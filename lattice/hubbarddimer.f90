!> This program solves the Hubbard dimer problem, replicating the results given
!! in Figs. 3, 4 and 5 of [D. J. Carrascal et al., J. Phys.: Condens. Matter 27, 393001 (2015)].
module hubbarddimer_m
  use input_m
  use grid_m
  use basis_m
  use operator_m
  use math_m
  use dft_m
  use many_m
  use xc_m

  implicit none

  private
  public :: &
    hubbarddimer_run

  contains

  subroutine hubbarddimer_run

  logical :: converged
  integer :: k, q
  real(8) :: energy, deltav, t, exactenergy, exactdeltan, cos3theta, theta, omega, c2, dftenergy
  real(8), allocatable :: vvalues(:), gamma(:), density(:), dftdensity(:), uvalues(:)
  type(operator_t) :: kin, v, w
  type(xc_t) :: xc


  t = 0.5_8; call read_variable('t', rvar = t)

  call operator_init_kinetic(kin)
  call operator_write_info(kin, 'kinetic')

  allocate(density(grid%p))
  allocate(dftdensity(grid%p))

  allocate(vvalues(grid%p))
  allocate(gamma(0:grid%p-1))

  allocate(uvalues(5))
  uvalues(1) = 0.2_8
  uvalues(2) = 1.0_8
  uvalues(3) = 2.0_8
  uvalues(4) = 5.0_8
  uvalues(5) = 10.0_8

  gamma = 0.0_8

  do q = 1, 5
    gamma(0) = uvalues(q)

    call operator_init_w(w, gamma)

    ! For exact functional, or for the EXX functional.
    !call xc_init(xc, gamma = gamma, mode = XC_EXX)
    call xc_init(xc, gamma = gamma, mode = XC_EXACT, kin = kin, w = w)

    do k = 1, 21
      deltav = (k-1)*1.0_8
      vvalues(1) = -deltav/2.0_8
      vvalues(2) = deltav/2.0_8

      ! These are the equations (A1-A6) in the paper.
      omega = sqrt(3.0_8*(1.0_8+deltav**2) + gamma(0)**2)
      cos3theta = ( 9.0_8*(deltav**2-0.5_8) - gamma(0)**2) * (gamma(0)/omega**3)
      theta = (1.0_8/3.0_8) * acos(cos3theta)
      exactenergy = (2.0_8/3.0_8) * ( gamma(0) - omega*sin(theta + M_PI/6.0_8) )

      c2 = 1.0_8 / ( 2.0_8 * (deltav**2 + (exactenergy - gamma(0))**2 * (1.0_8 + 1.0_8/exactenergy**2) ) )
      exactdeltan = - 8.0_8 * c2 * deltav * (exactenergy - gamma(0))

      call operator_init_extpot(v, vvalues)

      call many_solve(kin, v, w, energy, density)

      call dft_solve(basis%N, kin, v, gamma, xc, dftenergy, dftdensity, guess_density = density, converged = converged)

      write(*, *)
      write(*, '(a,g10.5,a,g5.0,a)') 'U = ', gamma(0), ' (q = ', q, ')'
      write(*, *) 'deltav = ', deltav, ' (k = ', k, ')'
      write(*, *) 'Energy (exact) = ', energy
      write(*, *) 'Energy (DFT) = ', dftenergy
      write(*, *) 'Error = ', maxval(abs(density-dftdensity))
      if(.not.converged) then
        write(*, *) 'WARNING: Not converged'
        !stop
      end if
      write(*, *)

      write(70+q, *) deltav, energy, exactenergy, dftenergy
      write(80+q, *) deltav, density(1)-density(2), exactdeltan

      call operator_end(v)
    end do

    call xc_end(xc)
    call operator_end(w)
  end do

  deallocate(density)
  deallocate(dftdensity)
  deallocate(vvalues)
  deallocate(uvalues)
  deallocate(gamma)
  end subroutine hubbarddimer_run

end module hubbarddimer_m
