module maxdft_m
  use input_m
  use grid_m
  use basis_m
  use operator_m
  use xc_m
  use gfunction_m
  use many_m
  use potential_m

  implicit none

  private
  public :: &
    maxdft_run

  contains

  subroutine maxdft_run

  integer :: nparams, noptpars, ires, npots, k, j, mu
  integer, allocatable :: optpars(:)
  integer(8) :: opt
  real(8) :: d0, g0, val, energy, deltav
  real(8), allocatable :: lambda(:), gammaref(:), vvalues(:), density(:), pars(:)
  real(8), allocatable :: lb(:), ub(:)
  type(operator_t), allocatable :: v(:)
  type(operator_t) :: kin, w
  type(xc_t) :: xcx, xc

  integer :: runmode
  integer, parameter ::        &
    RUNMODE_MINIMIZEG     = 0, &
    RUNMODE_MINIMIZED     = 1

  include 'nlopt.f'

  call operator_init_kinetic(kin)
  call operator_write_info(kin, 'kinetic')

  runmode = RUNMODE_MINIMIZEG

  select case(runmode)

  case(RUNMODE_MINIMIZEG)

    nparams = 5
    allocate(lambda(nparams))
    lambda = 0.0_8

    noptpars = 2
    allocate(optpars(noptpars))
    optpars(1) = 4
    optpars(2) = 5
    allocate(pars(noptpars))
    pars = 0.0_8

    allocate(gammaref(0:grid%p-1))
    gammaref = 0.0_8
    gammaref(0) = 1.0_8

    allocate(density(grid%p))

    call operator_init_w(w, gammaref)
    call xc_init(xcx, gamma = gammaref, mode = XC_EXACT, kin = kin, w = w)
    call xc_init(xc, lambda = lambda, mode = XC_GENERIC, parametrization = XC_PARAMS_TAYLOR_HD)
    call xc_write(xc, 'xc-initial-guess')

    npots = 40
    allocate(v(npots))
    allocate(vvalues(grid%p))
    open(unit = 21, file = 'densvsv')
    deltav = 0.01_8
    j = 1
    do k = 1, npots/2
      call v_init(vvalues, real(k-1,8))
      write(*, *) 'a', j
      call operator_init_extpot(v(j), vvalues)
      call many_solve(kin, v(j), w, energy, density)
      write(21, *) j, 2.0_8 * vvalues(2), density(1), density(2)

      j = j + 1

      call v_init(vvalues, real(1-k,8))
      write(*, *) 'b', j
      call operator_init_extpot(v(j), vvalues)
      write(*, *) 'c'
      call many_solve(kin, v(j), w, energy, density)
      write(21, *) j, 2.0_8 * vvalues(2), density(1), density(2)

      j = j + 1

    end do
    close(unit = 21)

    write(*, *)
    call gfunction(w, v, xcx, kin, gammaref, val)
    write(*, *) 'G (exact xc) = ', val
    call gfunction(w, v, xc, kin, gammaref, val)
    write(*, *) 'G (initial guess) = ', val
    write(*, *)

    call nlo_create(opt, NLOPT_LN_BOBYQA, noptpars)

    allocate(lb(noptpars))
    allocate(ub(noptpars))
    lb(:) = - 10.0_8
    ub(:) =   10.0_8
    call nlo_set_lower_bounds(ires, opt, lb)
    if(ires.ne.1) then
      write(*, *) 'NLOPT error at nlo_set_lower_bounds. code = ', ires
      stop
    end if
    call nlo_set_upper_bounds(ires, opt, ub)
    if(ires.ne.1) then
      write(*, *) 'NLOPT error at nlo_set_upper_bounds. code = ', ires
      stop
    end if
    call nlo_set_min_objective(ires, opt, gfunc, 0)
    if(ires.ne.1) then
      write(*, *) 'NLOPT error at nlo_set_min_objective. code = ', ires
      stop
    end if
    call nlo_set_xtol_abs(ires, opt, 1.0e-15_8)
    if(ires.ne.1) then
      write(*, *) 'NLOPT error at nlo_set_xtol_abs. code = ', ires
      stop
    end if
    call nlo_set_maxeval(ires, opt, 1000)
    if(ires.ne.1) then
      write(*, *) 'NLOPT error at nlo_set_maxeval. code = ', ires
      stop
    end if

    do mu = 1, noptpars
      pars(mu) = lambda(optpars(mu))
    end do
    call nlo_optimize(ires, opt, pars, g0)
    do mu = 1, noptpars
      lambda(optpars(mu)) = pars(mu)
    end do
    write(*, *) 'ires = ', ires
    write(*, *) 'lambda0 = ', lambda
    write(*, *) 'G(lambda0) = ', g0

    call xc_end(xc)
    call xc_init(xc, lambda = lambda, mode = XC_GENERIC, parametrization = XC_PARAMS_TAYLOR_HD)
    call xc_write(xc, 'xc-optimized')

    call operator_end(w)
    deallocate(gammaref)
    deallocate(lambda)
    deallocate(density)

  case(RUNMODE_MINIMIZED)

    nparams = 2
    allocate(lambda(0:nparams-1))
    lambda = 0.0_8
    call xc_init(xc, lambda = lambda, parametrization = XC_PARAMS_TAYLOR)

    allocate(gammaref(0:grid%p-1))
    gammaref = 0.0_8
    gammaref(0) = 1.0_8

    call nlo_create(opt, NLOPT_GN_DIRECT, nparams)

    allocate(lb(nparams))
    allocate(ub(nparams))
    lb(:) = - 10.0_8
    ub(:) =   10.0_8

    call nlo_set_lower_bounds(ires, opt, lb)
    if(ires.ne.1) then
      write(*, *) 'NLOPT error at nlo_set_lower_bounds. code = ', ires
      stop
    end if
    call nlo_set_upper_bounds(ires, opt, ub)
    if(ires.ne.1) then
      write(*, *) 'NLOPT error at nlo_set_upper_bounds. code = ', ires
      stop
    end if
    call nlo_set_min_objective(ires, opt, dfunc, 0)
    if(ires.ne.1) then
      write(*, *) 'NLOPT error at nlo_set_min_objective. code = ', ires
      stop
    end if
    call nlo_set_xtol_abs(ires, opt, 1.0e-8_8)
    if(ires.ne.1) then
      write(*, *) 'NLOPT error at nlo_set_xtol_abs. code = ', ires
      stop
    end if
    call nlo_set_maxeval(ires, opt, 1000)
    if(ires.ne.1) then
      write(*, *) 'NLOPT error at nlo_set_maxeval. code = ', ires
      stop
    end if

    call nlo_optimize(ires, opt, lambda(0:nparams-1), d0)
    write(*, *) 'ires = ', ires
    write(*, *) 'lambda0 = ', lambda(0:grid%p-1)
    write(*, *) 'D(lambda0) = ', d0

    call xc_end(xc)
    deallocate(lb)
    deallocate(ub)
    deallocate(gammaref)
    deallocate(lambda)

  end select

  contains

  subroutine dfunc(val, n, x, grad, need_gradient, f_data)
    real(8) :: val, x(n), grad(n), f_data
    integer :: n, need_gradient

    integer :: p, j
    real(8), allocatable :: gamma(:)

    p = grid%p
    allocate(gamma(0:p-1))

    do j = 0, p-1
      gamma(j) = x(j+1)
    end do
    ! gamma(p-1) = 0.0_8

    val = 0.0_8
    do j = 0, p-1
      val = val + (gamma(j) - gammaref(j))**2
    end do

    if(need_gradient .eq. 1) then
      grad = 0.0_8
      stop 'Error'
    end if
    deallocate(gamma)
  end subroutine dfunc


  subroutine gfunc(val, n, x, grad, need_gradient, f_data)
    real(8) :: val, x(n), grad(n), f_data
    integer :: n, need_gradient

    integer :: mu
    type(xc_t) :: xc

    write(*, *)
    write(*, *) 'gfunc: x = ', x

    do mu = 1, noptpars
      lambda(optpars(mu)) = x(mu)
    end do
    write(*, *) 'gfunc: lambda = ', lambda
    call xc_init(xc, lambda = lambda, mode = XC_GENERIC, parametrization = XC_PARAMS_TAYLOR_HD)
    call gfunction(w, v, xc, kin, gammaref, val)
    call xc_end(xc)

    write(*, *) 'gfunc: val = ', val
    write(*, *)

    if(need_gradient .eq. 1) then
      stop 'Error'
    end if

  end subroutine gfunc

  end subroutine maxdft_run

end module maxdft_m
