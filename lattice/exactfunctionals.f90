!> This module contains the subroutines resposible of the computation of the
!! universal functional \f$F\f$.
!!
!! One can define the universal functional \f$F\f$ as dependent on the full set of density numbers
!! \f$n_1,\dots,n_P\f$. However, these numbers are constrained by the condition
!! \f$\sum_{m=1}^P n_m = N\f$. The universal functional \f$F\f$ is defined only over sets
!! of densities that fulfill that condition. This poses a question on the
!! validity of the functional derivatives of this functional with respect to its
!! arguments: one may not vary one leaving the others unchanged withouth going
!! beyond the definition range of the functional. This problem is ignored in the
!! standard derivations of DFT (and probably can be ignored), but we will be
!! extra careful.
!!
!! We define, for any set of occupation numbers \f$n = (n_1,\dots,n_P)\f$, the set:
!! \f[
!! M(n) = \lbrace \Psi | n_m(\Psi) = n_m\rbrace\,.
!! \f]
!! This set is empty if the occupation numbers do not add up to
!! \f$N\f$. The ``universal functional'' \f$F\f$ is then defined as:
!! \f[
!! F(n) = \min_{\Psi \in M(n)}
!! \langle\Psi\vert(\hat{T}+\hat{W})\vert\Psi\rangle\,.
!! \f]
!!
!! The functional \f$F\f$ is universal, inasmuch as it does not depend on the
!! external potential \f$\hat{V}\f$. We however define a system-dependent functional:
!! \f[
!! \nonumber
!! E(n) = F(n) + \sum_m v_m n_m.
!! \f]
!! \ref exactfunctionals1 computes \f$F[n]\f$ using this equation.
!!
!! One may define a set of variables
!! \f$\rho_1,\dots,\rho_{P-1}\f$, functions of the density values, \f$\rho_n =
!! \rho_n(n)\f$, such that one has an inverse relation \f$n = n(\rho)\f$ that by
!! construction respects the sum rule. As an example, one easy choice could be:
!! \f[
!! \label{eq:rhodef1-1}
!! \rho_m = n_m\;\;(m=1,\dots,P-1)
!! \f]
!! and
!! \f{eqnarray}{
!! \label{eq:rhodef1-2}
!! n_m &=& \rho_m\;\;(m=1,\dots,P-1)\,,
!! \\
!! \label{eq:rhodef1-3}
!! n_P &=& N-\sum_{n=1}^{P-1}\rho_n\,.
!! \f}
!!
!! Then, we may repeat the derivation using this set of variables. We start with
!! the definition of the set:
!! \f[
!! M(\rho) = \lbrace \Psi | \rho_m(\Psi) = \rho_m\rbrace\,.
!! \f]
!! The ``universal functional'' \f$F\f$ and the energy functional \f$E\f$ are now:
!! \f[
!! F(\rho) = \min_{\Psi \in M(\rho)}
!! \langle\Psi\vert(\hat{T}+\hat{W})\vert\Psi\rangle\,.
!! \f]
!! \f[
!! \nonumber
!! E(\rho) = F(\rho) + \sum_m v_m n_m(\rho)
!! \f]
!! \ref exactfunctionals2 uses this definition of \f$F[n]\f$ to compute it.


module exactfunctionals_m
  use grid_m
  use basis_m
  use operator_m
  use many_m
  use potential_m

  implicit none

  private
  public ::            &
    exactfunctionals1, &
    exactfunctionals2

  contains

  !> Computes \f$F[n] = E[n] - V[n]\f$.
  !! @param[in] kin The kinetic energy operator.
  !! @param[in] w The electronic interaction operator.
  !! @param[in] npoints The number of different densities.
  !! @param[in] deltav (OBSOLETE)
  !! @param[out] n1 The array with the densities.
  !! @param[out] fn The array with the values of \f$F[n]\f$.
  !! @todo Expand the documentation. Right now these two subroutines
  !! are too unstable, so they are barely documented.
  subroutine exactfunctionals1(kin, w, npoints, deltav, n1, fn)
    type(operator_t), intent(in) :: kin, w
    integer, intent(in) :: npoints
    real(8), intent(in) :: deltav
    real(8), intent(out) :: n1(:,:)
    real(8), intent(out) :: fn(:)

    integer :: j, p
    real(8) :: energy
    real(8), allocatable :: vvalues(:), n0(:)
    type(operator_t) :: v


    p = grid%p

    allocate(vvalues(p))
    allocate(n0(p))
 
    do j = 1, npoints
      call v_init(vvalues, k = real(j,8))

      call operator_init_extpot(v, vvalues)
      call many_solve(kin, v, w, energy, n0)

      n1(j,:) = n0(:)
      fn(j) = energy - sum(vvalues(:)*n0(:))

      call operator_end(v)
    end do
      
    deallocate(vvalues)
    deallocate(n0)
  end subroutine exactfunctionals1

  !> Computes the functional \f$F[n]\f$ using the variational principle, where we
  !! use the NLOPT library to search in the space of antisymmetric wave-functions
  !! made with Slater determinants of the KS orbitals.
  !! @param[in] kin The kinetic energy operator.
  !! @param[in] w The electronic interaction operator.
  !! @param[in] npoints The number of different densities.
  !! @param[in] n0all The values of the densities.
  !! @param[out] fn The array with the values of \f$F[n]\f$.
  subroutine exactfunctionals2(kin, w, npoints, n0all, fn)
    type(operator_t), intent(in) :: kin, w
    integer, intent(in) :: npoints
    real(8), intent(in) :: n0all(:, :)
    real(8), intent(out) :: fn(:)

    integer(8) :: opt
    integer :: p, j, ires
    real(8) :: energy, min, tol
    real(8), allocatable :: vvalues(:), n0(:), psi0(:), nmin(:), lb(:), ub(:)
    complex(8), allocatable :: zpsi0(:)
    type(operator_t) :: v

    include 'nlopt.f'

    p = grid%p

    allocate(vvalues(p))
    allocate(n0(p))
    allocate(nmin(p))
    allocate(zpsi0(basis%dim))
    allocate(psi0(basis%dim))

    allocate(lb(basis%dim))
    allocate(ub(basis%dim))

    lb(:) = -1.0_8
    ub(:) =  1.0_8

    vvalues = 0.0_8
    call operator_init_extpot(v, vvalues)
    call many_solve(kin, v, w, energy, n0, zpsi = zpsi0)
    call operator_end(v)

    psi0 = real(zpsi0, 8)

    do j = 1, npoints
      n0(:) = n0all(j, :)

      call nlo_create(opt, NLOPT_LD_SLSQP, basis%dim)

      call nlo_set_min_objective(ires, opt, fpsi, 0)
      if(ires.ne.1) write(*, *) 'b', 'ires = ', ires
      call nlo_add_equality_constraint(ires, opt, g1, 0, 1.0e-12_8)
      if(ires.ne.1) write(*, *) 'c', 'ires = ', ires
      call nlo_add_equality_constraint(ires, opt, g2, 0, 1.0e-12_8)
      if(ires.ne.1) write(*, *) 'd', 'ires = ', ires
      call nlo_set_lower_bounds(ires, opt, lb)
      if(ires.ne.1) write(*, *) 'd.1', 'ires = ', ires
      call nlo_set_upper_bounds(ires, opt, ub)
      if(ires.ne.1) write(*, *) 'd.2', 'ires = ', ires
      call nlo_set_ftol_abs(ires, opt, 1.0e-12_8)
      if(ires.ne.1) write(*, *) 'e', 'ires = ', ires
      call nlo_set_maxeval(ires, opt, 1000)
      if(ires.ne.1) write(*, *) 'f', 'ires = ', ires

      call nlo_optimize(ires, opt, psi0, fn(j))

    end do

    deallocate(psi0)
    deallocate(zpsi0)
    deallocate(n0)
    deallocate(vvalues)
    deallocate(nmin)

    contains

  subroutine g1(res, n, psi, grad, need_gradient, fdata)
    real(8) :: res
    integer :: n
    real(8) :: psi(n)
    real(8) :: grad(n)
    integer :: need_gradient
    real    :: fdata

    res = dot_product(psi, psi) - 1.0_8

    if(need_gradient .ne. 0) then
      grad = 2.0_8 * psi
    end if

  end subroutine g1

  subroutine g2(res, n, psi, grad, need_gradient, fdata)
    real(8) :: res
    integer :: n
    real(8) :: psi(n)
    real(8) :: grad(n)
    integer :: need_gradient
    real    :: fdata

    integer :: ii, m
    real(8), allocatable :: nm(:)
    complex(8), allocatable :: zpsi(:)
    complex(8), allocatable :: zpsiplus(:)

    allocate(zpsi(basis%dim))
    allocate(zpsiplus(basis%dim))
    allocate(nm(p))

    zpsi = cmplx(psi, 0.0_8, 8)

    nm  = compute_density(zpsi)

    res = dot_product(nm - n0, nm - n0)

    if(need_gradient .ne. 0) then
      do ii = 1, n
        grad(ii) = 0.0_8
        do m = 1, p
          grad(ii) = grad(ii) + 4.0_8 * (nm(m)-n0(m)) * basis%nm(ii, m) * psi(ii)
        end do
      end do
    end if

    deallocate(nm)
    deallocate(zpsi)
    deallocate(zpsiplus)
  end subroutine g2

  subroutine fpsi(res, n, psi, grad, need_gradient, fdata)
    real(8) :: res
    integer :: n
    real(8) :: psi(n)
    real(8) :: grad(n)
    integer :: need_gradient
    real    :: fdata

    real(8), allocatable :: mat(:, :)

    allocate(mat(basis%dim, basis%dim))

    mat = real(w%mat2 + kin%mat2, 8)
    res = dot_product(psi, matmul(mat, psi))

    if(need_gradient .ne. 0) then
      grad = 2.0_8 * matmul(mat, psi)
    end if

    deallocate(mat)
  end subroutine fpsi

  end subroutine exactfunctionals2

end module exactfunctionals_m
