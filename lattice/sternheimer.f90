!Module for the computation of the gradient of the electronic density
!respect to the interaction potential parameters using
!the Sternheimer method

module sternheimer_m
  use basis_m
  use grid_m
  use math_m
  use operator_m

  implicit none

  private
  public ::   &
    sternheimer

  contains

  subroutine sternheimer(kin, w, v, gamma, psi0, eps0, l, psigrad)
    type(operator_t), intent(in) :: kin, w, v
    real(8), intent(in) :: gamma(0:)
    complex(8), intent(in) :: psi0(:)
    real(8), intent(in) :: eps0
    integer, intent(in) :: l
    complex(8), intent(out) :: psigrad(:)

    type(operator_t) :: wprime
    complex(8), allocatable :: a(:), b(:,:), c(:,:), psi1(:), work(:), proj(:, :), pert(:, :)
    complex(8) :: z
    real(8), allocatable :: gammap(:)
    integer, allocatable :: ipiv(:)
    integer dim, p, i, j, k, info, lwork

    dim = basis%dim
    p = basis%p
    allocate(psi1(1:dim))
    allocate(a(1:dim))
    allocate(b(1:dim, 1:dim))
    allocate(c(1:dim, 1:dim))
    allocate(pert(1:dim, 1:dim))
    allocate(proj(1:dim, 1:dim))
    allocate(ipiv(dim))

    ! Computation of the gradient of W wrt. paramters.
    allocate(gammap(0:p-1))
    gammap = 0.0_8
    gammap(l) = 1.0_8
    call operator_init_w(wprime, gammap)
    deallocate(gammap)

    psi1 = (0.0_8, 0.0_8)
    b = kin%mat2 + v%mat2 + w%mat2

    pert = wprime%mat2

    !Computation of a = (1 - |ps0><psi0|) * w |psi0>
    do i = 1, dim
      do j = 1, dim
        proj(i, j) = kdelta(i, j) - psi0(i) * psi0(j)
      end do
    end do
    c = matmul(proj, pert)
    a = - matmul(c, psi0)

    !Computation of b = (H0 - eps0)
    do i = 1, dim
      do j = 1, dim
        b(i,j) = b(i,j) - kdelta(i,j)*eps0
      end do
    end do

 

    !Calling the LAPACK linear solver ZGESV for b * psi1 = a
    !Documentation in
    !physics.oregonstate.edu/~landaur/nacphy/lapack/routines/csysv.html
    psi1 = a
    c = b
    !call zgesv('U', dim, 1, b, dim, ipiv, psi1, dim, work, lwork, info)
    call zgesv(dim, 1, b, dim, ipiv, psi1, dim, info)
    if(info .ne. 0) then
      write(*,*) 'LAPACK linear solver exit status: ', info
    end if
    psigrad = psi1

    !Checking the gradient result
    !call xc_init(xc, grid, lambda, parametrization = XC_PARAMS_TAYLOR)
    !do i = 0, p
    !  ggrad_g = gamma(i) + 0.001
    !  call gfunction(w, v, xc, kin, ggrad_g, ggrad_p)
    !  ggrad_g = gamma(i) - 0.001
    !  call gfunction(w, v, xc, kin, ggrad_g, ggrad_m)
    !  write(*,*) 'Ggrad_num - Ggrad_stern = ', &
    !  (ggrad_p - ggrad_m)/(2*0.001) - ngrad(i)
    !end do

    deallocate(proj)
    deallocate(a)
    deallocate(b)
    deallocate(c)
    deallocate(ipiv)
  end subroutine sternheimer

end module sternheimer_m
