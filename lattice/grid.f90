!> This module contains the definition of the spatial grid, for the moment
!! one-dimensional.
!!
!! The grid is a 1D grid for the interval \f$[a,b]\f$, and it has the form:
!!
!! \f[
!!     x_n = a + n\Delta \;\;\;\;\; (n = 1,\dots, P)
!! \f]
!!
!! Note that \f$ a\f$ does not belong the grid, and neigher does \f$ b = 
!! x_{P+1}\f$. The grid discretization is given by:
!!
!! \f[
!!     \Delta = \frac{b-a}{P}\,.
!! \f]
module grid_m
  implicit none

  private
  public ::          &
    grid_t,          &
    grid_init,       &
    grid_write_info, &
    grid_end ,       &
    grid
       

  !> This defines a grid in the interval [a,b], made of p points at
  !! \f$ x_n = a + n\Delta, n = 1,\dots, P\f$
  !! for \f$\Delta = (b-a)/P\f$. 
  !! The points \f$x_0 = a\f$ and \f$x_{P+1} = b\f$ do not belong to the grid, as it is assumed that all
  !! functions have zero values at a and b (zero boundary conditions)
  type grid_t
    real(8) :: a     !< Left boundary of the simulation box
    real(8) :: b     !< Right boundary of the simulation box
    integer :: P     !< Number of points
    real(8) :: delta !< Discretization step (distance from grid point to grid point) 
  end type grid_t

  !> This is the global grid variable.
  type(grid_t) :: grid

contains

  !> Initialization of the grid.
  !>
  !> Sets a, b, P, and delta = (b-a)/(p+1) for the global grid variable.
  !>
  !> @param a The left boundary of the simulation box
  !> @param b The right boundary of the simulation box
  !> @param p The number of grid points
  subroutine grid_init(a, b, p)
    real(8), intent(in) :: a, b
    integer, intent(in) :: p
    grid%a = a
    grid%b = b
    grid%p = p
    grid%delta = (b-a) / (p+1)
  end subroutine grid_init

  !> Finalization of the grid.
  !>
  !> It just sets p to zero.
  subroutine grid_end()
    grid%p = 0
  end subroutine grid_end

  !> Writes a file with information about the grid.
  !>
  !> The name of the file is given by filename.
  !>
  !> @param filename Name of the file where to write the grid details.
  subroutine grid_write_info(filename)
    character(len=*), intent(in) :: filename

    integer :: m

    open(unit = 11, file = trim(filename))

    write(11, *) 'Interval = [',grid%a, ',', grid%b, ']'
    write(11, *) 'delta = ', grid%delta
    write(11, *) 'Points : '
    do m = 1, grid%P
      write(11, *) grid%a + grid%delta*m
    end do
    close(unit = 11)

  end subroutine grid_write_info

end module grid_m
