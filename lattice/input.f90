module input_m
  implicit none

  private
  public :: &
  read_variable

  contains

  ! Scan "input" file for tag "label", and assign its value to "var".
  subroutine read_variable(label, rvar, ivar, svar, avar, zvar, required)
    character(len=*), intent(in) :: label
    real(8), optional, intent(inout) :: rvar
    integer, optional, intent(inout) :: ivar
    character(len=*), optional, intent(inout) :: svar
    real(8), optional, intent(inout) :: avar(:)
    complex(8), optional, intent(inout) :: zvar(:)
    logical, optional, intent(in) :: required
    character :: mode

    character(len=80) :: buffer, lab
    integer, parameter :: file_unit = 11
    integer :: iostate, pos, ierr, asize, zsize, i, j, ios
    logical :: required_

    required_ = .false.; if(present(required)) required_ = required
    mode = '0'
    zsize = 0
    asize = 0

    if(present(rvar)) then
      mode = 'r'
    elseif(present(ivar)) then
      mode = 'i'
    elseif(present(svar)) then
      mode = 's'
    elseif(present(avar)) then
      mode = 'a'
      asize = size(avar)
    elseif(present(zvar)) then
      mode = 'z'
      zsize = size(zvar)
    else
      return
    end if


    open(unit = file_unit, file = 'input')


    iostate = 0
    ierr = -1
    scanfile: do while(iostate == 0)
      read(file_unit, '(a)', iostat=iostate) buffer
      if(iostate == 0) then
      pos = scan(buffer, ' ')
      lab = buffer(1:pos)
      buffer = buffer(pos+1:)
      if(trim(lab) == trim(label)) then
        select case(mode)
        case('r')
          read(buffer, *, iostat = iostate) rvar
          if(iostate == 0) write(15, '(a,g20.10)') trim(label), rvar
        case('i')
        read(buffer, *, iostat = iostate) ivar
        if(iostate == 0) write(15, '(a,i20)') trim(label), ivar
        case('s')
          read(buffer, *, iostat = iostate) svar
          if(iostate == 0) write(15, '(a,1x,a)') trim(label), trim(svar)
        case('a')
          avar = 0.0_8
          do j = 1, asize
            read(buffer, *, iostat = ios) (avar(i), i = 1, j)
            if(ios.ne.0) exit
          end do
          iostate = 0 ! in any case, even if nothing could be read.
          write(15, '(a)', advance = 'no') trim(label)
          do i = 1, j-1
            write(15, '(g20.10)', advance = 'no') avar(i)
          end do
          write(15, *)
        case('z')
          zvar = 0.0_8
          do j = 1, zsize
            read(buffer, *, iostat = ios) (zvar(i), i = 1, j)
            if(ios.ne.0) exit
          end do
          iostate = 0 ! in any case, even if nothing could be read.
          write(15, '(a)', advance = 'no') trim(label)
          do i = 1, j-1
            write(15, '("(",g20.10,",",g20.10,")")', advance = 'no') zvar(i)
          end do
          write(15, *)
        end select
        ierr = 0
        exit scanfile
       end if
      end if
    end do scanfile

    if(ierr .ne. 0 .and. required_) then
      write(*, *) 'Variable "', trim(label), '" could not be read.'
      stop
    end if
    close(unit = file_unit)
  end subroutine read_variable

end module input_m
