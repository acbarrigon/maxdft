module gfunction_m
  use basis_m
  use operator_m
  use xc_m
  use many_m
  use dft_m
  use sternheimer_m

  implicit none

  private
  public ::    &
    gfunction, &
    ggradient

  integer, public, parameter ::       &
    GGRADIENT_METHOD_FD          = 0, &
    GGRADIENT_METHOD_STERNHEIMER = 1

  contains

  subroutine gfunction(w, v, xc, kin, gamma, val)
    type(operator_t), intent(in) :: w
    type(operator_t), intent(in) :: v(:)
    type(xc_t), intent(in) :: xc
    type(operator_t), intent(in) :: kin
    real(8), intent(in) :: gamma(0:)
    real(8), intent(out) :: val

    integer :: k, npots, p, N, m
    real(8) :: energy, dftenergy
    real(8), allocatable :: density(:), dftdensity(:)


    npots = size(v)
    p = basis%p
    N = basis%N

    allocate(density(p))
    allocate(dftdensity(p))

    val = 0.0_8
    do k = 1, npots
      call many_solve(kin, v(k), w, energy, density)
      call dft_solve(N, kin, v(k), gamma, xc, dftenergy, dftdensity, guess_density = density)
      do m = 1, p
        val = val + (density(m) - dftdensity(m))**2
      end do
    end do

    deallocate(density)
    deallocate(dftdensity)
  end subroutine gfunction

  subroutine ggradient(w, v, xc, kin, gamma, method, val, grad)
    type(operator_t), intent(in) :: w
    type(operator_t), intent(in) :: v(:)
    type(xc_t), intent(in) :: xc
    type(operator_t), intent(in) :: kin
    real(8), intent(in) :: gamma(0:)
    integer, intent(in) :: method
    real(8), intent(out) :: val
    real(8), intent(out) :: grad(:)

    integer :: k, N, npots, p, l, m, ii
    real(8) :: delta, valplus, valminus, energy, dftenergy, x, energyplus
    real(8), allocatable :: gammaplus(:), gammaminus(:)
    real(8), allocatable :: density(:, :), dftdensity(:, :), densityplus(:, :)
    complex(8), allocatable :: zpsi(:, :), zpsiprime(:), zpsiplus(:)
    type(operator_t) :: wprime


    select case(method)
    case(GGRADIENT_METHOD_FD)

      delta = 0.0001_8
      allocate(gammaplus(0:basis%p-1))
      allocate(gammaminus(0:basis%p-1))

      ! We will assume that gamma(p-1) = 0 always, and therefore we do not need its variation.
      do k = 0, basis%p-2
        gammaplus = gamma
        gammaplus(k)  = gammaplus(k) + delta
        call operator_init_w(wprime, gammaplus)
        call gfunction(wprime, v, xc, kin, gamma, valplus)
        call operator_end(wprime)
        gammaminus = gamma
        gammaminus(k)  = gammaminus(k) - delta
        call operator_init_w(wprime, gammaminus)
        call gfunction(wprime, v, xc, kin, gamma, valminus)
        call operator_end(wprime)
        grad(k+1) = (valplus - valminus) / (2 * delta)
      end do

      deallocate(gammaplus)
      deallocate(gammaminus)

    case default

      grad(:) = 0.0_8

      p = basis%p
      N = basis%N
      npots = size(v)
      allocate(density(p, npots))
      allocate(densityplus(p, npots))
      allocate(dftdensity(p, npots))
      allocate(zpsi(basis%dim, npots))
      allocate(zpsiprime(basis%dim))

      grad(:) = 0.0_8
      do k = 1, npots
        call many_solve(kin, v(k), w, energy, density(:, k), zpsi = zpsi(:, k))
        call dft_solve(N, kin, v(k), gamma, xc, dftenergy, dftdensity(:, k), guess_density = density(:, k))

        do l  = 0, basis%p - 2
          ! We add a 0.0001_8 infinitesimal to avoid the Sternheimer singularity.
          ! This should be taken care of by the projectors, but it seems it does not work properly.
          call sternheimer(kin, w, v(k), gamma, zpsi(:, k), energy+0.0001_8, l, zpsiprime)
          do m = 1, p
            x = 0.0_8 
            do ii = 1, basis%dim
              x = x + basis%nm(ii, m) * real( conjg(zpsi(ii, k)) * zpsiprime(ii) )
            end do
            grad(l+1) = grad(l+1) + 2.0_8 * (density(m, k) - dftdensity(m, k)) * 2.0_8 * x
          end do
        end do
      end do

      deallocate(density)
      deallocate(dftdensity)
      deallocate(zpsi)
      deallocate(zpsiprime)

    end select

  end subroutine ggradient


end module gfunction_m
