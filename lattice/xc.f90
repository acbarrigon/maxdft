!> This module takes care about everything related to the XC functional \f$E_{xc}[n]\f$,
!!including the initialization and deallocation of the XC type varible, the output
!! to a file of the information about the XC functional and the computation of
!! \f$E_{xc}[n]\f$ and the associated XC potential \f$V_{xc}[n]\f$.
module xc_m
  use input_m
  use grid_m
  use basis_m
  use operator_m
  use input_m
  use exactfunctionals_m
  use hartree_m
  use many_m
  use bspline_module
  use,intrinsic :: iso_fortran_env, only: wp => real64

  implicit none

  private
  public ::      &
    xc_t,        &
    xc_init,     &
    xc_end,      &
    xc_write,    &
    uxcenergy,   &
    compute_vxc

  integer, parameter, public :: &
    XC_PARAMS_ALL    =  0,      &
    XC_PARAMS_TAYLOR =  1,      &
    XC_PARAMS_TAYLOR_HD = 2,    &
    XC_PARAMS_NONE   = -1

  integer, parameter, public :: &
    XC_GENERIC = 0,             &
    XC_EXX     = 1,             &
    XC_EXACT   = 2

  type xc_t
    real(8), allocatable :: lambda(:)
    integer :: parametrization
    integer :: mode
    integer :: nparams
    ! For the exact xc:
    integer :: npoints
    real(8), allocatable :: exc_(:)
    real(8), allocatable :: vxc_(:, :)
  end type xc_t

  contains


  !> Subroutine in charge of initializating the xc variable.
  !! We will define the XC functional through a set of tensors, defining the expansion:
  !! \f[
  !! E_{xc}[n] = E_xc[0] + \sum_{m} e_{m} n_{m} + (1/2) \sum_{mn} e_{mn} n_{m} n_{n} + ...
  !! \f]
  !! We will cut the expansion there for now. It is sufficient for the EXX approximation (with 2e).
  !!
  !! The zero-th order term \f$E_{xc}[0]\f$ is irrelevant. The first order term is also irrelevant,
  !! because due to translational symmetry, all \f$e_{m}\f$ must be equal, and therefore the
  !! derivative wrt to the \f$n_{m}\f$ are all equal: the xc potential derived from this term would
  !! be a constant, which is irrelevant.
  !!
  !! We are left with the second-order term. Once again, due to translational symmetry, we
  !! must have \f$e_{mn} = e_{pq} if |m-n| = |p-q|\f$. Therefore, we only have P independent terms, and
  !! we may set:
  !! \f[
  !! E_{xc}[n] = (1/2) \sum_{mn} lambda_{|m-n|} n_m n_n + ....
  !! \f]
  !! Therefore, we may characterize the possible exchange and correlation potentials by a vector
  !! lambda(0:P-1)
  !!
  !! (Later on, we will introduced cubic, quartic, etc. terms, which will imply the use of higher
  !! order tensors lambda)
  subroutine xc_init(xc, gamma, lambda, mode, parametrization, kin, w)
    type(xc_t), intent(inout) :: xc
    real(8), optional, intent(in) :: gamma(0:), lambda(0:)
    integer, optional, intent(in) :: mode
    integer, optional, intent(in) :: parametrization
    type(operator_t), optional, intent(in) :: kin, w

    integer :: i, j, k, npoints, nsteps, m
    real(8), allocatable :: fn(:), n0(:, :), gamma0(:), ts(:)
    real(8) :: dn
    type(operator_t) :: w0


    ! BSPLINE variables
    type(bspline_1d) :: int1d
    integer :: use_bspline, kx, idx, iflag
    real(wp), allocatable :: fcn(:), x(:)
    real(wp) :: val
    kx=4  ! order of interpolation: 3=>cuadratic, 4=>cubic...
    idx=1  ! 0 for regular interpolation, 1 for its derivative
    use_bspline = 0; call read_variable('use_bspline', ivar = use_bspline)

    if(present(parametrization)) then
      xc%parametrization = parametrization
    else
      xc%parametrization = XC_PARAMS_ALL
    end if

    if(present(mode)) then
      xc%mode = mode
    else
      xc%mode = XC_GENERIC
    end if

    ! If the mode is "exact, it does not make sense to speak of parametrization.
    if(xc%mode == XC_EXACT) then
      xc%parametrization = XC_PARAMS_NONE
      ! We will use an odd number of points, so that the "center" is in the grid.
      select case(grid%p)
      case(2)
        npoints = 1001; call read_variable('npoints', ivar = npoints)
        nsteps = npoints
      case(3)
        npoints = 1001; call read_variable('npoints', ivar = npoints)
        !nsteps = npoints*(npoints-1)/2+npoints
        nsteps = npoints*(npoints+1)/2
      end select
      xc%npoints = nsteps

      ! Once again, the "new" way.
      allocate(n0(nsteps, grid%p))
      allocate(fn(xc%npoints))
      allocate(ts(xc%npoints))

      dn = real(basis%N,8) / (npoints-1)
      call density_init(npoints, nsteps, dn, n0)

      allocate(xc%exc_(xc%npoints))
      allocate(xc%vxc_(xc%npoints, grid%p))

      allocate(gamma0(0:grid%p-1))
      gamma0 = 0.0_8
      call operator_init_w(w0, gamma0)
      deallocate(gamma0)

      call exactfunctionals2(kin, w0, xc%npoints/2+1, n0(xc%npoints/2+1:xc%npoints, :), ts(xc%npoints/2+1:xc%npoints))
      call exactfunctionals2(kin, w, xc%npoints/2+1, n0(xc%npoints/2+1:xc%npoints, :), fn(xc%npoints/2+1:xc%npoints))

      do j = npoints/2+1, npoints
        xc%exc_(j) = fn(j) - ts(j) - uenergy(gamma, n0(j, :))
      end do
      k = npoints/2+2
      do j = npoints/2, 1, -1
        xc%exc_(j) = xc%exc_(k)
        k = k + 1
      end do

      select case(grid%p)
      case(2)
        if (use_bspline .eq. 1) then
          allocate(fcn(nsteps))
          allocate(x(nsteps))
          do i = 1, nsteps
            fcn(i) = xc%exc_(i)
            x(i) = n0(i,1)
          end do

          call int1d%initialize(x, fcn, kx, iflag)
          if (iflag/=0) then
            write(*,*) 'Error initializing 1D spline: '//get_status_message(iflag)
          end if

          do i = 1, nsteps
            call int1d%evaluate(x(i), idx, val, iflag)
            if (iflag/=0) then
              write(*,*) 'Error computing 1D spline: '//get_status_message(iflag)
            end if
            xc%vxc_(i,1) = val
            !write(*,*) x(i), fcn(i), xc%exc_(i), val(i)
          end do

          call int1d%destroy()

          deallocate(fcn)
          deallocate(x)
        else
          xc%vxc_(1, 1) = (xc%exc_(2) - xc%exc_(1))/dn
          do j = 2, npoints - 1
            xc%vxc_(j, 1) = (xc%exc_(j+1) - xc%exc_(j-1))/(2.0_8*dn)
          end do
          xc%vxc_(npoints, 1) = (xc%exc_(npoints) - xc%exc_(npoints-1))/dn
        endif
      case(3)
        xc%vxc_(1, 1) = (xc%exc_(2) - xc%exc_(1))/dn
        xc%vxc_(1, 2) = (xc%exc_(2) - xc%exc_(1))/dn
        do j = 2, npoints - 1
          xc%vxc_(j, 1) = (xc%exc_(j+1) - xc%exc_(j-1))/(2.0_8*dn)
          xc%vxc_(j, 2)= (xc%exc_(j+1) - xc%exc_(j-1))/(2.0_8*dn)
        end do
        xc%vxc_(npoints, 1) = (xc%exc_(npoints) - xc%exc_(npoints-1))/dn
        xc%vxc_(npoints, 2) = (xc%exc_(npoints) - xc%exc_(npoints-1))/dn
      end select

      ! We will arbitrarily set the xc potential of the last lattice point to zero.
      xc%vxc_(:, grid%p) = 0.0_8

      open(unit = 11, file = 'exact-xc_')
      do j = 1, npoints
        write(11, *) j, n0(j, 1:grid%p-1), xc%exc_(j), xc%vxc_(j, 1)
      end do
      close(unit = 11)

      deallocate(fn)
      deallocate(ts)
      deallocate(n0)
      call operator_end(w0)

    end if


    select case(xc%parametrization)
    case(XC_PARAMS_ALL)
      xc%nparams = grid%p
      allocate(xc%lambda(0:grid%p-1))
      select case(xc%mode)
      case(XC_GENERIC)
        xc%lambda = lambda
      case(XC_EXX)
        xc%lambda = - 0.5_8 * gamma
      end select

    case(XC_PARAMS_TAYLOR)
      xc%nparams = size(lambda)
      !write(*, *) 'nparams = ', xc%nparams
      allocate(xc%lambda(0:xc%nparams-1))
      xc%lambda(0:xc%nparams-1) = lambda(0:xc%nparams-1)

    case(XC_PARAMS_TAYLOR_HD)
      xc%nparams = size(lambda)
      !write(*, *) 'nparams = ', xc%nparams
      allocate(xc%lambda(xc%nparams))
      xc%lambda = lambda

    case(XC_PARAMS_NONE)
      xc%nparams = 0
    end select

  end subroutine xc_init

  !> Subroutine in charge of freeing the xc variable.
  !! @param[inout] xc The variable with the information about \f$E_{xc}\f$.
  subroutine xc_end(xc)
    type(xc_t), intent(inout) :: xc
    if(allocated(xc%lambda)) deallocate(xc%lambda)
!!$    if(allocated(xc%exc)) deallocate(xc%exc)
!!$    if(allocated(xc%vxc)) deallocate(xc%vxc)
    if(allocated(xc%exc_)) deallocate(xc%exc_)
    if(allocated(xc%vxc_)) deallocate(xc%vxc_)
  end subroutine xc_end


  !> Computation of the exchange and correlation energy. Right now, it assumes
  !! the exact-exchange for two electrons:
  !! \f[
  !! E_{xc}[n] = - \frac{1}{2} U[n]
  !!\f]
  !! It also computes the "xcintegral":
  !! \f[
  !! \int dx n(x) v_{xc}[n] (x)
  !! \f]
  !! necessary to compute the total energy in a DFT calculation.
  !! @param[in] xc The variable with the information about \f$E_{xc}\f$.
  !! @param[in] nm The array with the density of the system.
  !! @param[out] xcenergy The computed value of \f$E_{xc}[n]\f$.
  !! @param[out] xcintegral The numerical integral \f$\int dx n(x) v_{xc}[n] (x)\f$.
  !! @todo Instead of using two interpolations for \f$E_{xc}[n]\f$ and \f$V_{xc}[n]\f$,
  !! only use one for \f$E_{xc}[n]\f$ and compute the derivative.
  subroutine uxcenergy(xc, nm, xcenergy, xcintegral)
    type(xc_t), intent(in) :: xc
    real(8), intent(in) :: nm(:)
    real(8), intent(out) :: xcenergy, xcintegral

    integer :: m, n, j, k
    real(8) :: dn, na, nb
    real(8), allocatable :: vxc(:)

    ! BSPLINE variables
    type(bspline_1d) :: int_exc, int_vxc
    integer :: use_bspline, kx, idx, iflag, i
    real(wp):: fvxc(5), fexc(5), x(5), val
    kx=4
    idx = 0
    use_bspline = 0; call read_variable('use_bspline', ivar = use_bspline)

    allocate(vxc(one_basis%p))

    if(xc%mode == XC_EXACT) then
      dn = real(basis%N, 8) / ( xc%npoints - 1 )
      j = 1 + int( nm(1) * (xc%npoints - 1)/2 )
      na = (j-1)*dn
      nb = j*dn
      !write(*, *) j, na, nm(1), nb
      if(j == xc%npoints) then
        xcenergy = xc%exc_(j)
        vxc(:) = xc%vxc_(j, :)
      else
        if(use_bspline .eq. 1) then
          ! BSPLINE needs at least 5 points for the cubic interpolation,
          ! so we have to be careful with the array's boundaries
          do i = 1, 5
            if(j .le. 3) then
              fexc(i) = xc%exc_(i)
              fvxc(i) = xc%vxc_(i,1)
              x(i) = dn*(i-1)
            else if(j .ge. (xc%npoints-3)) then
              fexc(i) = xc%exc_(xc%npoints-5+i)
              fvxc(i) = xc%vxc_(xc%npoints-5+i,1)
              x(i) = dn*(xc%npoints-5+i)
            else
              fexc(i) = xc%exc_(j+i-3)
              fvxc(i) = xc%vxc_(j+i-3,1)
              x(i) = dn*(j+i-3)
            endif
            !write(*,*) j, x(i), fcn(i), xc%vxc_(i+j-1,1)
          end do

          call int_exc%initialize(x, fexc, kx, iflag)
          if (iflag/=0) then
            write(*,*) 'Error initializing Exc 1D spline: '//get_status_message(iflag)
          end if
          call int_vxc%initialize(x, fvxc, kx, iflag)
          if (iflag/=0) then
            write(*,*) 'Error initializing Vxc 1D spline: '//get_status_message(iflag)
          end if

          x(1) = nm(1)
          call int_exc%evaluate(x(1), idx, val, iflag)
          if (iflag/=0) then
            write(*,*) 'Error computing Exc 1D spline: '//get_status_message(iflag)
          end if
          xcenergy = xc%exc_(j) + val
          call int_vxc%evaluate(x(1), idx, val, iflag)
          if (iflag/=0) then
            write(*,*) 'Error computing Vxc 1D spline: '//get_status_message(iflag)
          end if
          vxc(1) = xc%vxc_(j, 1) + val
          !write(*,*) x(1), fcn(1), xc%vxc_(j,1), val

          call int_exc%destroy()
          call int_vxc%destroy()
        else
          xcenergy = xc%exc_(j) + ((xc%exc_(j+1) - xc%exc_(j)) / (nb - na)) * (nm(1) - na)
          vxc(:) = xc%vxc_(j, :) + ((xc%vxc_(j+1, :) - xc%vxc_(j, :)) / (nb - na)) * (nm(1) - na)
          !write(*, *) j, xc%exc_(j), xcenergy, xc%exc_(j+1)
        end if
      end if
      vxc(one_basis%p) = 0.0_8
      xcintegral = 0.0_8
      do j = 1, one_basis%p
        xcintegral = xcintegral + nm(j)*vxc(j)
      end do

    else

      select case(xc%parametrization)

      case(XC_PARAMS_TAYLOR_HD)
        xcenergy = 0.0_8
        xcintegral = 0.0_8
        vxc = 0.0_8
        do k = 1, xc%nparams
          do j = 1, one_basis%p
            xcenergy = xcenergy + xc%lambda(k) * nm(j)**(k+1)
            vxc(j) = vxc(j) + (k+1) * xc%lambda(k) * nm(j)**k
          end do
        end do
        do j = 1, one_basis%p
          xcintegral = xcintegral + vxc(j) * nm(j)
        end do

      case default
        xcenergy = 0.0_8
        xcintegral = 0.0_8
        do m = 1, one_basis%p
          do n = 1, one_basis%p
            xcenergy = xcenergy + 0.5_8 * xc%lambda(abs(m-n)) * nm(m) * nm(n)
            xcintegral = xcintegral + xc%lambda(abs(m-n)) * nm(m) * nm(n)
          end do
        end do
      end select

    end if

  end subroutine uxcenergy

  !> Computation of the exchange and correlation potential. Right now, we assume
  !! the exact-exchange approximation for two electrons.
  !! @param[in] nm The array with the density of the system.
  !! @param[in] xc The variable with the information about \f$E_{xc}\f$.
  !! @param[out] vxc The array with the values of \f$V_{xc}[n]\f$.
  !! @todo Link bspline and check for the 1D case that everything goes well.
  subroutine compute_vxc(nm, xc, vxc)
    real(8), intent(in) :: nm(:)
    type(xc_t), intent(in) :: xc
    real(8), intent(inout) :: vxc(:)

    real(8) :: dn, na, nb
    integer :: m, n, j, k

    ! BSPLINE variables
    type(bspline_1d) :: int1d
    integer :: use_bspline, kx, idx, iflag, i
    real(wp):: fcn(5), x(5), val
    kx=4
    idx = 0
    use_bspline = 0; call read_variable('use_bspline', ivar = use_bspline)

    if(xc%mode == XC_EXACT) then

      dn = 2.0_8 / ( xc%npoints - 1 )
      j = 1 + int( nm(1) * (xc%npoints - 1)/2 )
      na = (j-1)*dn
      nb = j*dn
      !write(*, *) j, na, nm(1), nb
      if(j == xc%npoints) then
        vxc(1) = xc%vxc_(j, 1)
      else
        if(use_bspline .eq. 1) then
          ! BSPLINE needs at least 5 points for the cubic interpolation,
          ! so we have to be careful with the array's boundaries
          do i = 1, 5
            if(j .le. 3) then
              fcn(i) = xc%vxc_(i,1)
              x(i) = dn*(i-1)
            else if(j .ge. (xc%npoints-3)) then
              fcn(i) = xc%vxc_(xc%npoints-5+i,1)
              x(i) = dn*(xc%npoints-5+i)
            else
              fcn(i) = xc%vxc_(j+i-3,1)
              x(i) = dn*(j+i-3)
            endif
            !write(*,*) j, x(i), fcn(i), xc%vxc_(i+j-1,1)
          end do

          call int1d%initialize(x, fcn, kx, iflag)
          if (iflag/=0) then
            write(*,*) 'Error initializing 1D spline: '//get_status_message(iflag)
          end if

          x(1) = nm(1)
          call int1d%evaluate(x(1), idx, val, iflag)
          if (iflag/=0) then
            write(*,*) 'Error computing 1D spline: '//get_status_message(iflag)
          end if
          vxc(1) = xc%vxc_(j, 1) + val
          !write(*,*) x(1), fcn(1), xc%vxc_(j,1), val

          call int1d%destroy()
        else
          vxc(1) = xc%vxc_(j, 1) + ((xc%vxc_(j+1, 1) - xc%vxc_(j, 1)) / (nb - na)) * (nm(1) - na)
          !write(*,*) j, vxc(1)
        end if
        vxc(grid%p) = 0.0_8

      end if
    else 

      select case(xc%parametrization)

      case(XC_PARAMS_TAYLOR_HD)
        vxc = 0.0_8
        do k = 1, xc%nparams
          do j = 1, one_basis%p-1
            vxc(j) = vxc(j) + (k+1) * xc%lambda(k) * nm(j)**k
          end do
        end do

      case default
        do m = 1, one_basis%p
          vxc(m) = 0.0_8
          do n = 1, one_basis%p
           vxc(m) = vxc(m) + xc%lambda(abs(m-n))* nm(n)
          end do
        end do

      end select

    end if

  end subroutine compute_vxc

  !> Right now this subroutine writes \f$E_{xc}[n]\f$ and its correspondent density.
  !! @param[in] xc The variable with the information about \f$E_{xc}\f$.
  !! @param[in] filename The filename for the file where we want to store \f$E_{xc}[n]\f$.
  !! @todo Hardcoded for 2 sites, rewrite it for p sites.
  subroutine xc_write(xc, filename)
    type(xc_t), intent(in) :: xc
    character(len=*), intent(in) :: filename

    integer :: j, npoints
    real(8) :: dn, xcenergy, xcintegral
    real(8), allocatable :: n0(:)

    allocate(n0(grid%p))

    npoints = 1001
    dn = 2.0_8 / (npoints - 1)

    open(unit = 11, file = trim(filename))
    do j = 1, npoints
      n0(1) = dn * (j-1)
      n0(2) = 2.0_8 - n0(1)
      call uxcenergy(xc, n0, xcenergy, xcintegral)
      write(11, *) j, n0(1:grid%p-1), xcenergy
    end do
    close(unit = 11)

    deallocate(n0)
  end subroutine xc_write

end module xc_m
