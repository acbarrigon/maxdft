!> main program lattice
!! @mainpage
!!
!! The lattice program implements "lattice DFT", or "occupation number functional
!! theory, as it is sometimes called.
program lattice
  use basis_m
  use dft_m
  use grid_m  
  use hubbarddimer_m
  use input_m
  use many_m
  use maxdft_m
  use ming_m
  use universal_m

  implicit none

  integer, parameter :: &
    RUN_MODE_HUBBARDIMER = 0, &
    RUN_MODE_MING        = 1, &
    RUN_MODE_MAXDFT      = 2, &
    RUN_MODE_UNIVERSAL   = 3, &
    RUN_MODE_SCHRODINGER = 4, &
    RUN_MODE_DFT         = 5

  integer :: p, N
  real(8) :: L
  integer :: run_mode   !> run_mode documentation

  ! Number of electrons
  N = 2; call read_variable('N', ivar = N)

  ! Number of lattice points
  p = 2; call read_variable('p', ivar = p)

  ! Length of simulation box. This value is set to make the 2t = 1.
  L = 3.0; call read_variable('L', rvar = L)

  call grid_init(-L/2.0_8, L/2.0_8, p)
  call grid_write_info('grid')

  call one_basis_init()
  call one_basis_write_info('one_basis')

  call basis_init(N)
  call basis_write_info('basis')

  run_mode = 0; call read_variable('run_mode', ivar = run_mode)

  select case(run_mode)

  case(RUN_MODE_HUBBARDIMER)
    call hubbarddimer_run
  case(RUN_MODE_MING)
    call ming_run
  case(RUN_MODE_MAXDFT)
    call maxdft_run
  case(RUN_MODE_UNIVERSAL)
    call universal_run
  case(RUN_MODE_SCHRODINGER)
    call many_run
  case(RUN_MODE_DFT)
    call dft_run
  end select

end program lattice
