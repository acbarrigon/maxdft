!> This module computes the universal functional F of the the Hubbard dimer, replicating the results given
!! in Fig. 7 of [D. J. Carrascal et al., J. Phys.: Condens. Matter 27, 393001 (2015)].
module universal_m
  use input_m
  use grid_m
  use basis_m
  use operator_m
  use many_m
  use hartree_m
  use exactfunctionals_m
  use xc_m
  use potential_m

  implicit none

  private
  public :: &
    universal_run

  contains

  subroutine universal_run

  integer :: i, j, k, ires, iunit, npoints, nsteps
  integer(8) :: opt, local_opt
  real(8) :: dn, fn, val, deltav, energy, energy0
  real(8), allocatable :: gamma(:), n0(:, :), nmin(:), &
                          vvalues(:), n1(:,:), universalf(:), ts(:), vxc(:)
  type(operator_t) :: kin, w, v, w0
  real(8) :: xcenergy, xcintegral
  type(xc_t) :: xc
  include 'nlopt.f'

  write(*,*)'**********************'
  write(*,*)'*   UNIVERSAL'
  write(*,*)'**********************'
  write(*,*)''

  call operator_init_kinetic(kin)
  call operator_write_info(kin, 'kinetic')

  allocate(gamma(0:grid%p-1))
  gamma = 0.0_8
  ! We define first the zero interaction, to be used as a reference.
  call operator_init_w(w0, gamma)

  ! Init gamma. This will be read from the input file.
  call gamma_init(gamma, from_file = .true.)
  call operator_init_w(w, gamma)

  allocate(nmin(grid%p))
  allocate(vvalues(grid%p))

  ! First, we will get the functional F by using the formula F[n] = E[n] - V[n]
  write(*,*)'    Computing F[n] using the formula F[n] = E[n] - V[n]'
  write(*,*)'      Ts[n] will be stored in ts and F[n] will be stored in fn'
  write(*,*)'    ...'
  npoints = 201
  deltav = 0.1_8
  allocate(n1(npoints, grid%p))
  allocate(universalf(npoints))
  allocate(ts(npoints))
  call exactfunctionals1(kin, w0, npoints, deltav, n1, ts)
  iunit = 11
  open(unit = iunit, file = 'ts')
  do j = 1, npoints
    write(iunit, *) n1(j,1:grid%p-1), ts(j)
  end do
  close(unit = iunit)

  call exactfunctionals1(kin, w, npoints, deltav, n1, universalf)
  iunit = 11
  open(unit = iunit, file = 'fn')
  do j = 1, npoints
    write(iunit, *) n1(j,1:grid%p-1), universalf(j)
  end do
  close(unit = iunit)
 
  deallocate(ts)
  deallocate(universalf)
  deallocatE(n1)

  write(*,*)'    DONE'
  write(*,*)''

  ! Then, we get the functional F by using the formula:
  !
  ! F[n0] = min_{Psi -> n0} <Psi|T+W|Psi>
  write(*,*)'    Computing F[n] minimizing F[n] = min_{Psi -> n} <Psi|T+W|Psi>'
  write(*,*)'      Ts[n] will be stored in ts2 and F[n] will be stored in fn2'
  write(*,*)'    ...'
  if (grid%p .eq. 2) then
    npoints = 101
    nsteps = npoints
    dn = 1.0_8 / (npoints-1)
  else if (grid%p .eq. 3) then
    npoints = 101
    nsteps = npoints*(npoints+1)/2
    dn = 2.0_8 / (npoints-1)
  endif
  allocate(n0(nsteps, grid%p))
  call density_init(npoints, nsteps, dn, n0)

  allocate(universalf(nsteps))
  allocate(ts(nsteps))
  call exactfunctionals2(kin, w, nsteps, n0, universalf)
  iunit = 11
  open(unit = iunit, file = 'fn2')
  do j = 1, nsteps
    write(iunit, *) n0(j, 1:grid%p-1), universalf(j)
  end do
  close(unit = iunit)

  call exactfunctionals2(kin, w0, nsteps, n0, ts)
  iunit = 11
  open(unit = iunit, file = 'ts2')
  do j = 1, nsteps
    write(iunit, *) n0(j, 1:grid%p-1), ts(j)
  end do
  close(unit = iunit)

  write(*,*)'    DONE'
  write(*,*)''

  write(*,*)'    Writing Exc = F - Ts - U in file exc2'
  iunit = 11
  open(unit = iunit, file = 'exc2')
  do j = 1, nsteps
    write(iunit, *) n0(j, :), universalf(j)  - ts(j) - uenergy(gamma, n0(j, :))
  end do
  close(iunit)

  write(*,*)'    Computing Vxc interpolating Exc and taking its derivative'
  write(*,*)'      The interpolated Exc and Vxc will be stored in exc-interpolated'
  write(*,*)'    ...'
  call xc_init(xc, gamma = gamma, mode = XC_EXACT, kin = kin, w = w)
  if (grid%p .eq. 2) then
    npoints = 1001
    nsteps = npoints
  else if (grid%p .eq. 3) then
    npoints = 1001
    nsteps = npoints*(npoints+1)/2
  endif
  deallocate(n0)
  allocate(n0(nsteps, grid%p))
  allocate(vxc(grid%p))
  dn = real(basis%N, 8) / (npoints-1)
  iunit = 11
  open(unit = iunit, file = 'exc-interpolated')
  call density_init(npoints, nsteps, dn, n0)
  do j = 1, nsteps
    call uxcenergy(xc, n0(j, 1:grid%p), xcenergy, xcintegral)
    call compute_vxc(n0(j, 1:grid%p), xc, vxc)
    write(iunit, *) n0(j, 1:grid%p-1), xcenergy, vxc(1:grid%p-1)
  end do
  close(unit = iunit)

  write(*,*)'    DONE'
  write(*,*)''

  open(unit = iunit, file = 'exc-in-xc')
  do j = 1, xc%npoints
    write(iunit, *) n0(j, 1:grid%p-1), xc%exc_(j), xc%vxc_(j, 1:grid%p-1)
  end do
  close(unit = iunit)
  deallocate(vxc)


  call operator_end(kin)
  call operator_end(w)
  call grid_end()
  deallocate(gamma)
  deallocate(n0)
  deallocate(nmin)
  deallocate(vvalues)
  deallocate(universalf)
  deallocate(ts)

  write(*,*)'**********************'
  write(*,*)'*   UNIVERSAL END'
  write(*,*)'**********************'
  end subroutine universal_run

end module universal_m
