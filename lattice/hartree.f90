module hartree_m
  use basis_m

  implicit none

  private
  public ::    &
    uenergy,   &
    compute_vh

  contains

  ! Computation of the Hartree energy:
  ! U[n] = 0.5 * sum_{mn} w_{mn} * n_m * n_n
  real(8) function uenergy(gamma, nm)
    real(8), intent(in) :: gamma(0:)
    real(8), intent(in) :: nm(:)

    integer m, n

    uenergy = 0.0_8
    do m = 1, one_basis%p
      do n = 1, one_basis%p
        uenergy = uenergy + 0.5_8 * gamma(abs(m-n)) * nm(m) * nm(n)
      end do
    end do
  end function uenergy

  ! Computation of the Hartree potential.
  subroutine compute_vh(nm, gamma, vh)
    real(8), intent(in) :: nm(:)
    real(8), intent(in) :: gamma(0:)
    real(8), intent(inout) :: vh(:)

    integer :: m, n

    do m = 1, one_basis%p
      vh(m) = 0.0_8
      do n = 1, one_basis%p
        vh(m) = vh(m) + gamma(abs(m-n))* nm(n)
      end do
    end do

  end subroutine compute_vh

end module hartree_m
