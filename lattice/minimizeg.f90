module minimizeg_m
  use grid_m
  use basis_m
  use xc_m
  use operator_m
  use gfunction_m

  implicit none

  private
  public ::        &
    minimizeg,     &
    gradient_check

  contains

  subroutine minimizeg(kin, xc, v, lb, ub, gamma, g0)
    type(operator_t), intent(in) :: kin
    type(xc_t), intent(in) :: xc
    type(operator_t), intent(in) :: v(:)
    real(8), intent(in) :: lb(:), ub(:)
    real(8), intent(inout) :: gamma(0:)
    real(8), intent(out) :: g0

    integer(8) :: opt
    integer :: p, ires
    include 'nlopt.f'

    p = grid%p

    !call nlo_create(opt, NLOPT_LN_BOBYQA, p-1)
    !call nlo_create(opt, NLOPT_GN_DIRECT, p-1)
    call nlo_create(opt, NLOPT_LD_SLSQP, p-1)

    call nlo_set_lower_bounds(ires, opt, lb)
    if(ires.ne.1) then
      write(*, *) 'NLOPT error at nlo_set_lower_bounds. code = ', ires
      stop
    end if
    call nlo_set_upper_bounds(ires, opt, ub)
    if(ires.ne.1) then
      write(*, *) 'NLOPT error at nlo_set_upper_bounds. code = ', ires
      stop
    end if
    call nlo_set_min_objective(ires, opt, gfunc, 0)
    if(ires.ne.1) then
      write(*, *) 'NLOPT error at nlo_set_min_objective. code = ', ires
      stop
    end if
    call nlo_set_xtol_abs(ires, opt, 1.0e-8_8)
    if(ires.ne.1) then
      write(*, *) 'NLOPT error at nlo_set_xtol_abs. code = ', ires
      stop
    end if
    call nlo_set_maxeval(ires, opt, 1000)
    if(ires.ne.1) then
      write(*, *) 'NLOPT error at nlo_set_maxeval. code = ', ires
      stop
    end if

    call nlo_optimize(ires, opt, gamma(0:p-1), g0)

    contains

    subroutine gfunc(val, n, x, grad, need_gradient, f_data)
      real(8) :: val, x(n), grad(n), f_data
      integer :: n, need_gradient

      type(operator_t) :: w
      real(8), allocatable :: gamma(:)

      write(*, *)
      write(*, *) 'gfunc: x = ', x(1:n)

      allocate(gamma(0:n))
      gamma = 0.0_8
      gamma(0:n-1) = x(1:n)

      call operator_init_w(w, gamma)
      call gfunction(w, v, xc, kin, gamma, val)
      write(*, *) 'gfunc: val = ', val
      if(need_gradient .eq. 1) then
        call ggradient(w, v, xc, kin, gamma, GGRADIENT_METHOD_STERNHEIMER, val, grad)
        write(*, *) 'gfunc: |grad gfunc| = ', sqrt(dot_product(grad, grad))
      end if
      write(*, *)

      call operator_end(w)

      deallocate(gamma)
    end subroutine gfunc

  end subroutine minimizeg


  subroutine gradient_check(kin, xc, v, gamma)
    type(operator_t), intent(in) :: kin
    type(xc_t), intent(in) :: xc
    type(operator_t), intent(in) :: v(:)
    real(8), intent(inout) :: gamma(0:)

    real(8) :: g, gplus, gminus, delta, val
    real(8), allocatable :: gammaplus(:), gammaminus(:), grad(:)
    type(operator_t) :: w


    allocate(gammaplus(0:grid%p-1))
    allocate(gammaminus(0:grid%p-1))
    allocate(grad(0:grid%p-2))

    call operator_init_w(w, gamma)
    call gfunction(w, v, xc, kin, gamma, g)
    call ggradient(w, v, xc, kin, gamma, GGRADIENT_METHOD_FD, val, grad)
    write(*, *) 'GRADIENT COMPUTED WITH FINITE DIFFERENCES: '
    write(*, *) 'g = ', g
    write(*, *) 'dg = ', grad(:)
    write(*, *)
    call ggradient(w, v, xc, kin, gamma, GGRADIENT_METHOD_STERNHEIMER, val, grad)
    write(*, *) 'GRADIENT COMPUTED WITH STERNHEIMER: '
    write(*, *) 'g = ', g
    write(*, *) 'dg = ', grad(:)
    write(*, *)
    call operator_end(w)

    delta = 0.00001_8
    gammaplus = gamma
    gammaplus(0) = gammaplus(0) + delta
    gammaminus = gamma
    gammaminus(0) = gammaminus(0) - delta
    call operator_init_w(w, gammaplus)
    call gfunction(w, v, xc, kin, gamma, gplus)
    call operator_end(w)
    call operator_init_w(w, gammaminus)
    call gfunction(w, v, xc, kin, gamma, gminus)
    call operator_end(w)

    write(*, *) 'GRADIENT COMPUTED WITH FINITE DIFFERENCES: '
    write(*, *) 'dg = ', (gplus - gminus)/(2.0_8 * delta)
    write(*, *)

    deallocate(gammaplus)
    deallocatE(gammaminus)
  end subroutine gradient_check

end module minimizeg_m

