!> This module contains the subroutines that initialize the values of the
!! external potential of the system and the interaction potential of the electrons.
module potential_m
  use input_m
  use grid_m
  use basis_m

  implicit none

  private
  public ::    &
  v_init,      &
  gamma_init

  contains

  !> Initializes the values \f$vvalues(i)\f$ \f$(i=1,...,p)\f$ of the external potential
  !! \f$V\f$ according to the initialization method specified in the input variable
  !! \f$pot\_type\f$.
  !!
  !! Currently there are three ways of initializing the potential:\n
  !!  -random: Random initialization of the external potential. If \f$v\_init\f$
  !!   is called with the optional argument \f$rand\_seed\f$ each call to \f$v\_init\f$
  !!   will return different values for \f$vvalues\f$, else it will assign the
  !!   same sequence every call.\n
  !!  -dimer: Initializes the external potential according to the potential
  !!   generated by two nuclei in the extremes of the grid:
  !!   \f[
  !!    \rm{vvalues}(i) = \frac{1}{\sqrt{1 + (l(p-i))^{2}}} + \frac{1}{\sqrt{1 + (l(1-i))^{2}}}.
  !!   \f]
  !!   Requires the optional argument \f$l\f$ which is the grid parameter.\n
  !!  -linear: Initializes the external potential as a straight line going from
  !!   the input value \f$lin\_pot\f$ to \f$-lin\_pot\f$:
  !!   \f[
  !!    \rm{vvalues}(i) = \rm{linpot} - \Delta v(i-1),\qquad\Delta v = \frac{2linpot}{p-1}.
  !!   \f]
  !!   If called with the optional argument \f$k\f$, \f$vvalues\f$ is multiplied by \f$k\f$.
  !!
  !! @param[inout] vvalues The array holding the \f$p\f$ values of the external potential.
  !! @param[in] k (OPTIONAL) Integer that modifies the slope of the 'linear' initialization
  !! by \f$k\f$.
  !! @param[in] l (OPTIONAL) Real(8) with the value of the grid parameter.
  !! @param[in] rand_seed (OPTIONAL) Integer used to initialize the random number generator
  !! using the system clock if present.
  subroutine v_init(vvalues, k, l, rand_seed)
    real(8), intent(inout) :: vvalues(:)
    integer, intent(in), optional :: rand_seed
    real(8), intent(in), optional :: k, l

    integer :: i, n, clock
    integer, allocatable :: seed(:)
    real(8) :: delta_v, lin_pot
    character(len = 32) :: pot_type

    pot_type = 'linear'; call read_variable('pot_type', svar = pot_type)

    select case(pot_type)

    case('random')
      ! Random initialization of the vvalues vector
      if(present(rand_seed)) then
        call random_seed(size=n)
        call system_clock(COUNT=clock)
        allocate(seed(n), source = clock + 37 * [(i, i = 0,n-1)])
        call random_seed(put=seed)
        do i = 1, grid%p
          call random_number(vvalues(i))
        end do
      else
        do i = 1, grid%p
          call random_number(vvalues(i))
        end do
      end if

    case('dimer')
      ! External potential generated by a dimer
      ! with the nuclei in the extremes of the grid
      do i = 1, grid%p
        vvalues(i) = 1.0/sqrt(1.0_8 + (l*(grid%p-i))**2) + 1.0/sqrt(1.0_8 + (l*(1-i))**2)
      end do

    case('linear')
      ! External potential starting at p=0 with value
      ! lin_pot and decreasing until -lin_pot
      lin_pot = 1.5; call read_variable('lin_pot', rvar = lin_pot)
      delta_v = lin_pot * 2 / (grid%p-1)
      if (present(k)) then
        do i = 1, grid%p
          vvalues(i) = k * (lin_pot - delta_v * (i-1))
        end do
      else
        do i = 1, grid%p
          vvalues(i) = lin_pot - delta_v * (i-1)
        end do
      end if

    case default
      write(*,*) 'pot_type variable does not match with the expected potential names'
      write(*,*)
      stop

    end select

  end subroutine v_init

  !> Initializes the values \f$gamma(i)\f$ \f$(i=0,...,p-1)\f$ of the electronic
  !! interaction potential \f$W\f$. Right now the only initializations avaible
  !! are: (1) the random initialization of the values of \f$gamma\f$, except for the
  !! last value \f$gamma(p-1)\f$, which is set to 0; and (2) the initialization from
  !! values given by the user in the input file, through the "gamma" input variable, which
  !! should be a list of p real numbes, assigned consecutively to gamma(0),...,gamma(p-1).
  !!
  !! @param[inout] gamma The array holding the \f$p\f$ values of the interaction potential.
  !! @param[in] rand_seed (OPTIONAL) Integer used to initialize the random number generator
  !! with the system clock if present.
  !! @param[in] from_file (OPTIONAL) Logical argument used to read the \f$gamma(i)\f$
  !! from the input file.
  subroutine gamma_init(gamma, rand_seed, from_file)
    real(8), intent(inout) :: gamma(0:grid%p-1)
    integer, intent(in), optional :: rand_seed
    logical, optional, intent(in) :: from_file

    logical :: from_file_
    integer :: i, n, clock
    integer, allocatable :: seed(:)

    if(present(from_file)) then
       from_file_ = from_file
    else
       from_file_ = .false.
    end if

    if(from_file_) then
      gamma = 0.0_8; call read_variable('gamma', avar = gamma(0:grid%p-1))
    else if(present(rand_seed)) then
      call random_seed(size=n)
      call system_clock(COUNT=clock)
      allocate(seed(n), source = clock + 37 * [(i, i = 0,n-1)])
      call random_seed(put=seed)
      do i = 0, grid%p-2
        call random_number(gamma(i))
      end do
    else
      do i = 0, grid%p-2
        call random_number(gamma(i))
      end do
    end if
    gamma(grid%p-1) = 0

  end subroutine gamma_init


  end module
