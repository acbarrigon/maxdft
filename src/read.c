/*

Copyright 2015, Adrián Gómez Pueyo and Alberto Castro

This file is part of maxdft.

maxdft is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

maxdft is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
maxdft.  If not, see <http://www.gnu.org/licenses/>.

*/


#include <stdio.h>
#include <stdlib.h>

#include "parallel.h"
#include "grid.h"
#include "read.h"

void leeDFT(double *n)
{

  char tirar[8];
  int i;
  double a;
  FILE *Fdata;

  if((Fdata=fopen("static/density.y=0,z=0","rt"))==NULL){
    printf("Error al abrir el fichero %s\n", "static/density.y=0,z=0");
    parallel_end(EXIT_FAILURE);
  }

  fscanf(Fdata,"%s         %s                     %s                     %s\n",tirar,tirar,tirar,tirar);
  for(i = 0; i < grid.N; i++){
    fscanf(Fdata," %lf  %lf\n", &a, &n[i]);
    if((a != a) || (n[i] != n[i])){
      printf("\n\nError reading the DFT density from the %s file:\n\tOctopus is returning a NaN value for the electronic density\n", "static/density.y=0,z=0");
      exit(2);
    }
  }

  fclose(Fdata);
}


void leeSCH(double *n, int *kpar)
{

  int i;
  double tirar;
  FILE *Fdata;
  char *filename;

  filename = malloc(100*sizeof(char));

  if(kpar == NULL){
    sprintf(filename, "density");
  }else{
    sprintf(filename, "test%d", *kpar);
  }


  if((Fdata=fopen(filename,"rt"))==NULL){
    printf("Error al abrir el fichero %s\n", filename);
#if defined(HAVE_MPI)
    MPI_Finalize();
#endif
    exit(0);
  }

  for(i = 0; i < grid.N; i++){
    fscanf(Fdata," %lf  %lf\n", &tirar, &n[i]);
    if((tirar != tirar) || (n[i] != n[i])){
      printf("\n\nError reading the SCH density from the %s file:\n\tOctopus is returning a NaN value for the electronic density\n", filename);
      exit(2);
    }
  }

  free(filename);
  fclose(Fdata);
}
