/*

Copyright 2015, Adrián Gómez Pueyo and Alberto Castro

This file is part of maxdft.

maxdft is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

maxdft is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
maxdft.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef IPF_H
#define IPF_H


typedef struct
{
  int poten_selector;
    /*
    The possible values are the strictly positive ones defined in usdefspecies_h
    */

  /* Number of parameters for the IPF, These are the variable parameters,
     that change during the minimizations */
  int npar;
  gsl_vector *gamma;

  /* These extra parameters are constant. For example, the soft-Coulomb constant.*/
  int nextrapars;
  gsl_vector *extrapars;

  /* This is the "range", i.e. the maximum r over which the IPF is defined. It will
     be used for printing, and in order to compute distances, etc. */
  double range;

} ipf_t;

ipf_t ipf;
ipf_t ipf_ref;

int ipf_init(ipf_t *ipf, double range, double w0);
int ipf_ref_init(ipf_t *ipf, double range);
void ipf_end(ipf_t *ipf);
void ipf_initial_gamma(ipf_t *ipf);
void ipf_write(ipf_t ipf, ipf_t ipf_ref, char * filename);
int ipf_read_file(gsl_vector *x, char * filename);
double ipf_diff(ipf_t *wa, ipf_t *wb);
void ipf_copy(ipf_t *ipfout, ipf_t *ipfin);


#endif
