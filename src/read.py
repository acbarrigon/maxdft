#!/usr/bin/env python
# -*- coding: utf-8 -*- 
# Copyright 2015, Adrián Gómez Pueyo and Alberto Castro

# This file is part of maxdft.

# maxdft is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# maxdft is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.

# You should have received a copy of the GNU General Public License
# along with maxdft.  If not, see <http://www.gnu.org/licenses/>.

import array
import math
import string
import sys

def DFT():
    """Read the DFT density from a file

    In this function we read from the static/density.y=0,z=0
    file the electronic density of our system computed
    using the KS equations, store it in an array ndft, check
    if we are getting NaN values (usually caused by the minimization
    algorithms when the parameter variation gives high values for the
    IPF) and return this array as output.
    """

    filename = 'static/density.y=0,z=0'

    try:
        fread = open(filename, 'r')
    except IOError:
        print 'Error opening the file ' + filename
        sys.exit(0)

    ndft = array.array('d',[])
    lines = fread.readlines()
    for line in lines[2:]:
        data = string.split(line)
        ndft.append(float(data[1]))

    for i in ndft:
        if math.isnan(i):
            print '\n\nError reading the DFT density from the ' + filename\
            + ' file:'
            print '    Octopus is returning a NaN value for the '\
            + 'electronic density\n'
            sys.exit(2)

    fread.close()

    return ndft


def SCH(kpar):
    """Read the SCH density from a file

    In this function we read from the static/density.y=0,z=0
    file the electronic density of our system computed
    using the SCH equation, store it in an array nsch, check
    if we are getting NaN values (usually caused by the minimization
    algorithms when the parameter variation gives high values for the
    IPF) and return this array as output.
    """

    if kpar is None:
        filename = 'density'
    else:
        filename = 'test' + str(kpar)

    try:
        fread = open(filename, 'r')
    except IOError:
        print 'Error opening the file ' + filename
        sys.exit(0)

    nsch = array.array('d',[])
    for i, line in enumerate(fread):
            data = string.split(line)
            nsch.append(float(data[1]))

    for i in nsch:
        if math.isnan(i):
            print '\n\nError reading the SCH density from the '\
            + filename + ' file:'
            print '    Octopus is returning a NaN value for the '\
            + 'electronic density\n'
            sys.exit(2)

    fread.close()

    return nsch
