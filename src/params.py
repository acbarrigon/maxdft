#!/usr/bin/env python
# -*- coding: utf-8 -*- 
# Copyright 2015, Adrián Gómez Pueyo and Alberto Castro

# This file is part of maxdft.

# maxdft is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.

# maxdft is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with
# maxdft.  If not, see <http://www.gnu.org/licenses/>.

import sys

def param_init():

    print "\n  Grid parameters:"
    l = float(sys.argv[1])
    print "    l = ", l
    step = float(sys.argv[2])
    print "    step = ", step


    print "\n  External potential parameters:"
    alpha = float(sys.argv[3])
    print "    alpha = ", alpha
    lmin = float(sys.argv[4])
    print "    lmin = ", lmin
    lmax = float(sys.argv[5])
    print "    lmax = ",lmax
    delta = float(sys.argv[6])
    print "    delta = ", delta
    npotex = int(sys.argv[7])
    print "    npotex = ", npotex


    print "\n  IPF parameters:"
    npar = int(sys.argv[8])
    print "    npar = ", npar
    poten_selector = int(sys.argv[9])
    print "    poten_selector = ", poten_selector, "\n"

    return l, step, alpha, lmin, lmax, delta, npotex, npar, poten_selector
