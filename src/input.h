/*

Copyright 2015, Adrián Gómez Pueyo and Alberto Castro

This file is part of maxdft.

maxdft is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

maxdft is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
maxdft.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef INPUT_H
#define INPUT_H


void readinput_init();
void readinput_end();
void parse_double (char * keyword, double * value);
void parse_double_array (char * keyword, int size, double * value);
void parse_int (char * keyword, int * value);
void parse_string (char * keyword, char ** value);


#endif
