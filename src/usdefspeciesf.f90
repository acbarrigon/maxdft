module usdefspeciesf_m
  use iso_c_binding

  implicit none

  private
  public :: usdefspecies_init,             &
            usdefspecies_free,             &
            usdefspecies_nparameters,      &
            usdefspecies_nfixedparameters, &
            usdefspecies_parameters,       &
            usdefspecies_value,            &
            usdefspecies_dvalue

  integer, public, parameter :: USDEFSPECIES_HYDROGEN_1E = 0,  &
                        USDEFSPECIES_HYDROGEN_2E = -1

  interface
    integer(C_INT) function usdefspecies_init(s, kind, nparameters, parameters) bind(C, name = 'usdefspecies_init')
      use iso_c_binding
      type(C_PTR) :: s
      integer(C_INT), value :: kind
      integer(C_INT), value :: nparameters
      type(C_PTR), value :: parameters
    end function usdefspecies_init

    subroutine usdefspecies_free(s) bind(C, name = 'usdefspecies_free')
      use iso_c_binding
      type(C_PTR) :: s
    end subroutine usdefspecies_free

    integer(C_INT) function usdefspecies_nparameters(s) bind(C, name = 'usdefspecies_nparameters')
      use iso_c_binding
      type(C_PTR) :: s
    end function usdefspecies_nparameters

    integer(C_INT) function usdefspecies_nfixedparameters(s) bind(C, name = 'usdefspecies_nfixedparameters')
      use iso_c_binding
      type(C_PTR) :: s
    end function usdefspecies_nfixedparameters

    subroutine usdefspecies_parameters(s, parameters) bind(C, name = 'usdefspecies_parameters')
      use iso_c_binding
      type(C_PTR) :: s
      type(C_PTR), value :: parameters
    end subroutine usdefspecies_parameters

    real(C_DOUBLE) function usdefspecies_value(s, x, y, z, r) bind(C, name = 'usdefspecies_value')
      use iso_c_binding
      type(C_PTR) :: s
      real(C_DOUBLE), value :: x
      real(C_DOUBLE), value :: y
      real(C_DOUBLE), value :: z
      real(C_DOUBLE), value :: r
    end function usdefspecies_value

    real(C_DOUBLE) function usdefspecies_dvalue(s, i, x, y, z, r) bind(C, name = 'usdefspecies_dvalue')
      use iso_c_binding
      type(C_PTR) :: s
      integer(C_INT), value :: i
      real(C_DOUBLE), value :: x
      real(C_DOUBLE), value :: y
      real(C_DOUBLE), value :: z
      real(C_DOUBLE), value :: r
    end function usdefspecies_dvalue
  end interface

end module usdefspeciesf_m
