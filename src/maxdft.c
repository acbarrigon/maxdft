/*

Copyright 2015, Adrián Gómez Pueyo and Alberto Castro

This file is part of maxdft.

maxdft is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

maxdft is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
maxdft.  If not, see <http://www.gnu.org/licenses/>.

*/



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_multimin.h>
#include <math.h>

#include "parallel.h"
#include "messages.h"
#include "input.h"
#include "grid.h"
#include "octopus.h"
#include "ipf.h"
#include "extpot.h"
#include "density.h"
#include "inp.h"
#include "compute_density.h"
#include "gfunction.h"
#include "minim.h"
#include "maxdft.h"



int main(int argc, char* argv[])
{
  density_t *ndft, *n0, *nref;
  int runmode, i;
  double ldamix;
  params_gsl_multimin_function_t params;
  gsl_vector *grad;
  FILE *Fdif;

  parallel_init(argc, argv);

  messages_welcome();

  octopus_init(argc, argv);
  readinput_init();
  grid_init(&grid);
  ipf_ref_init(&ipf_ref, 2.0*grid.l);
  ipf_init(&ipf, 2.0*grid.l, gsl_vector_get(ipf_ref.gamma, 0));
  extpot_init(&extpot);
  inp_init();
  gfunction_init();

  runmode = RUNMODE_MINIMIZE_IPF; parse_int ("runmode", &runmode);

  switch(runmode){

  case RUNMODE_CHECK_GRAD :
    messages_basic("\n\n***** Run mode: check gradient *****\n\n");

    /* Reference electronic density calculated using the Kohn-Sham equations  */
    ndft = density_init(grid.N, extpot.npotex);
    compute_ndft(ndft, extpot, 0.0);

    /* Set the initial value for the parameters of the interaction potential */
    ipf_write(ipf, ipf_ref, "initial-pot");

    /* Calculation of n_0 for a given interaction functional */
    n0 = density_init(grid.N, extpot.npotex);
    compute_nsch(n0, extpot, ipf.gamma);

    nref = density_init(grid.N, extpot.npotex);
    density_get_reference(ndft, n0, nref);

    params.nDFT = density_get_val(nref);

    grad = gsl_vector_alloc (ipf.npar);

    my_df (ipf.gamma, &params, grad);

    messages_basic("\n\n***** Gradient *****\n");
    if(myid == 0){
      for(i = 0; i < ipf.npar; i++) printf("%d %.12lf\n", i, gsl_vector_get(grad, i));
    }
    messages_basic("********************\n");
    gsl_vector_free(grad);

    density_free(ndft);
    density_free(n0);
    density_free(nref);

    break;

  case RUNMODE_MINIMIZE_IPF :
    messages_basic("\n\n***** Run mode: minimize IPF *****\n\n");

    /* Reference electronic density calculated using the Kohn-Sham equations  */
    ndft = density_init(grid.N, extpot.npotex);
    compute_ndft(ndft, extpot, 0.0);

    /* Set the initial value for the parameters of the interaction potential */
    ipf_write(ipf, ipf_ref, "initial-pot");

    /* Calculation of n_0 for a given interaction functional */
    n0 = density_init(grid.N, extpot.npotex);
    compute_nsch(n0, extpot, ipf.gamma);

    nref = density_init(grid.N, extpot.npotex);
    density_get_reference(ndft, n0, nref);

    /* Print the diff between initial and reference IPFs */
    if(myid == 0) printf("\nDiff[ IPF(initial), IPF(ref) ] = %.12lg\n", ipf_diff(&ipf, &ipf_ref));

    /* This is the routine that will compute the ipf that minimizes the function */
    minimize_ipf(nref);

    density_free(ndft);
    density_free(n0);
    density_free(nref);

    break;

  case RUNMODE_MINIMIZE_EXC :
    messages_basic("\n\n***** Run mode: minimize EXC *****\n\n");

    ldamix = 0.1; parse_double("ldamix_init", &ldamix);

    if((Fdif=fopen("XC-dif","wt"))==NULL){
      printf("Error al abrir el fichero XC-dif\n");
      exit(0);
    }

    ndft = density_init(grid.N, extpot.npotex);
    n0 = density_init(grid.N, extpot.npotex);
    nref = density_init(grid.N, extpot.npotex);

    while(ldamix <= 1.0){
      if(myid == 0) printf("\n*** Starting minimization for LDAMix = %.2f ***\n", ldamix);
      /* Reference electronic density calculated using the Kohn-Sham equations  */
      compute_ndft(ndft, extpot, ldamix);

      /* Set the initial value for the parameters of the interaction potential */
      ipf_write(ipf, ipf_ref, "initial-pot");

      /* Calculation of n_0 for a given interaction functional */
      compute_nsch(n0, extpot, ipf.gamma);

      density_get_reference(ndft, n0, nref);

      /* Print the diff between initial and reference IPFs */
      if(myid == 0) printf("\nDiff[ IPF(initial), IPF(ref) ] = %.12lg\n", ipf_diff(&ipf, &ipf_ref));

      /* This is the routine that will compute the ipf that minimizes the function */
      minimize_ipf(nref);

      fprintf(Fdif, "%lf\t%lf\n", ldamix, ipf_diff(&ipf, &ipf_ref));

      ldamix += 0.1;
    }

    density_free(ndft);
    density_free(n0);
    density_free(nref);

    fclose(Fdif);

    break;

  }


  fflush(stdout);
  inp_end();
  ipf_end(&ipf);
  ipf_end(&ipf_ref);
  readinput_end();
  octopus_end();
  parallel_end(EXIT_SUCCESS);
  return 0;
}
