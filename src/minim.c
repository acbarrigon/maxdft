/*

Copyright 2015, Adrián Gómez Pueyo and Alberto Castro

This file is part of maxdft.

maxdft is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

maxdift is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
maxdft.  If not, see <http://www.gnu.org/licenses/>.

*/


#include <stdio.h>
#include <stdlib.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_multimin.h>
#include <gsl/gsl_blas.h>
#include <string.h>

#if defined(HAVE_NLOPT) || defined(HAVE_BAYES)
#include <math.h>
#include <nlopt.h>
#endif
#if defined(HAVE_BAYES)
#include <bayesopt/bayesopt.h>
#endif

#include "parallel.h"
#include "messages.h"
#include "input.h"
#include "ipf.h"
#include "grid.h"
#include "extpot.h"
#include "density.h"
#include "gfunction.h"
#include "minim.h"


void minimize_ipf(density_t *ndft){
  int optimization_algorithm;

  optimization_algorithm = 2; parse_int ("optimization_algorithm", &optimization_algorithm);
  gradient_free_mode = 1;     parse_int ("gradient_free_mode", &gradient_free_mode);
  epsilon_grad = 1.0e-3;      parse_double ("epsilon_grad", &epsilon_grad);
  epsilon_gvalue = 1.0e-8;    parse_double ("epsilon_gvalue", &epsilon_gvalue);
  minim_maxiter = 100;        parse_int ("minim_maxiter", &minim_maxiter);

 switch(optimization_algorithm){
  case GRADIENT_BASED :
    minim(ndft);
    break;
  case NLOPT :
#if defined(HAVE_NLOPT) || defined(HAVE_BAYES)
    minim_nlopt(ndft);
#else
    messages_basic("Compiled without nlopt support. Change optimization_algorithm");
    parallel_end(EXIT_FAILURE);
#endif
    break;
  case BAYES :
#if defined(HAVE_BAYES)
    minim_bayes(ndft);
#else
    messages_basic("Compiled without bayesopt support. Change optimization_algorithm");
    parallel_end(EXIT_FAILURE);
#endif
    break;
  default :
    minimd(ndft);
    break;
  }

}

#if defined(HAVE_BAYES)
void minim_bayes(density_t *ndft)
{
  int i;
  double minf;
  double *x;

  bayes_par par_input;
  nlopt_function_data function_data;
  bopt_params par;

  /* Initializing the bounds for the optimization.
     We need two vectors with size ipf.npar to parse
     them to bayesopt */
  par_input.lb = (double *)malloc(ipf.npar*sizeof(double));
  parse_double ("bayes_lb", par_input.lb);
  for(i = 1; i < ipf.npar; i++) par_input.lb[i] = par_input.lb[0];
  par_input.ub = (double *)malloc(ipf.npar*sizeof(double));
  parse_double ("bayes_ub", par_input.ub);
  for(i = 1; i < ipf.npar; i++) par_input.ub[i] = par_input.ub[0];

  x = (double *)malloc(ipf.npar*sizeof(double));
  for(i = 0; i < ipf.npar; i++) x[i] = gsl_vector_get(ipf.gamma, i);

  /* Initializing all the parameters to default, and then modifying them */
  par = initialize_parameters_to_default();

  /* The use of the auxiliar data.n_iter, data.init_samples and data.init_method
     parameters is caused by their par.* equivalents being defined as size_t
     instead of int, which caused some warnings and errors in the parser */
  par.n_iterations = 190;
  parse_int("bayes_iter", &par_input.n_iter);  par.n_iterations = par_input.n_iter;
  par.n_init_samples = 10;
  parse_int("bayes_init_samples", &par_input.init_samples);  par.n_init_samples = par_input.init_samples;
  par.init_method = 1;
  parse_int("bayes_init_method", &par_input.init_method);  par.init_method = par_input.init_method;

  par.random_seed = 0;
  par.verbose_level = 1;  parse_int("bayes_verbose", &par.verbose_level);
  par.noise = 1e-10;  parse_double("bayes_noise", &par.noise);

  par_input.learning = "L_MCMC";  parse_string("bayes_learning", &par_input.learning);
  set_learning(&par, par_input.learning);

  function_data.counter = 0;
  function_data.ndft = density_get_val(ndft);

  messages_basic("\n\n\nStarting the optimization.\n\n\n");

  bayes_optimization(ipf.npar, nlopt_gfunction, &function_data, par_input.lb, par_input.ub, x, &minf, par);

  if(myid == 0)  printf("Success: iterations = %d, G(gamma) = %15.10f\n", function_data.counter, minf);
  if(myid == 0) printf("gamma =  ");
  if(myid == 0) {for(i = 0; i < ipf.npar; i++) printf("%.5f ", x[i]);}
  if(myid == 0)  printf("\n");

  for(i = 0; i < ipf.npar; i++) gsl_vector_set(ipf.gamma, i, x[i]);
  ipf_write(ipf, ipf_ref, "pot");

  parallel_end(EXIT_SUCCESS);
}
#endif

#if defined(HAVE_NLOPT) || defined(HAVE_BAYES)
void minim_nlopt(density_t *ndft){

  nlopt_opt opt;
  double minf;
  int i, status;
  double *x;

  nlopt_function_data function_data;
  nlopt_par par;

  x  = (double *)malloc(ipf.npar*sizeof(double));
  for(i = 0; i < ipf.npar; i++) x[i] = gsl_vector_get(ipf.gamma, i);

  /* Here we choose the minimization method from NLOPT (check the
     avaible algorithms at
     http://ab-initio.mit.edu/wiki/index.php/NLopt_Algorithms)
     and specify the dimension of the problem */
  par.algorithm = 1;  parse_int("nlopt_algorithm", &par.algorithm);
  switch(par.algorithm){
  case 1 :
    opt = nlopt_create(NLOPT_LN_BOBYQA, ipf.npar);
    if(myid == 0)  printf("\n\n%s", nlopt_algorithm_name(NLOPT_LN_BOBYQA));
    break;
  case 2 :
    opt = nlopt_create(NLOPT_GN_DIRECT, ipf.npar);
    if(myid == 0)  printf("\n\n%s", nlopt_algorithm_name(NLOPT_GN_DIRECT));
    break;
  case 3 :
    opt = nlopt_create(NLOPT_GD_STOGO, ipf.npar);
    if(myid == 0)  printf("\n\n%s", nlopt_algorithm_name(NLOPT_GD_STOGO));
    break;
  case 4:
    opt = nlopt_create(NLOPT_GN_ESCH, ipf.npar);
    if(myid == 0)  printf("\n\n%s", nlopt_algorithm_name(NLOPT_GN_ESCH));
    break;
  default :
    if(myid == 0)  printf("\n\nWARNING: wrong choice for the NLOPT algorithm\nTHe optimization will start  with the default algorithm:");
    opt = nlopt_create(NLOPT_GN_DIRECT_L, ipf.npar);
    if(myid == 0)  printf("\n%s", nlopt_algorithm_name(NLOPT_GN_DIRECT_L));
    break;
  }

  nlopt_set_min_objective(opt, nlopt_gfunction, &function_data);

  par.rel_tol = 1e-4;  parse_double("nlopt_rel_tol", &par.rel_tol);
  nlopt_set_xtol_rel(opt, par.rel_tol);

  /* Set the lower and upper bounds of the optimization
     to a single constant lb or ub respectively */
  par.lb = -2.0;  parse_double("nlopt_lb", &par.lb);
  par.ub = 2.0;  parse_double("nlopt_ub", &par.ub);
  nlopt_set_lower_bounds1(opt, par.lb);
  nlopt_set_upper_bounds1(opt, par.ub);

  function_data.counter = 0;
  function_data.ndft = density_get_val(ndft);

  messages_basic("\n\n\nStarting the optimization.\n\n\n");

  if ((status = nlopt_optimize(opt, x, &minf)) <  0){
    if (myid == 0) printf("nlopt failed! status = %d\n", status);
    parallel_end(EXIT_FAILURE);
  }

  if(myid == 0) printf("Success. status = %d, iterations = %d, minf = %.12lg\n", status, function_data.counter, minf);
  if(myid == 0) {for(i = 0; i < ipf.npar; i++) printf("%.5f ", x[i]);}

  nlopt_destroy(opt);

  for(i = 0; i < ipf.npar; i++) gsl_vector_set(ipf.gamma, i, x[i]);
  ipf_write(ipf, ipf_ref, "pot");

  parallel_end(EXIT_SUCCESS);
}
#endif


void minim(density_t * ndft){
  const gsl_multimin_fdfminimizer_type *T;
  gsl_multimin_fdfminimizer *s;
  gsl_multimin_function_fdf my_func;
  gsl_vector * grad_initial;
  size_t iter;

  ipf_t ipf_previous;

  int  status;
  double minimum, g_initial, norm_initial;
  char * output_string;
  int i;
  params_gsl_multimin_function_t params;

  output_string = (char *) malloc(25*sizeof(char));

  params.nDFT = density_get_val(ndft);
  my_func.n = ipf.npar;
  my_func.f = my_f;
  my_func.df = my_df;
  my_func.fdf = my_fdf;
  my_func.params = (void *) (&params);

  /* We use the BFGS algorithm from thee GNU Scientific Library (GSL)
     in its optimized version bfgs2 */
  T = gsl_multimin_fdfminimizer_vector_bfgs2;

  messages_basic("\n\nStarting the optimization.\n\n\n");


  grad_initial = gsl_vector_alloc(ipf.npar);
  my_fdf (ipf.gamma, &params, &g_initial, grad_initial);
  norm_initial = gsl_blas_dnrm2(grad_initial);
  if(g_initial < epsilon_gvalue){
    if(myid == 0) printf("The value of G for the starting gamma is %.12lg,\n", g_initial);
    if(myid == 0) printf("which is already below the requested threshold of %.12lg\n", epsilon_gvalue);
    parallel_end(EXIT_SUCCESS);
  }
  if(norm_initial < epsilon_grad){
    if(myid == 0) printf("The value of the normm of the gradient of G for the starting gamma is %.12lg,\n", norm_initial);
    if(myid == 0) printf("which is already below the requested threshold of %.12lg\n", epsilon_grad);
    parallel_end(EXIT_SUCCESS);
  }
  if(myid == 0) printf("  Starting from gamma =  ");
  if(myid == 0) {for(i = 0; i < ipf.npar; i++) printf ("%.5f ", gsl_vector_get (ipf.gamma, i));}
  if(myid == 0) printf("\n  G(gamma) = %.12lg\n", g_initial);
  if(myid == 0) printf("  ||Grad G(gamma)|| = %.12lg\n\n", norm_initial);


  /* Initialization of the minimizer s for the function
     my_func starting at the x point
     The step for the first try is 0.1
     and the convergence criteria is 0.1 */
  messages_basic("\n\nInitialization of the minimizer.\n\n\n");
  s = gsl_multimin_fdfminimizer_alloc (T, ipf.npar);
  gsl_multimin_fdfminimizer_set (s, &my_func, ipf.gamma, 0.1, 0.1);

  minimum = g_initial;
  iter = 0;
  do{
    iter++;

    ipf_copy(&ipf_previous, &ipf);

    if(myid == 0) printf("  Iter = %d\n", (int)iter);
    if(myid == 0) printf("    starting gamma =  ");
    if(myid == 0) {for(i = 0; i < ipf.npar; i++) printf ("%.5f ", gsl_vector_get (gsl_multimin_fdfminimizer_x(s), i));}
    if(myid == 0) printf("\n    starting G(gamma) = %15.10f\n", minimum);
    /* We make an iteration of the minimizer s */
    status = gsl_multimin_fdfminimizer_iterate (s);
    minimum = gsl_multimin_fdfminimizer_minimum(s);

    if (status){
      if(myid == 0) printf("  Breaking. Reason: %s\n", gsl_strerror(status));
      break;
    }

    if(myid == 0) printf("    ||Grad G(gamma)|| = %15.10f\n", gsl_blas_dnrm2(gsl_multimin_fdfminimizer_gradient(s)));
    if(myid == 0) printf("    G(gamma) = %15.10f\n", minimum);

    /* Checks convergence */
    if(minimum < epsilon_gvalue){
       status = GSL_SUCCESS;
    }else{
       status = gsl_multimin_test_gradient (gsl_multimin_fdfminimizer_gradient(s), epsilon_grad);
    }

    gsl_vector_memcpy(ipf.gamma, gsl_multimin_fdfminimizer_x(s));
    ipf_write(ipf, ipf_ref, "intermediate-pot");


    /* Print the diff between initial and reference IPFs */
    if(myid == 0) printf("    Diff[ IPF(previous), IPF(new) ] = %.12lg\n", ipf_diff(&ipf_previous, &ipf));
    ipf_end(&ipf_previous);

  }
  while (status == GSL_CONTINUE && iter < minim_maxiter);

  if(myid == 0) printf("\n\nFinished optimization. status = %d (%s)\n", status, gsl_strerror(status));
  if(myid == 0) printf("  Final gamma =  ");
  if(myid == 0) {for(i = 0; i < ipf.npar; i++) printf ("%.5f ", gsl_vector_get (gsl_multimin_fdfminimizer_x(s), i));}
  if(myid == 0) printf("\n  With value: G(gamma) = %.12lg\n\n", gsl_multimin_fdfminimizer_minimum(s));

  gsl_vector_memcpy(ipf.gamma, gsl_multimin_fdfminimizer_x(s));
  sprintf(output_string, "pot");
  ipf_write(ipf, ipf_ref, output_string);
  gsl_multimin_fdfminimizer_free (s);
  fflush(stdout);

}


void minimd(density_t * ndft){
  int  status;
  int i;
  double stepmin, minimum, g_initial;
  char * output_string;
  gsl_vector *ss;
  const gsl_multimin_fminimizer_type *T;
  gsl_multimin_fminimizer *s;
  gsl_multimin_function my_func;
  size_t iter;
  params_gsl_multimin_function_t params;

  switch(gradient_free_mode){

  case SIMPLEX :

    output_string = (char *) malloc(25*sizeof(char));

    params.nDFT = density_get_val(ndft);
    my_func.n = ipf.npar;
    my_func.f = my_f;
    my_func.params = (void *) (&params);

    /* Initial step sizes */
    ss = gsl_vector_alloc (ipf.npar);
    gsl_vector_set_all(ss, 0.1);

    /* We use the Simplex algorithm from thee GNU Scientific Library (GSL)
       in its optimized version nmsimplex2 */
    T = gsl_multimin_fminimizer_nmsimplex2;

    messages_basic("\n\n\nStarting the optimization.\n\n\n");


    g_initial = my_f(ipf.gamma, &params);
    if(g_initial < epsilon_gvalue){
      if(myid == 0) printf("The value of G for the starting gamma is %.12lg,\n", g_initial);
      if(myid == 0) printf("which is already below the requested threshold of %.12lg\n", epsilon_gvalue);
      parallel_end(EXIT_SUCCESS);
    }
    if(myid == 0) printf("  Starting from gamma =  ");
    if(myid == 0) {for(i = 0; i < ipf.npar; i++) printf ("%.5f ", gsl_vector_get (ipf.gamma, i));}
    if(myid == 0) printf("\n  G(gamma) = %.12lg\n", g_initial);

    /* Initialization of the minimizer s for the function
       my_func starting at the x point */
    messages_basic("\n\nInitialization of the minimizer.\n\n\n");
    s = gsl_multimin_fminimizer_alloc (T, ipf.npar);
    gsl_multimin_fminimizer_set (s, &my_func, ipf.gamma, ss);


    minimum = g_initial;
    iter = 0;
    do
      {
      iter++;
      if(myid == 0) printf("  Iter = %d\n", (int)iter);
      if(myid == 0) printf("    gamma =  ");
      if(myid == 0) {for(i = 0; i < ipf.npar; i++) printf ("%.5f ", gsl_vector_get (gsl_multimin_fminimizer_x(s), i));}
      if(myid == 0) printf("\n    starting G(gamma) = %15.10lg\n", minimum);
      /* We make an iteration of the minimizer s */
      status = gsl_multimin_fminimizer_iterate (s);
      minimum = gsl_multimin_fminimizer_minimum(s);

      if (status){
        if(myid == 0) printf("  Breaking. Reason: %s\n", gsl_strerror(status));
        break;
      }

      if(myid == 0) printf("    G(gamma) = %15.10f\n", minimum);

      stepmin = gsl_multimin_fminimizer_size (s);
      status = gsl_multimin_test_size (stepmin, 1e-2);

    }
    while (status == GSL_CONTINUE && iter < 100);

    if(myid == 0) printf("\n\nFinished optimization. status = %d (%s)\n", status, gsl_strerror(status));
    if(myid == 0) printf("  Final gamma =  ");
    if(myid == 0) {for(i = 0; i < ipf.npar; i++) printf ("%.5f ", gsl_vector_get (gsl_multimin_fminimizer_x(s), i));}
    if(myid == 0) printf("\n  With value: G(gamma) = %.12lg\n\n", gsl_multimin_fminimizer_minimum(s));

    gsl_vector_memcpy(ipf.gamma, gsl_multimin_fminimizer_x(s));
    sprintf(output_string, "pot");
    ipf_write(ipf, ipf_ref, output_string);
    gsl_multimin_fminimizer_free (s);
    fflush(stdout);

    gsl_vector_free(ss);

    break;

  case GENETIC_ALGORITHM :

    output_string = (char *) malloc(75*sizeof(char));

    sprintf(output_string, "python ga.py %f %f %f %f %f %f %d %d %d",
            grid.l, grid.step, extpot.alpha, extpot.Lmin, extpot.Lmax,
            extpot.delta, extpot.npotex, ipf.npar, ipf.poten_selector);

    system(output_string);
    free(output_string);

    break;
  }
}

