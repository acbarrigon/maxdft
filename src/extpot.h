/*

Copyright 2015, Adrián Gómez Pueyo and Alberto Castro

This file is part of maxdft.

maxdft is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

maxdft is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
maxdft.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef EXTPOT_H
#define EXTPOT_H


typedef struct
{
  double alpha; /* Soft-Coulomb interaction parameter */
  double Lmin;  /* Minimum separation for the nuclei */
  double Lmax;  /* Maximum separation for the nuclei */
  double delta; /* Increase in the separation between the nuclei */
  int npotex;   /* Number of external potentials */
  int initpot, finalpot, npotex_proc;
} extpot_t;

extpot_t extpot;

int extpot_init(extpot_t *extpot);


#endif
