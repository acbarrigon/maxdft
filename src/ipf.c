/*

Copyright 2015, Adrián Gómez Pueyo and Alberto Castro

This file is part of maxdft.

maxdft is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

maxdft is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
maxdft.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_vector.h>

#include "ipf.h"
#include "usdefspecies.h"
#include "input.h"
#include "parallel.h"

double ipf_diff(ipf_t *wa, ipf_t *wb){
  int i, npoints;
  double dr, diff, r;
  double *gamma_a, *gamma_b;
  usdefspecies_t **sa;
  usdefspecies_t **sb;

  sa = (usdefspecies_t **)malloc(sizeof(usdefspecies_t **));
  sb = (usdefspecies_t **)malloc(sizeof(usdefspecies_t **));

  gamma_a = (double *)malloc((wa->npar+wa->nextrapars)*sizeof(double));
  for(i = 0; i < wa->npar ; i++)  gamma_a[i] =  gsl_vector_get(wa->gamma, i);
  for(i = 0; i < wa->nextrapars; i++) gamma_a[wa->npar+i] = gsl_vector_get(wa->extrapars, i);
  usdefspecies_init(sa, wa->poten_selector, wa->npar+wa->nextrapars, gamma_a);

  gamma_b = (double *)malloc((wb->npar+wb->nextrapars)*sizeof(double));
  for(i = 0; i < wb->npar ; i++)  gamma_b[i] =  gsl_vector_get(wb->gamma, i);
  for(i = 0; i < wb->nextrapars; i++) gamma_b[wb->npar+i] = gsl_vector_get(wb->extrapars, i);
  usdefspecies_init(sb, wb->poten_selector, wb->npar+wb->nextrapars, gamma_b);

  npoints = 1000;
  dr = wa->range/npoints;
  diff = 0.0;
  for(i = 0; i < npoints + 1; i++){
    r = dr*i;
    diff = diff + dr * pow(usdefspecies_dvalue_dr(sa, r) - usdefspecies_dvalue_dr(sb, r), 2);
  }
 
  usdefspecies_free(sa);
  usdefspecies_free(sb);
  free(sa);
  free(sb);
  return sqrt(diff);
}

int ipf_init(ipf_t * ipf, double range, double w0){
  double alpha, lambda;
  int i;

  ipf->poten_selector = 1;  parse_int("poten_selector", &(ipf->poten_selector));
  ipf->npar = 3;            parse_int ("npar", &(ipf->npar));
  alpha = 1.0;              parse_double ("alpha", &alpha);
  lambda = 0.0;             parse_double ("taylor_lambda",&lambda);
  ipf->gamma = gsl_vector_alloc(ipf->npar);
  ipf->range = range;

  switch (ipf->poten_selector){
  case USDEFSPECIES_PADE :
    ipf->nextrapars = 0;
    break;
  case USDEFSPECIES_PADE_ONE :
    ipf->nextrapars = 0;
    break;
  case USDEFSPECIES_PADE_FIXED :
    ipf->nextrapars = 1;
    ipf->extrapars = gsl_vector_alloc(ipf->nextrapars);
    gsl_vector_set(ipf->extrapars, 0, w0);
    break;
  case USDEFSPECIES_SC_PADE :
    ipf->nextrapars = 2;
    ipf->extrapars = gsl_vector_alloc(ipf->nextrapars);
    gsl_vector_set(ipf->extrapars, 0, alpha);
    gsl_vector_set(ipf->extrapars, 1, w0);
    break;
  case USDEFSPECIES_TAYLOR_SOFT_COULOMB_2 :
    ipf->nextrapars = 2;
    ipf->extrapars = gsl_vector_alloc(ipf->nextrapars);
    gsl_vector_set(ipf->extrapars, 0, alpha);
    gsl_vector_set(ipf->extrapars, 1, lambda);
    break;
  default :
    ipf->nextrapars = 1;
    ipf->extrapars = gsl_vector_alloc(ipf->nextrapars);
    gsl_vector_set(ipf->extrapars, 0, alpha);
  }

  ipf_initial_gamma(ipf);

  if(myid == 0) {
    printf("\n********** ipf_init\n");
    printf("  IPF: %d.\n", ipf->poten_selector);
    printf("  Initial constant parameters: ");
    for(i=0; i < ipf->nextrapars; i++) printf(" %.12g ", gsl_vector_get(ipf->extrapars, i));
    printf("\n");
    printf("  Initial variable parameters: ");
    for(i=0; i < ipf->npar; i++) printf(" %.12g ", gsl_vector_get(ipf->gamma, i));
    printf("\n");
    printf("**********\n"); 
    fflush(stdout);
  }

  return 0;
};


int ipf_ref_init(ipf_t * ipf_ref, double range){
  double alpha, lambda;

  ipf_ref->poten_selector = USDEFSPECIES_SOFT_COULOMB;
  ipf_ref->npar = 1;
  alpha = 1.0; parse_double ("alpha", &alpha);
  ipf_ref->gamma = gsl_vector_alloc(ipf_ref->npar);
  ipf_ref->range = range;

  ipf_ref->nextrapars = 1;
  ipf_ref->extrapars = gsl_vector_alloc(ipf_ref->nextrapars);
  gsl_vector_set(ipf_ref->extrapars, 0, alpha);

  lambda = 1.0; parse_double ("poisson_lambda", &lambda);
  gsl_vector_set(ipf_ref->gamma, 0, lambda);

  if(myid == 0) {
    printf("\n********** ipf_ref_init\n");
    printf("  Reference IPF: Soft Coulomb with lambda = %.12g\n", lambda);
    printf("                                   alpha  = %.12g\n", alpha);
    printf("**********\n"); 
    fflush(stdout);
  }

  return 0;
};


void ipf_end(ipf_t *ipf){
  gsl_vector_free(ipf->gamma);
  if(ipf->nextrapars > 0) gsl_vector_free(ipf->extrapars);
}


void ipf_initial_gamma(ipf_t *ipf)
{
  int p, i;
  double *gamma;
  int ipf_from;
  char *filename;

  /*
  ipf_from = 0 -> Set initial values to zero.
  ipf_from = 1 -> Read initial values from the array variable "ipf_initial_values".
  ipf_from = 2 -> Read initial values from the file indicated in variable "ipf_from_file".
   */
  ipf_from = 0; parse_int("ipf_from", &ipf_from);

  /* 
   There is very little error handling here.
   */
  switch(ipf_from){
  case 1 :
    gamma = (double *)malloc(ipf->npar * sizeof(double));
    /*printf("xxxxxxxxxxx %d\n", ipf->npar);*/
    parse_double_array("ipf_initial_values", ipf->npar, gamma);
    for(i = 0; i < ipf->npar; i++) gsl_vector_set (ipf->gamma, i, gamma[i]);

    free(gamma);
    break;
  case 2 :
    filename = NULL; parse_string("ipf_from_file", &filename);
    if(!filename) filename = "pot";
    p = ipf_read_file(ipf->gamma, filename); 
    if(p > ipf->npar){
      if(myid == 0) printf("The number of parameters in file %s should be %d", filename, ipf->npar);
      parallel_end(EXIT_FAILURE);
    }
    break;
  default :
    for(i = 0; i < ipf->npar; i++) gsl_vector_set (ipf->gamma, i, 0.0);
    break;
  }

}

void ipf_write(ipf_t ipf, ipf_t ipf_ref, char * filename)
{
  int i, npoints;
  double r, dr;
  FILE *Fdata;
  double *gamma;
  double *gammaref;
  usdefspecies_t **s;
  usdefspecies_t **sref;

  if(myid != 0) return;

  if((Fdata=fopen(filename,"wt"))==NULL){
    printf("Error al abrir un fichero");
    exit(1);
  }

  fprintf(Fdata,"#npar = %d\n",ipf.npar);

  for(i = 0; i < ipf.npar ; i++)  fprintf(Fdata,"#gamma[%d] = %.10lf\n",i, gsl_vector_get(ipf.gamma,i));

  s = (usdefspecies_t **)malloc(sizeof(usdefspecies_t **));
  gamma = (double *)malloc((ipf.npar+ipf.nextrapars)*sizeof(double));
  for(i = 0; i < ipf.npar ; i++)  gamma[i] =  gsl_vector_get(ipf.gamma,i);
  for(i = 0; i < ipf.nextrapars; i++) gamma[ipf.npar+i] = gsl_vector_get(ipf.extrapars, i);
  usdefspecies_init(s, ipf.poten_selector, ipf.npar+ipf.nextrapars, gamma);

  sref = (usdefspecies_t **)malloc(sizeof(usdefspecies_t **));
  gammaref = (double *)malloc((ipf_ref.npar+ipf_ref.nextrapars)*sizeof(double));
  for(i = 0; i < ipf_ref.npar ; i++)  gammaref[i] =  gsl_vector_get(ipf_ref.gamma,i);
  for(i = 0; i < ipf_ref.nextrapars; i++) gammaref[ipf_ref.npar+i] = gsl_vector_get(ipf_ref.extrapars, i);
  usdefspecies_init(sref, ipf_ref.poten_selector, ipf_ref.npar+ipf_ref.nextrapars, gammaref);


  npoints = 1000;
  dr = ipf.range/npoints;
  for(i = 0; i < npoints + 1; i++){
    r = dr*i;
    /*
     Here we plot, r, the IPF, the "reference" IPF, the IPF derivative computed analytically,
     the reference IPF derivative, and the IPF derivative computed numerically.
     */
    fprintf(Fdata,"%.10lf\t%.10lf\t%.10lf\t%.10lf\t%.10lf\t%.10lf\n",
	    r, usdefspecies_value_r(s, r), usdefspecies_value_r(sref, r),
	    usdefspecies_dvalue_dr(s, r), usdefspecies_dvalue_dr(sref, r),
            (usdefspecies_value_r(s, r+dr)-usdefspecies_value_r(s, r))/dr);
  }

  usdefspecies_free(s);
  usdefspecies_free(sref);
  free(s);
  free(sref);
  free(gamma);
  free(gammaref);
  fclose(Fdata);
}

int ipf_read_file(gsl_vector *gamma, char* filename)
{
  int i, np;
  double a;
  char s[8];
  FILE *Finp;

  if((Finp=fopen(filename,"rt"))==NULL){
    printf("Error al abrir el fichero %s\n", filename);
    exit(0);
  }

  fscanf(Finp,"%s = %d\n",s, &np);

  for(i = 0; i < np - 1; i++)
    {
      fscanf(Finp,"%s = %lf\n",s, &a);
      gsl_vector_set(gamma, i, a);
    }
  fscanf(Finp,"%s = %lf\n",s, &a);
  gsl_vector_set(gamma, ipf.npar - 1, a);

  fclose(Finp);

  return np;
}

void ipf_copy(ipf_t *ipfout, ipf_t *ipfin){
  ipfout->poten_selector = ipfin->poten_selector;
  ipfout->npar = ipfin->npar;
  ipfout->gamma = gsl_vector_alloc(ipfout->npar);
  gsl_vector_memcpy(ipfout->gamma, ipfin->gamma);
  ipfout->nextrapars = ipfin->nextrapars;
  if(ipfin->nextrapars > 0){
    ipfout->extrapars = gsl_vector_alloc(ipfout->nextrapars);
    gsl_vector_memcpy(ipfout->extrapars, ipfin->extrapars);
  }
  ipfout->range = ipfin->range;
}
