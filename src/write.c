/*

Copyright 2015, Adrián Gómez Pueyo and Alberto Castro

This file is part of maxdft.

maxdft is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

maxdft is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
maxdft.  If not, see <http://www.gnu.org/licenses/>.

*/


#include <stdio.h>
#include <stdlib.h>
#include <gsl/gsl_vector.h>

#include "grid.h"
#include "octopus.h"
#include "ipf.h"
#include "extpot.h"
#include "inp.h"
#include "read.h"


void writeDFT(double *n, int p)
{
  int i;
  char s[8];
  FILE *Fdata;

  sprintf(s,"nDFT_%d",p);

  if((Fdata=fopen(s,"wt"))==NULL){
    printf("Error al abrir un fichero 5\n");
    exit(2);
  }

  for(i = 0; i < grid.N; i++)  fprintf(Fdata,"%.10lf\t%.10lf\n", grid.l - grid.step*i, *(n + i));

  fclose(Fdata);
}

void writeSCH(const gsl_vector *p)
{
  int i, j;
  char s[8];
  double *n, *gamma;
  FILE *Fdata;

  n = malloc(grid.N*sizeof(double));
  gamma = malloc(ipf.npar*sizeof(double));

  for(i = 0; i < ipf.npar; i++) gamma[i] = gsl_vector_get(p,i);

  for(i = 0; i < extpot.npotex; i++)
    {
      sprintf(s,"nSCH_%d",i);
      
      inpSCH(gamma, i, 0);
      run_octopus(octopus, "out", 1, 0);
      leeSCH(n, NULL);
      
      if((Fdata=fopen(s,"wt"))==NULL){
	printf("Error al abrir un fichero 6\n");
	exit(2);
      }

      for(j = 0; j < grid.N; j++)  fprintf(Fdata,"%.2lf\t%.10lf\n", grid.l - grid.step*j, n[j]);
      
      fclose(Fdata);
    }

  free(n); free(gamma);
}

