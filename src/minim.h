/*

Copyright 2015, Adrián Gómez Pueyo and Alberto Castro

This file is part of maxdft.

maxdft is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

maxdft is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
maxdft.  If not, see <http://www.gnu.org/licenses/>.

*/


#ifndef MINIM_H
#define MINIM_H


#define GRADIENT_BASED 1
#define GRADIENT_FREE  2
#define NLOPT 3
#define BAYES 4

int gradient_free_mode;
#define SIMPLEX 1
#define GENETIC_ALGORITHM 2

double epsilon_grad;
double epsilon_gvalue;
int minim_maxiter;

void minimize_ipf(density_t *ndft);
void minim(density_t *ndft);
void minimd(density_t *ndft);

#if defined(HAVE_NLOPT) || defined(HAVE_BAYES)
void minim_nlopt(density_t *ndft);
typedef struct{
  double lb;
  double ub;
  double rel_tol;
  int algorithm;
} nlopt_par;
#endif

#if defined(HAVE_BAYES)
void minim_bayes(density_t *ndft);
typedef struct{
  double *lb;
  double *ub;
  char *learning;
  int n_iter;
  int init_samples;
  int init_method;
} bayes_par;
#endif

#endif
