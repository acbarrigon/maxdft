/*

Copyright 2015, Adrián Gómez Pueyo and Alberto Castro

This file is part of maxdft.

maxdft is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

maxdft is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
maxdft.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <gsl/gsl_vector.h>

#include "inp.h"
#include "input.h"
#include "usdefspecies.h"
#include "grid.h"
#include "ipf.h"
#include "extpot.h"
#include "maxdft.h"

void inp_init(){
  inpstring_DFT = NULL;  parse_string("dft_inp", &inpstring_DFT);
  inpstring_SCH = NULL;  parse_string("exact_inp", &inpstring_SCH);
}

void inp_end(){
  free(inpstring_DFT);
  free(inpstring_SCH);
}

void inpDFT(int npot, double ldamix)
{
  FILE *Finp;
  int runmode;
  double l;

  if((Finp=fopen("inp","wt"))==NULL){
    printf("Error al abrir un fichero 1\n");
    exit(1);
    }

  runmode = RUNMODE_MINIMIZE_IPF; parse_int("runmode", &runmode);

  if(inpstring_DFT != NULL) fprintf(Finp, "\n%s\n", inpstring_DFT);

  if(runmode == RUNMODE_MINIMIZE_EXC) fprintf(Finp, "\nLDAMix = %f\n", ldamix);


  fprintf(Finp, "CalculationMode = gs\n");
  fprintf(Finp, "Dimensions = 1\n");
  fprintf(Finp, "FromScratch = yes\n\n");

  /* Spatial parameters */
  fprintf(Finp, "BoxShape = parallelepiped\n");
  fprintf(Finp, "lsize = %f\n", grid.l);
  fprintf(Finp, "Spacing = %f\n\n", grid.step);

  /* Output */
  fprintf(Finp, "Output = wfs_sqmod + density + potential\n");
  fprintf(Finp, "OutputFormat = axis_x\n\n");

  fprintf(Finp, "\n\n%%Species\n");
  fprintf(Finp, "\"elec\" | species_usdefspecies | 0 | 1 | %.12g \n", extpot.alpha);
  fprintf(Finp, "%%\n\n");

  l = extpot.Lmin + extpot.delta*npot;
  fprintf(Finp, "%%Coordinates\n");
  fprintf(Finp, "\"elec\" | %.12g\n",  l/2);
  fprintf(Finp, "\"elec\" | %.12g\n", -l/2);
  fprintf(Finp, "%%\n");

  fclose(Finp);
}

void inpSCH(const double *gamma, int npot, int der)
{
  FILE *Finp;
  int i;
  double l;

  l = extpot.Lmin + extpot.delta*npot;

  if((Finp=fopen("inp","wt"))==NULL){
    printf("Error al abrir un fichero 2\n");
    exit(1);
    }

  if(inpstring_SCH != NULL) fprintf(Finp, "\n%s\n", inpstring_SCH);

  /* Calculation mode and units used */
  fprintf(Finp, "ExperimentalFeatures = yes\n");
  fprintf(Finp, "CalculationMode = gs\n");
  fprintf(Finp, "NTypeParticleModelMB = 1\n");
  fprintf(Finp, "NParticleModelMB = 2\n");
  fprintf(Finp, "NDimModelMB = 1\n");
  fprintf(Finp, "Dimensions = 2\n");
  fprintf(Finp, "%%DescribeParticlesModelMB\n");
  fprintf(Finp, " \"electron\" | 1 | 1. | 1. | fermion\n");
  fprintf(Finp, " \"electron\" | 1 | 1. | 1. | fermion\n");
  fprintf(Finp, "\%%\n");
  fprintf(Finp, "%%DensitytoCalc\n");
  fprintf(Finp, "\"electron\" | 1 | 0\n");
  fprintf(Finp, "%%\n");
  fprintf(Finp, "FromScratch = yes\n");
  fprintf(Finp, "TheoryLevel = independent_particles\n\n");

  /* Spatial parameters */
  fprintf(Finp,"BoxShape = parallelepiped\nlsize = %f\nSpacing = %f\n\n", grid.l, grid.step);

  fprintf(Finp, "LCAOStart = lcao_none\n\n");

  fprintf(Finp, "%%Species\n");
  fprintf(Finp, "\"elec\"| species_usdefspecies | %d | 1 ", ipf.poten_selector);
  if( ipf.poten_selector != 1 ){
    for( i = 0; i < ipf.npar ; i++ ) fprintf(Finp, " | %.12g ",gamma[i]) ;
    for( i = 0; i < ipf.nextrapars ; i++ ) fprintf(Finp, " | %.12g ", gsl_vector_get(ipf.extrapars, i)) ;
  }
  else{
    for( i = 0; i < ipf.npar-1 ; i++ ) fprintf(Finp, " | %.12g ",gamma[i]) ;
    fprintf(Finp, " | %.12g ", fabs(gamma[ipf.npar-1])) ;
    for( i = 0; i < ipf.nextrapars ; i++ ) fprintf(Finp, " | %.12g ", gsl_vector_get(ipf.extrapars, i)) ;
  }

  fprintf(Finp, " \n");
  fprintf(Finp, "\"Hydrogen\" | species_usdefspecies | -1 | 0 | %.12g | %.12g\n", extpot.alpha, 0.0 );
  fprintf(Finp, "%%\n\n");

  /* Coordinates */
  fprintf(Finp, "%%Coordinates\n");
  fprintf(Finp, "\"elec\" | 0 | 0 | \n");
  fprintf(Finp, "\"Hydrogen\" |  %.12g | %.12g\n",  l/2.0,  l/2.0);
  fprintf(Finp, "\"Hydrogen\" |  %.12g | %.12g\n", -l/2.0, -l/2.0);
  fprintf(Finp, "%%\n");

  /* Eigensolver iteration controller */
  fprintf(Finp,"EigenSolverMaxIter = 200\n\n");

  if(der) fprintf(Finp, "SCFWDerivatives = yes\n\n");

  fclose(Finp);
}
