/*

Copyright 2015, Adrián Gómez Pueyo and Alberto Castro

This file is part of maxdft.

maxdft is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

maxdft is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
maxdft.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdio.h>
#include <stdlib.h>

#include "extpot.h"
#include "input.h"
#include "parallel.h"

int extpot_init(extpot_t * extpot){

  extpot->alpha = 1.0;         parse_double ("alpha", &(extpot->alpha));
  extpot->Lmin = 1.0;          parse_double ("Lmin", &(extpot->Lmin));
  extpot->Lmax = 1.5;          parse_double ("Lmax", &(extpot->Lmax));
  extpot->delta = 0.5;         parse_double ("delta", &(extpot->delta));

  /* Number of external potentials */
  extpot->npotex = (int) ((extpot->Lmax - extpot->Lmin)/extpot->delta) + 1;

  if ( (extpot->npotex % numprocs) == 0 ){
    extpot->npotex_proc = extpot->npotex / numprocs;
    extpot->initpot = myid * extpot->npotex_proc;
    extpot->finalpot = (myid+1) * extpot->npotex_proc-1;
  }else{
    if (myid == 0) 
      printf("The number of potentials (%d) must be a multiple of the number of processors (%d).\n", extpot->npotex, numprocs);
      parallel_end(EXIT_FAILURE);
  }

  if(myid == 0) printf("\n********** extpot_init\n");
#if defined(HAVE_MPI)
  MPI_Barrier(MPI_COMM_WORLD);
#endif
  fflush(stdout);
  if(myid == 0) printf("  There are %d external potentials.\n", extpot->npotex);
  printf("  Process %d will handle potentials %d - %d\n", myid, extpot->initpot, extpot->finalpot);
  fflush(stdout); 
#if defined(HAVE_MPI)
  MPI_Barrier(MPI_COMM_WORLD);
#endif
  if(myid == 0) {printf("**********\n"); fflush(stdout);}


  return 0;
};
