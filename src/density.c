/*

Copyright 2015, Adrián Gómez Pueyo and Alberto Castro

This file is part of maxdft.

maxdft is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

maxdft is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
maxdft.  If not, see <http://www.gnu.org/licenses/>.

*/


#include <stdlib.h>

#include "density.h"
#include "input.h"


int density_free(density_t * dens){
  free(dens->val);
  free(dens);
  return 0;
}

density_t * density_init(int grid_size, int m){
  density_t * dens = (density_t *) malloc(sizeof(density_t));

  dens->npoints = grid_size;
  dens->m = m;
  dens->val = (double *)malloc(sizeof(double)* grid_size * m);

  return dens;
}

double * density_get_val(density_t * dens){
  return dens-> val;
}

void density_get_reference(density_t * ndft, density_t * n0, density_t * nref){
  double eps;
  int i;
  eps = 0.1; parse_double ("eps", &eps);
  for(i = 0; i < ndft->npoints * ndft->m ; i++) nref->val[i] = (1.0-eps) * n0->val[i] + eps * ndft->val[i];
}
