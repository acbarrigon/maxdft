/*

Copyright 2015, Adrián Gómez Pueyo and Alberto Castro

This file is part of maxdft.

maxdft is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

maxdft is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
maxdft.  If not, see <http://www.gnu.org/licenses/>.

*/


#ifndef GFUNCTION_H
#define GFUNCTION_H

typedef struct {
  double a, b;
} my_constraint_data;

typedef struct {
  int counter;
  double *ndft;
} nlopt_function_data;

typedef struct{
  double *nDFT;
} params_gsl_multimin_function_t;

typedef struct{
  int i;
  double *nDFT;
  gsl_vector *x;
} param_function_t;

int gradient_mode;
#define GRADIENT_PRECISE 2
#define GRADIENT_STERNHEIMER 3

double delta_numder;

void gfunction_init();

double my_f(const gsl_vector *v, void *params);
void my_df (const gsl_vector *v, void *params, gsl_vector *df);
void my_fdf (const gsl_vector *x, void *params, double *f, gsl_vector *df);

double nlopt_gfunction(unsigned n, const double *x, double *grad, void *my_func_data);
/* double nlopt_constraint(unsigned n, const double *x, double *grad, void *data); */

#endif
