/*

Copyright 2015, Adrián Gómez Pueyo and Alberto Castro

This file is part of maxdft.

maxdft is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

maxdft is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
maxdft.  If not, see <http://www.gnu.org/licenses/>.

*/


#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <wait.h>
#include <string.h>

#include "parallel.h"
#include "octopus.h"

int octopus_init(int argc, char* argv[]){
  char filename[25];

  /* The code will run the octopus command given by the first command line
     argument. If it is not present, then it will use whatever is in the
     environment variable "OCTOPUS", if it is defined. Otherwise, it try
     running just "./octopus".
  */
  if(argc > 1)
  {
    octopus = (char *) malloc(strlen(argv[1]) + 1);
    strcpy(octopus, argv[1]);
  }
  else if(getenv("OCTOPUS") != NULL)
  {
    octopus = (char *) malloc(strlen(getenv("OCTOPUS")));
    strcpy(octopus, getenv("OCTOPUS"));
  }else{
    octopus = (char *) malloc(strlen("./octopus") + 1);
    strcpy(octopus, "./octopus");
  }

  if(myid == 0) {
    printf("\n********** octopus_init\n");
    printf("  octopus command: %s.\n", octopus);
    printf("**********\n"); 
    fflush(stdout);
  }

  sprintf(filename, "octopus_record_file-%d", myid);
  if((octopus_record_file=fopen(filename,"wt"))==NULL){
    printf("Error al abrir un fichero %s\n", filename);
    parallel_end(EXIT_FAILURE);
  }

  octopus_runs_counter = 0;

  return 0;
}

void octopus_end(){
  fclose(octopus_record_file);
}

void run_octopus(char *octopus_command, char* output, int scf_check_convergence, int sternheimer_check_convergence){
  char * system_command;
  int status;

  if (output != NULL){
    system_command = (char *) malloc(strlen(octopus_command)+strlen(output)+strlen(" > ")+strlen(" 2>&1 ")+1);
    strcpy(system_command, octopus_command);
    strcat(system_command, " > ");
    strcat(system_command, output);
    strcat(system_command, " 2>&1 ");
  }else{
    system_command = (char *) malloc(strlen(octopus_command)+1);
    strcpy(system_command, octopus_command);
  }
  fflush(stdout);

  status = system(system_command);
  if(status) {
    printf("octopus command:\n"); 
    printf("%s\n", system_command);
    printf("failed.\n");
    parallel_end(EXIT_FAILURE);
  }

  if(scf_check_convergence){
    if(output != NULL){
      if(!octopus_check_convergence(output)){
        printf("octopus run was not converged.\n");
        parallel_end(EXIT_FAILURE);
      }
    }
  }

  if(sternheimer_check_convergence){
    if(!octopus_check_sternheimer_convergence(output)){
      printf("octopus sternheimer computations were not converged.\n");
      parallel_end(EXIT_FAILURE);
    }
  }

  /* The following code allows the runs to be interrumpted with SIGINT or SIGQUIT (Control-C, typically) */
  if (WIFSIGNALED(status) &&
      (WTERMSIG(status) == SIGINT || WTERMSIG(status) == SIGQUIT))
    exit(0);

  ++octopus_runs_counter;
  fprintf(octopus_record_file, "OCTOPUS RUN #%d\n", octopus_runs_counter);

}

int octopus_check_convergence(char *output_file){
  return search_in_file(output_file, "SCF converged in");
}

int octopus_check_sternheimer_convergence(char *output_file){
  return search_in_file(output_file, "ALL STERNHEIMER LINEAR SYSTEMS CONVERGED");
}


int search_in_file(char *fname, char *str) {
  FILE *fp;
  char temp[512];

  if((fp = fopen(fname, "r")) == NULL) return 0;
  while(fgets(temp, 512, fp) != NULL) {
    if((strstr(temp, str)) != NULL) {
      fclose(fp);
      return 1;
    }
  }

  fclose(fp);
  return 0;
}
