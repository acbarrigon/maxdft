#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright 2015, Adrián Gómez Pueyo and Alberto Castro

# This file is part of maxdft.

# maxdft is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# maxdft is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.

# You should have received a copy of the GNU General Public License
# along with maxdft.  If not, see <http://www.gnu.org/licenses/>.


from pyevolve import G1DList
from pyevolve import GSimpleGA
from pyevolve import Initializators
from pyevolve import Selectors
from pyevolve import Statistics
from pyevolve import DBAdapters
import pyevolve
import inp
import compute_density
import function
import math
import os
import params
import read
import sys


# This function is the evaluation function, we want
# to give high score to more zero'ed chromosomes
def eval_func(chromosome):
    """Evaluation function of the chromosome fitness

    In this function we make the octopus input file inp,
    where the arguments for the IPF are the genes of our
    chromosome, then we call octopus and calculate the
    fitness function F(G[gamma]).
    """

    score = 0.0

    for i in range(npotex):
        # Reading the DFT density
        dirname = 'DFT.' + str(i)
        os.chdir(dirname)
        ndft = read.DFT()
        os.chdir('..')

        # Computation of the SCH density
        compute_density.nsch(l, step, alpha, lmin, lmax, delta, i,
                             npar, poten_selector, chromosome)
        nsch = read.SCH(None)
        os.chdir('..')

        score += function.obj_fun(nsch, ndft, step)

    if score >= 1.0:
        score = math.log(score)
    else:
        score = -math.log(score)

    return score


# Start of the GAs minimization
print '\n\n'
print '****' + ' Minimization algorithm: Genetic Algorithm '+ '****'

# Initialization of the parameters
print '\n\nReading the parameters for the evaluation function:\n'
(l, step, alpha, lmin, lmax, delta, npotex, npar,
poten_selector) = params.param_init()

# Enable the pyevolve logging system
pyevolve.logEnable()

# Genome instance, 1D List of npar elements
genome = G1DList.G1DList(npar)

# Sets the range max and min of the 1D List
genome.setParams(rangemin=-2, rangemax=2)

# Change the genome to real values
genome.initializator.set(Initializators.G1DListInitializatorReal)

# The evaluator function (evaluation function)
genome.evaluator.set(eval_func)

# Genetic Algorithm Instance
ga = GSimpleGA.GSimpleGA(genome)

# Set the population size
ga.setPopulationSize(10)

# Set the Roulette Wheel selector method, the number of generations and
# the termination criteria
ga.selector.set(Selectors.GRouletteWheel)
ga.setGenerations(100)
ga.terminationCriteria.set(GSimpleGA.ConvergenceCriteria)

# Sets the DB Adapter, the resetDB flag will make the Adapter recreate
# the database and erase all data every run, you should use this flag
# just in the first time, after the pyevolve.db was created, you can
# omit it.
sqlite_adapter = DBAdapters.DBSQLite(identify="ex1", resetDB=True)
ga.setDBAdapter(sqlite_adapter)

# Do the evolution, with stats dump
# frequency of freq_stats generations
ga.evolve(freq_stats=1)

# Best individual
best = ga.bestIndividual()
gfinal = math.exp(-best.score)
print '\nG[gamma] final value: ' + str(gfinal) + '\n'
print best

