/*

Copyright 2015, Adrián Gómez Pueyo and Alberto Castro

This file is part of maxdft.

maxdft is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

maxdft is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
maxdft.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdio.h>
#include <stdlib.h>

#include "grid.h"
#include "input.h"

int grid_init(grid_t * gr){

  gr->l = 15.0;   parse_double ("boxlength", &(gr->l));
  gr->step = 0.2; parse_double ("step", &(gr->step));

  gr->N = ((int)(gr->l/gr->step)) * 2 + 1;

  return 0;
};

double grid_function_diff(double *f1, double *f2)
{

  int i;
  double a = 0;

  a += 0.5*grid.step*(f1[0] - f2[0])*(f1[0] - f2[0]);
  for(i = 1; i < grid.N - 1; i++)  a += grid.step*(f1[i] - f2[i])*(f1[i] - f2[i]);
  a += 0.5*grid.step*(f1[grid.N - 1] - f2[grid.N - 1])*(f1[grid.N - 1] - f2[grid.N - 1]);

  return(a);
}

double grid_function_dotp(double *f1, double *f2)
{

  int i;
  double a = 0;

  a += 0.5 * grid.step * f1[0] * f2[0];
  for(i = 1; i < grid.N - 1; i++)  a += grid.step * f1[i] * f2[i];
  a += 0.5 * grid.step * f1[grid.N - 1] *f2[grid.N - 1];

  return(a);
}
