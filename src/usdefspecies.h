/*

Copyright 2015, Adrián Gómez Pueyo and Alberto Castro

This file is part of maxdft.

maxdft is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

maxdft is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
maxdft.  If not, see <http://www.gnu.org/licenses/>.

*/


#ifndef USDEFSPECIES_H
#define USDEFSPECIES_H



typedef struct{
  int kind;
  int nparameters;
  int nfixedparameters;
  double *parameters;
} usdefspecies_t;

/* The list of species should all be defined with constants here. 
   The precise definition of what each of them means should be in
   the documentation.
*/

/* The constants with values equal or lower to one do not refer to an IPF, but to
   external potentials (either for KS or for Schrödinger equation calculations.
 */
#define USDEFSPECIES_HYDROGEN_1E 0
#define USDEFSPECIES_HYDROGEN_2E -1


/* From here on, the IPFs. */

#define USDEFSPECIES_TAYLOR_SOFT_COULOMB 1
/* This is the Soft Coulomb potential, multiplied by a Taylor series, and by an exponential
   tail in that makes the Taylor series go to one at infinity (so that the behaviour is
   of Coulomb type at large r.

   For n parameters (n >=2):

   w(r) = sc(r) *
          gamma[0] + ( gamma[1]*r + gamma[2]*r^2 + ... + gamma[n-2]*r^(n-2) ) * exp ( -gamma[n-1]*r*r )

   sc(r) = (alpha + r*r)^(-1/2)

 */

#define USDEFSPECIES_TAYLOR_SOFT_COULOMB_2 8
/* This is the Soft Coulomb potential, multiplied by a Taylor series, and by an exponential
   tail in that makes the Taylor series go to one at infinity (so that the behaviour is
   of Coulomb type at large r. The parameter that multiplies the exponential is a fixed
   parameter.

   For n parameters

   w(r) = sc(r) * p(r)

   p(r) = gamma[0] + ( gamma[1]*r + gamma[2]*r^2 + ... + gamma[n-1]*r^(n-1) ) * exp ( -lambda*r^2 )

   sc(r) = (alpha + r*r)^(-1/2)

   The derivative is:

   w'(r) = sc'(r) * p(r) + sc(r) * p'(r)

   p'(r) = ( gamma[1] + 2*r*gamma[2] + ... + (n-1)*gamma[n-1]*r^(n-2) ) * exp( - lambda*r^2 ) + 
           ( gamma[1]*r + gamma[2]*r^2 + ... + gamma[n-1]*r^(n-1) ) * ( - 2*r*lambda ) * exp ( -lambda*r^2 )

   And the derivatives wrt the coefficients are:

   i = 0: w_i(r) = sc(r)
   i > 0: w_i(r) = sc(r) * r^i * exp(-lambda*r^2)

 */

#define USDEFSPECIES_TAYLOR 2
/* This is a pure Taylor series:
   w(r) = gamma[0] + gamma[1]*r + ... + gamma[n-1]*r^(n-1)
 */

#define USDEFSPECIES_SOFT_COULOMB 3
/* This is the soft-Coulomb parametrization, multiplied by a constant, which is the only
   parameter:

   w(r) = gamma[0] * sc(r)

   sc(r) = (alpha+r^2)^(-1/2)
*/

#define USDEFSPECIES_PADE 4
/* This is Pade approximant of order [n/2,n/2+1]. It has n parameters (n must be an even number).
 w(r) = ( gamma[0] + gamma[1]*r + gamma[2]*r^2 + ... + gamma[n/2-1]*r^(n/2-1) ) /
        ( 1 + gamma[n/2]*r + gamma[n/2+1]*r^2 + ... + gamma[n-1]*r^(n/2) )
 */

#define USDEFSPECIES_PADE_ONE 5
/* For n parameters, where n is odd, this is a Pade approximant of order [(n-1)/2,(n-1)/2+1], 
   but with the first coefficient of the numerator fixed to one. This can be done because two IPFs are equivalent
   if they differ by a constant, which would be the zero-th order term of a Taylor expansion, that coincides with
   the first coefficient of the numerator ina Pade approximant.

   If we set m = (n-1)/2, then it is a Pade approximant
   of order [m, m+1], defined as:

   w(r) = p(r)/q(r)

   p(r) = ( 1 + gamma[0]*r + gamma[1]*r^2 + ... + gamma[m-1]*r^m )

   q(r) = ( 1 + gamma[m]*r + gamma[m+1]*r^2 + ... + gamma[2m]*r^(m+1) )

   The derivatives are:

   p'(r) = ( gamma[0] + 2*gamma[1]*r + ... + m*gamma[m-1]*r^(m-1) )
   q'(r) = ( gamma[m] + 2*gamma[m+1]*r + ... + (m+1)*gamma[2m]*r^m )

   The derivatives wrt the coefficients are:
   If i < m:
     w_i(r) = r^(i+1) / q(r)
   If i >= m
     w_i(r) = - ( p(r) / q(r)^2 ) * r^(i-p+1)
 */

#define USDEFSPECIES_PADE_FIXED 6
/* For n parameters, where n is even, this is a Pade approximant of order [n/2-1,n/2], plus a 
   correction term that fixes the value at zero at a given user-defined value. Setting m = n/2,
   we have a Pade approximant of [m-1,m] = [n/2-1,n]

   w(r) = p(r)/q(r) + w0 - gamma[0]

   p(r) = ( gamma[0] + gamma[1]*r + gamma[2]*r^2 + ... + gamma[m-1]*r^(m-1) )

   q(r) = ( 1 + gamma[m]*r + gamma[m+1]*r^2 + ... + gamma[m+m-1 = n]*r^m) )

   The derivatives are:

   p'(r) = (gamma[1] + 2*gamma[2]*r + ... + (m-1) * gamma[m-1] * r^(m-2) )

   q'(r) = (gamma[m] + 2*gamma[m+1]*r + ... + m * gamma[n] * r^(m-1) )

   The derivatives wrt the coefficients are:

   if i = 0:
     w_i(r) = 1 / q(r) - 1
   if 0 < i < m:
     w_i(r) = r^i / q(r)
   if i >= m:
     w_i(r) = - ( p(r) / q(r)^2 ) * r^(i-m+1) 

 */

#define USDEFSPECIES_SC_PADE 7
/* For n parameters, where n is odd, this is the function that results of multiplying the 
   "reference" IPF (soft-Coulomb), by a Padé approximant of order [(n-1)/2,(n-1)/2]. Setting m = (n-1)/2,

   w(r) = sc(r) * ( p(r) / q(r) )

   sc(r) = lambda * (alpha+r^2)^(-1/2)

   p(r) = gamma[0] + gamma[1]*r + ... + gamma[(n-1)/2]*r^((n-1)/2)
   p(r) = gamma[0] + gamma[1]*r + ... + gamma[m]*r^m

   q(r) = 1 + gamma[(n-1)/2+1]*r + gamma[(n-1)/2+2]*r^2 + ... + gamma[(n-1)/2+(n-1)/2 = n-1]*r^((n-1)/2)
   q(r) = 1 + gamma[m+1]*r + gamma[m+2]*r^2 + ... + gamma[m+m = n-1]*r^((n-1)/2)

   The derivatives are:

   sc'(r) = - lambda * (alpha + r^2)^(-3/2) * r

   p'(r) = gamma[1] + 2*gamma[2]*r + ... + ((n-1)/2)*gamma[(n-1)/2]*r^((n-1)/2-1)
   p'(r) = gamma[1] + 2*gamma[2]*r + ... + m * gamma[m]*r^(m-1)

   q'(r) = gamma[(n-1)/2+1] + 2*gamma[(n-1)/2+2]*r + ... + ((n-1)/2)*gamma[n-1]*r^((n-1)/2-1)
   q'(r) = gamma[m+1] + 2*gamma[m+2]*r + ... + m*gamma[n-1]*r^(m-1)

   w'(r) = sc'(r) * ( p(r) / q(r) ) + sc(r) * (p'(r)*q(r) - q'(r)*p(r)) / (q(r)^2)

   The derivatives wrt to the coefficients are:

   if 0 < i <= (n-1)/2
     w_i(r) = sc(r) * r^i / q(r)
   if (n-1)/2 < i < n
     w_i(r) = - sc(r) * (p(r) / (q(r)^2) ) * r^(i-(n-1)/2)

 */


int usdefspecies_init(usdefspecies_t **s, int kind, int nparameters, double *parameters);
void usdefspecies_free(usdefspecies_t **s);
int usdefspecies_nparameters(usdefspecies_t **s);
int usdefspecies_nfixedparameters(usdefspecies_t **s);
void usdefspecies_parameters(usdefspecies_t **s, double *parameters);
int usdefspecies_string(usdefspecies_t s, char * string);
double usdefspecies_value_r(usdefspecies_t **s, double r);
double usdefspecies_dvalue_dr(usdefspecies_t **s, double r);
double usdefspecies_value(usdefspecies_t **s, double x, double y, double z, double r);
double usdefspecies_dvalue(usdefspecies_t **s, int i, double x, double y, double z, double r);


#endif
