#!/usr/bin/env python
# -*- coding: utf-8 -*- 
# Copyright 2015, Adrián Gómez Pueyo and Alberto Castro

# This file is part of maxdft.

# maxdft is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# maxdft is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.

# You should have received a copy of the GNU General Public License
# along with maxdft.  If not, see <http://www.gnu.org/licenses/>.

import inp
import os
import octopus
import read

def nsch(l, step, alpha, lmin, lmax, delta,
    potex, npar, poten_selector, gamma):
    """Compute the SCH density

    In this function we create a new directory where
    we run octopus with the appropriate parameters
    for the IPF and compute the electronic density
    of our system using the Schrodinger equation.
    """

    #print '  Potential ' + str(potex) + '.'

    octopus_command = os.getenv('OCTOPUS')
    output_string = 'out.exact.' + str(potex)

    dirname = 'exact.' + str(potex)
    if not os.path.exists(dirname):
        os.mkdir(dirname, 0775)
    os.chdir(dirname)

    inp.sch(l, step, alpha, lmin, lmax, delta,
    potex, npar, poten_selector, gamma)
    octopus.run_octopus(octopus_command, output_string)
        
