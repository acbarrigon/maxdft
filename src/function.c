/*

Copyright 2015, Adrián Gómez Pueyo and Alberto Castro

This file is part of maxdft.

maxdft is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

maxdft is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
maxdft.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_deriv.h>

#include "parallel.h"
#include "input.h"
#include "grid.h"
#include "octopus.h"
#include "ipf.h"
#include "inp.h"
#include "extpot.h"
#include "density.h"
#include "minim.h"
#include "read.h"
#include "gfunction.h"


double gfunction(const double *x, double *ndft)
{
  int i, j;
  double *nSCH, *n_i;
  char * output_string;
  double total;

  nSCH = (double *)malloc(grid.N*sizeof(double));
  n_i  = (double *)malloc(grid.N*sizeof(double));

  output_string = (char *)malloc(25*sizeof(char));

  total = 0.0;
  for(i = extpot.initpot; i <= extpot.finalpot; i++)
    {
      sprintf(output_string, "%s%d", "tmp.", i);
      mkdir(output_string, 0775);
      chdir(output_string);
      inpSCH(x, i, 0);  /* Creating the input file for Octopus */
      run_octopus(octopus, "out", 1, 0);  /* Computing the SCH density for an external potential v_k */
      leeSCH(nSCH, NULL);  /* Reading and storing the SCH density */
      chdir("..");

      for(j = 0; j < grid.N; j++) n_i[j] = ndft[(grid.N)*i + j];

      total += grid_function_diff(nSCH, n_i);  /* Computing the target function G[gamma, lambda; v_k] */
    }

#if defined(HAVE_MPI)
  MPI_Allreduce(&total, &total, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
#endif


  free(output_string);
  free(nSCH);
  free(n_i);
  return total;
}

double function( double x, void *params)
{
  int i;
  param_function_t * p;
  double result;
  double *xx;

  p = (param_function_t *) params;
  xx = (double *)malloc(ipf.npar*sizeof(double));
  for(i = 0; i < ipf.npar; i++) xx[i] = gsl_vector_get(p->x, i);
  xx[p->i] = x;

  result = gfunction(xx, p->nDFT);
  free(xx);
  return result;
}


void grad_gfunction(const double *x, double *ndft, double *grad)
{
  int i, j, k;
  double *var, *nSCH, *n_i, *nSCH_der, *deltan, *gradient, *gradient_all;
  gsl_vector *xx;
  char * output_string;

  gsl_function F;
  param_function_t p;
  double result, abserr;


  var  = (double *)malloc(ipf.npar*sizeof(double));
  nSCH = (double *)malloc(grid.N * sizeof(double));
  n_i  = (double *)malloc(grid.N * sizeof(double));


  for(i = 0; i < ipf.npar; i++) var[i] = x[i];
  xx = gsl_vector_alloc(ipf.npar);

  /* Computation of G(gamma)'s gradient */

  switch(gradient_mode){

  case GRADIENT_PRECISE :

    for(k = 0; k < ipf.npar ; k++)
      {
        for(i = 0; i < ipf.npar; i++) gsl_vector_set(xx, i, x[i]);
        F.function = &function;
        p.i = k;
        p.nDFT = ndft;
        p.x = xx;
        F.params = (void *)(&p);

        gsl_deriv_central (&F, x[k], delta_numder, &result, &abserr);
        grad[k] = result;

      }
    break;

  case GRADIENT_STERNHEIMER :

    output_string = (char *)malloc(25*sizeof(char));
    nSCH_der  = (double *)malloc(grid.N * sizeof(double));
    deltan  = (double *)malloc(grid.N * sizeof(double));
    gradient = (double *)calloc(ipf.npar, ipf.npar*sizeof(double));
    gradient_all = (double *)malloc(ipf.npar*sizeof(double));

    for(i = extpot.initpot; i<= extpot.finalpot; i++)
      {
        sprintf(output_string, "%s%d", "tmp.", i);
        mkdir(output_string, 0775);
        chdir(output_string);
        inpSCH(var, i, 1);
        run_octopus(octopus, "out", 1, 1);
        /* First, we read the "normal" density */
        leeSCH(nSCH, NULL);
        /* Now, we read the derivatives of the density: it should be leeSCH(nSCH, &k) */
        for( k = 0; k < ipf.npar ; k++) {
          leeSCH(nSCH_der, &k);
	  for( j = 0; j < grid.N ; j++ ) deltan[j] = nSCH[j] - ndft[i*grid.N+j] ;
	  gradient[k] += 2.0 * grid_function_dotp(deltan, nSCH_der);
        }
        chdir("..");
      }

#if defined(HAVE_MPI)
    if(numprocs > 1){
      MPI_Allreduce(gradient, gradient_all, ipf.npar, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
      for (k = 0; k < ipf.npar; k++ ) grad[k] = gradient_all[k];
    }else{
      for (k = 0; k < ipf.npar; k++ ) grad[k] = gradient[k];
    }
#else
    for (k = 0; k < ipf.npar; k++ ) grad[k] = gradient[k];
#endif

    free(output_string);
    free(nSCH_der); free(deltan); free(gradient); free(gradient_all);
    break;

  }

  gsl_vector_free(xx);
  free(var); free(nSCH); free(n_i);
}


/*
 For the NLOPT calls:
 */

double nlopt_gfunction(unsigned n, const double *x, double *grad, void *data)
{
  nlopt_function_data *function_data = (nlopt_function_data *) data;
  double total;

  ++function_data-> counter;

  total = gfunction(x, function_data->ndft);

  if (grad) grad_gfunction(x, function_data->ndft, grad);

  if(myid == 0){
    printf("Called nlopt_gfunction, value = %.12lg\n", total);
  }

  return total;
}

/* double nlopt_constraint(unsigned n, const double *x, double *grad, void *data)
{

  my_constraint_data *d = (my_constraint_data *) data;
  double a = d->a, b = d->b;
  if (grad) {
    grad[0] = 3 * a * (a*x[0] + b) * (a*x[0] + b);
    grad[1] = -1.0;
  }
  return ((a*x[0] + b) * (a*x[0] + b) * (a*x[0] + b) - x[1]);

} */

void gfunction_init(){
  gradient_mode = 2;   parse_int ("gradient_mode", &gradient_mode);
  delta_numder = 0.01; parse_double ("delta_numder", &delta_numder);
}


/*
  For the GSL calls:
*/

double my_f (const gsl_vector *x, void *params)
{
  int i;
  double *var;
  double total;
  params_gsl_multimin_function_t *p;

  p = (params_gsl_multimin_function_t *) params;
  var  = malloc(ipf.npar*sizeof(double));
  for(i = 0; i < ipf.npar; i++) var[i] = gsl_vector_get(x, i);
  total = gfunction(var, p->nDFT);

  free(var);
  return total; 
}


void my_df (const gsl_vector *v, void *params, gsl_vector *df)
{
  int i;
  double *x, *grad;
  params_gsl_multimin_function_t *par;

  par = (params_gsl_multimin_function_t *) params;

  x = (double *)malloc(ipf.npar*sizeof(double));
  grad = (double *)malloc(ipf.npar*sizeof(double));

  for(i = 0; i < ipf.npar; i++) x[i] = gsl_vector_get(v, i);

  grad_gfunction(x, par->nDFT, grad);

  for(i = 0; i < ipf.npar; i++) gsl_vector_set(df, i, grad[i]);

  free(x);
  free(grad);
}

void my_fdf (const gsl_vector *x, void *params, double *f, gsl_vector *df) 
{
  *f = my_f(x, params); 
  my_df(x, params, df);
}

