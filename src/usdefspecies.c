/*

Copyright 2015, Adrián Gómez Pueyo and Alberto Castro

This file is part of maxdft.

maxdft is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

maxdft is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
maxdft.  If not, see <http://www.gnu.org/licenses/>.

*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "usdefspecies.h"


int usdefspecies_init(usdefspecies_t **s, int kind, int nparameters, double *parameters){
  int i;

  *s = malloc(sizeof(usdefspecies_t));
  (*s)->kind = kind;
  (*s)->nparameters = nparameters;
  (*s)->parameters = (double *) malloc(nparameters*sizeof(double));
  for(i=0; i<nparameters; i++){(*s)->parameters[i] = parameters[i];}

  switch (kind){
  case USDEFSPECIES_HYDROGEN_1E :
    (*s)->nfixedparameters = 0; break;
  case USDEFSPECIES_HYDROGEN_2E :
    (*s)->nfixedparameters = 0; break;
  case USDEFSPECIES_TAYLOR_SOFT_COULOMB :
    (*s)->nfixedparameters = 1;
  case USDEFSPECIES_TAYLOR : break;
    (*s)->nfixedparameters = 1;
  case USDEFSPECIES_SOFT_COULOMB :
    (*s)->nfixedparameters = 1;
    break;
  case USDEFSPECIES_PADE :
    (*s)->nfixedparameters = 0;
    break;
  case USDEFSPECIES_PADE_ONE :
    (*s)->nfixedparameters = 0;
    break;
  case USDEFSPECIES_PADE_FIXED :
    (*s)->nfixedparameters = 1;
    break;
  case USDEFSPECIES_SC_PADE :
    (*s)->nfixedparameters = 2;
    break;
  case USDEFSPECIES_TAYLOR_SOFT_COULOMB_2 :
    (*s)->nfixedparameters = 2;
    break;
  }

  return 0;
}

void usdefspecies_free(usdefspecies_t **s){free((*s)->parameters); free(*s);}

int usdefspecies_nparameters(usdefspecies_t **s){return (*s)->nparameters;}

int usdefspecies_nfixedparameters(usdefspecies_t **s){return (*s)->nfixedparameters;}

void usdefspecies_parameters(usdefspecies_t **s, double *parameters){
  int i;
  for( i = 0; i < (*s)->nparameters; i++){ parameters[i] = (*s)->parameters[i]; }
}


double usdefspecies_dvalue_dr(usdefspecies_t **s, double r){
  double p, q, pprime, qprime, gamma0, alpha, soft_coulomb, sc, scprime, lambda;
  int j, npars, nfixedpars, n, m;

  npars = (**s).nparameters;
  nfixedpars = (**s).nfixedparameters;

  switch((**s).kind){
  case USDEFSPECIES_TAYLOR_SOFT_COULOMB :
    /* WARNING: Missing derivative */
  case USDEFSPECIES_TAYLOR :
    /* WARNING: Missing derivative */
    return 0.0;
  case USDEFSPECIES_SOFT_COULOMB :
    alpha = (**s).parameters[npars-nfixedpars];
    gamma0 = (**s).parameters[0];
    soft_coulomb = 1.0/sqrt(alpha + r*r);
    return - r * pow(soft_coulomb, 3) * gamma0;
  case USDEFSPECIES_PADE :
    p = (**s).parameters[0];
    for(j = 1; j < (npars - nfixedpars)/2; j++)  p += (**s).parameters[j] * pow(r, j);
    q = 1.0;
    for(j = 0; j < (npars - nfixedpars)/2; j++)  q += (**s).parameters[j + (npars - nfixedpars)/2] * pow(r, j+1);
    pprime = 0.0;
    for(j = 1; j < (npars - nfixedpars)/2; j++)  pprime += j * (**s).parameters[j] * pow(r, j-1);
    qprime = 0.0;
    for(j = 0; j < (npars - nfixedpars)/2; j++)  qprime += (j+1) * (**s).parameters[j + (npars - nfixedpars)/2] * pow(r, j);
    return (pprime*q - qprime*p)/(q*q);
  case USDEFSPECIES_PADE_ONE :
    n = (npars - nfixedpars - 1)/2;
    p = 1.0;
    for(j = 0; j < n ; j++) p += (**s).parameters[j] * pow(r, j+1);
    q = 1.0;
    for(j = n; j < 2*n+1; j++) q += (**s).parameters[j] * pow(r, j-n+1);
    pprime = (**s).parameters[0];
    for(j = 1; j < n ; j++) pprime += (j+1) * (**s).parameters[j] * pow(r, j);
    qprime = (**s).parameters[n];
    for(j = n+1; j < 2*n+1; j++) qprime += (j-n+1)*(**s).parameters[j] * pow(r, j-n);
    return (pprime*q - qprime*p)/(q*q);
  case USDEFSPECIES_PADE_FIXED :
    n = (npars - nfixedpars);
    m = n/2;
    p = (**s).parameters[0]; for (j = 1; j < m ; j++) p += (**s).parameters[j] * pow(r, j);
    q = 1.0; for (j = 1; j < m+1 ; j++) q += (**s).parameters[m+j-1] * pow(r, j);
    pprime = 0.0; for (j = 1; j < m ; j++) pprime += j * (**s).parameters[j] * pow(r, j-1);
    qprime = 0.0; for (j = 1; j < m+1 ; j++) qprime += j * (**s).parameters[m+j-1] * pow(r, j-1);
    return (pprime*q-qprime*p)/(q*q);
  case USDEFSPECIES_SC_PADE:
    alpha = (**s).parameters[npars-2];
    lambda = (**s).parameters[npars-1];
    n = (npars - nfixedpars);
    m = (n-1)/2;
    p = (**s).parameters[0]; for (j = 1; j < m+1; j++) p += (**s).parameters[j] * pow(r, j);
    /*q = 1.0; for (j = 1; j < m+1; j++) q += (**s).parameters[m+1+j] * pow(r, j);*/
    q = 1.0; for (j = 1; j < m+1; j++) q += (**s).parameters[m+j] * pow(r, j);
    sc = lambda * pow(alpha+r*r, -0.5);
    scprime = - lambda * pow(alpha+r*r, -1.5) * r;
    pprime = 0.0; for(j = 1; j < m+1; j ++) pprime += j * (**s).parameters[j] * pow(r, j-1);
    qprime = 0.0; for (j = 1; j < m+1; j++) qprime += j * (**s).parameters[m+1+j] * pow(r, j-1);
    return scprime * (p / q) + sc * ((pprime*q-qprime*p)/(q*q));
  case USDEFSPECIES_TAYLOR_SOFT_COULOMB_2 :
    alpha = (**s).parameters[npars-2];
    lambda =(**s).parameters[npars-1];
    n = (npars - nfixedpars);
    p = 0.0;
    if(n > 1){
      p = (**s).parameters[1] * r;
      for(j = 2; j < n; j++)  p += (**s).parameters[j] * pow(r, j);
      p *= exp( -lambda * r *r);
    }
    p += (**s).parameters[0];
    pprime = 0.0;
    if(n > 1){
      pprime = (**s).parameters[1];
      for(j = 2; j < n; j++) pprime += j * (**s).parameters[j] * pow(r, j-1);
      pprime *= exp(-lambda*r*r );
    }
    pprime += p * (-2*r*lambda) * exp(-lambda*r*r );
    soft_coulomb = pow(alpha+r*r, -0.5);
    scprime = -r * pow(alpha+r*r, -1.5);
    return soft_coulomb * pprime + scprime*p;

  default :
    return 0.0;
  }

}

double usdefspecies_value_r(usdefspecies_t **s, double r){
  double alpha, poten, soft_coulomb, pade, p, q, lambda;
  int j, npars, nfixedpars, n, m;

  npars = (**s).nparameters;
  nfixedpars = (**s).nfixedparameters;

  switch((**s).kind){
  case USDEFSPECIES_TAYLOR_SOFT_COULOMB :
    alpha = (**s).parameters[npars-nfixedpars];
    poten = (**s).parameters[1] * r;
    for(j = 2; j < npars - nfixedpars -1; j++)  poten += (**s).parameters[j] * pow(r, j);
    poten *= exp(-(**s).parameters[npars-nfixedpars-1] * r*r);
    poten += (**s).parameters[0];
    soft_coulomb = sqrt(alpha + r*r);
    poten /= soft_coulomb;
    return poten;
  case USDEFSPECIES_TAYLOR_SOFT_COULOMB_2 :
    alpha = (**s).parameters[npars-2];
    lambda =(**s).parameters[npars-1];
    n = npars - nfixedpars;
    p = 0.0;
    if(n > 1){
      p = (**s).parameters[1] * r;
      for(j = 2; j < n; j++)  p += (**s).parameters[j] * pow(r, j);
      p *= exp( -lambda * r *r);
    }
    p += (**s).parameters[0];
    soft_coulomb = pow(alpha+r*r,-0.5);
    return soft_coulomb * p;
  case USDEFSPECIES_TAYLOR :
    poten = (**s).parameters[0];
    for(j = 1; j < npars - nfixedpars; j++)  poten += (**s).parameters[j] * pow(r, j);
    return poten;
  case USDEFSPECIES_SOFT_COULOMB :
    alpha = (**s).parameters[npars-nfixedpars];
    poten = (**s).parameters[0];
    soft_coulomb = sqrt(alpha + r*r);
    poten /= soft_coulomb;
    return poten;
  case USDEFSPECIES_PADE :
    poten = (**s).parameters[0];
    for(j = 1; j < (npars - nfixedpars)/2; j++)  poten += (**s).parameters[j] * pow(r, j);
    pade = 1.0;
    for(j = 0; j < (npars - nfixedpars)/2; j++)  pade += (**s).parameters[j + (npars - nfixedpars)/2] * pow(r, j+1);
    poten /= pade;
    return poten;
  case USDEFSPECIES_PADE_ONE :
    n = (npars - nfixedpars - 1)/2;
    poten = 1.0;
    for(j = 0; j < n ; j++) poten += (**s).parameters[j] * pow(r, j+1);
    pade = 1.0;
    for(j = n; j < 2*n+1; j++) pade += (**s).parameters[j] * pow(r, j-n+1);
    return poten/pade;
  case USDEFSPECIES_PADE_FIXED :
    n = (npars - nfixedpars);
    m = n/2;
    p = (**s).parameters[0]; for (j = 1; j < m ; j++) p += (**s).parameters[j] * pow(r, j);
    q = 1.0; for (j = 1; j < m+1 ; j++) q += (**s).parameters[m+j-1] * pow(r, j);
    return p/q - (**s).parameters[0] + (**s).parameters[n];
  case USDEFSPECIES_SC_PADE :
    alpha = (**s).parameters[npars-2];
    lambda = (**s).parameters[npars-1];
    n = (npars - nfixedpars);
    m = (n-1)/2;
    p = (**s).parameters[0]; for (j = 1; j < m+1; j++) p += (**s).parameters[j] * pow(r, j);
    /*q = 1.0; for (j = 1; j < m+1; j++) q += (**s).parameters[m+1+j] * pow(r, j);*/
    q = 1.0; for (j = 1; j < m+1; j++) q += (**s).parameters[m+j] * pow(r, j);
    return (lambda / sqrt(alpha + r*r) ) * (p/q);
  default :
    return 0.0;
  }

}

double usdefspecies_value(usdefspecies_t **s, double x, double y, double z, double r){
  double val, alpha;

  switch((**s).kind){
  case USDEFSPECIES_HYDROGEN_1E :
    alpha = (**s).parameters[0];
    val = -1/sqrt(alpha+x*x);
    return val;
  case USDEFSPECIES_HYDROGEN_2E :
    alpha = (**s).parameters[0];
    val = -1/sqrt(alpha+x*x) - 1/sqrt(alpha+y*y);
    return val;
  default :
    return usdefspecies_value_r(s, fabs(x-y));
  }
}

double usdefspecies_dvalue(usdefspecies_t **s, int i, double x, double y, double z, double r){
  double alpha, poten, soft_coulomb, pade, p, q, rr, lambda, sc;
  int npars, nfixedpars, n, m, j;

  npars = (**s).nparameters;
  nfixedpars = (**s).nfixedparameters;

  switch((**s).kind){
  case USDEFSPECIES_TAYLOR_SOFT_COULOMB :
    if(i > npars - nfixedpars - 1 ){
      printf("Error in usdefspecies_dvalue\n");
      return 0.0;
    }
    alpha = (**s).parameters[npars-nfixedpars];
    if(i == 0){
      poten = 1.0/sqrt(alpha+(x-y)*(x-y));
    }else if(i == npars - nfixedpars - 1 ){
      poten = (**s).parameters[1] * fabs(x-y);
      for(j = 2; j < npars - nfixedpars -1; j++)  poten += (**s).parameters[j] * pow(fabs(x-y),j);
      poten *= exp(-(**s).parameters[npars - nfixedpars -1] * (x-y)*(x-y));
      soft_coulomb = sqrt(alpha+(x-y)*(x-y));
      poten /= soft_coulomb;
      poten *= - (x-y) * (x-y);
    }else{
      poten = pow(fabs(x-y),i) * exp(-(**s).parameters[npars-nfixedpars-1] * (x-y)*(x-y));
      soft_coulomb = sqrt(alpha+(x-y)*(x-y));
      poten /= soft_coulomb;
    }
    return poten;
  case USDEFSPECIES_TAYLOR :
    if(i > npars-3 ){
      printf("Error in usdefspecies_dvalue\n");
      return 0.0;
    }
    poten = pow(fabs(x-y),i);
    return poten;
  case USDEFSPECIES_SOFT_COULOMB :
    if(i > npars-nfixedpars-1 ){
      printf("Error in usdefspecies_dvalue\n");
      return 0.0;
    }
    /*IPF = lambda / Soft-Coulomb*/
    alpha = (**s).parameters[npars-nfixedpars];
    poten = 1.0/sqrt(alpha+(x-y)*(x-y));
    return poten;
  case USDEFSPECIES_PADE :

    if(i > npars-nfixedpars-1 ){
      printf("Error in usdefspecies_dvalue\n");
      return 0.0;
    }
    if(i < (npars - nfixedpars)/2){
      poten = pow(fabs(x - y), i);
      pade = 1.0;
      for(j = 0; j < (npars - nfixedpars)/2; j++) pade += (**s).parameters[j + (npars - nfixedpars)/2] * pow(fabs(x - y), j+1);
      poten /= pade;
    }
    else{
      poten = (**s).parameters[0];
      for(j = 1; j < (npars - nfixedpars)/2; j++)  poten += (**s).parameters[j] * pow(fabs(x - y), j);
      pade = 1.0;
      for(j = 0; j < (npars - nfixedpars)/2; j++)  pade += (**s).parameters[j + (npars - nfixedpars)/2] * pow(fabs(x - y), j+1);
      pade *= -pade;
      poten /= pade;
      poten *= pow(fabs(x - y),i+1-(npars-nfixedpars)/2);
    }
    return poten;

  case USDEFSPECIES_PADE_ONE :
    n = (npars - nfixedpars);
    m = n/2-1;
    rr = fabs(x-y);
    p = (**s).parameters[0]; for (j = 1; j < m ; j++) p += (**s).parameters[j] * pow(r, j);
    q = 1.0; for (j = 1; j < m+1 ; j++) q += (**s).parameters[m+j-1] * pow(r, j);
    if(i < m){
      poten = pow(rr, i+1)/q;
    }else{
      poten = - (p / (q*q)) * pow(rr, i-m+1);
    }
    return poten;

  case USDEFSPECIES_PADE_FIXED :
    n = (npars - nfixedpars);
    m = n/2;
    rr = fabs(x-y);
    p = (**s).parameters[0]; for (j = 1; j < m ; j++) p += (**s).parameters[j] * pow(rr, j);
    q = 1.0; for (j = 1; j < m+1 ; j++) q += (**s).parameters[m+j-1] * pow(rr, j);
    if( i == 0 ){
      poten = 1/q - 1.0;
    }else if( i < m ){
      poten = pow(rr, i)/q;
    }else{
      poten = - (p / (q*q)) * pow(rr, i-m+1);
    }
    return poten;

  case USDEFSPECIES_SC_PADE :
    rr = fabs(x-y);
    alpha = (**s).parameters[npars-2];
    lambda = (**s).parameters[npars-1];
    n = (npars - nfixedpars);
    m = (n-1)/2;
    p = (**s).parameters[0]; for (j = 1; j < m+1; j++) p += (**s).parameters[j] * pow(rr, j);
    /*q = 1.0; for (j = 1; j < m+1; j++) q += (**s).parameters[m+1+j] * pow(rr, j);*/
    q = 1.0; for (j = 1; j < m+1; j++) q += (**s).parameters[m+j] * pow(rr, j);
    sc = lambda * pow(alpha+rr*rr, -0.5);
    if( i <= m ){
      return sc * pow(rr, i) / q;
    }else{
      return -sc * (p / (q*q)) * pow(rr, i-m);
    }

  case USDEFSPECIES_TAYLOR_SOFT_COULOMB_2 :
    alpha = (**s).parameters[npars-2];
    lambda =(**s).parameters[npars-1];
    rr = fabs(x-y);
    soft_coulomb = pow(alpha+rr*rr,-0.5);
    if(i == 0){
      return soft_coulomb;
    }else{
      return soft_coulomb * pow(rr, i) * exp(-lambda*rr*rr);
    }

  default :
    printf("Error in usdefspecies_dvalue\n");
    return 0.0;
  }
}

