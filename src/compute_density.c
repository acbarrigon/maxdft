/*

Copyright 2015, Adrián Gómez Pueyo and Alberto Castro

This file is part of maxdft.

maxdft is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

maxdft is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
maxdft.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <gsl/gsl_vector.h>

#include "density.h"
#include "extpot.h"
#include "parallel.h"
#include "octopus.h"
#include "grid.h"
#include "inp.h"
#include "input.h"
#include "read.h"
#include "write.h"
#include "maxdft.h"


int compute_ndft(density_t * ndft, extpot_t extpot, double ldamix){
  char * output_string;
  char * dirname;
  int i;
  double * ndft_val;
#if defined(HAVE_MPI)
  int mpi_err;
#endif

  output_string = (char *) malloc(25*sizeof(char));
  dirname = (char *) malloc(25*sizeof(char));

  if (myid == 0) printf("\nStart running the DFT calculations.\n");
  fflush(stdout); 
#if defined(HAVE_MPI)
  MPI_Barrier(MPI_COMM_WORLD);
#endif

  ndft_val = density_get_val(ndft);

  for(i = extpot.initpot; i <= extpot.finalpot; i++)
    {
      sprintf(output_string, "%s%d", "out.DFT.",i);
      sprintf(dirname, "DFT.%d",i);
      mkdir(dirname, 0775);
      chdir(dirname);
      inpDFT(i, ldamix);
      run_octopus(octopus, output_string, 1, 0);
      /* Reading and storage for the reference calculation */
      leeDFT(&ndft_val[grid.N * i]);
      /* Writing of the DFT density so we can plot it later */
      writeDFT(&ndft_val[grid.N * i],i);
      chdir("..");
    }
  if(myid == 0) printf("Done.\n\n");

#if defined(HAVE_MPI)
  mpi_err = MPI_Allgather( &ndft_val[grid.N * extpot.npotex_proc*myid], grid.N * extpot.npotex_proc, 
              MPI_DOUBLE, &ndft_val[0], grid.N * extpot.npotex_proc, MPI_DOUBLE, MPI_COMM_WORLD);
  if( mpi_err != 0){
    if(myid == 0) printf("MPI_Allgather failed.");
    parallel_end(EXIT_FAILURE);
  }
#endif

  return 0;
}


int compute_nsch(density_t * n0, extpot_t extpot, gsl_vector *x){
  /* WARNING TODO Fill this function */
  int i;
  char * output_string;
  char * dirname;
  double *n_0;
#if defined(HAVE_MPI)
  int mpi_err;
#endif

  output_string = (char *) malloc(25*sizeof(char));
  dirname = (char *) malloc(25*sizeof(char));

  n_0 = density_get_val(n0);

  if (myid == 0)   printf("Running reference exact calculation\n");
  fflush(stdout); 
#if defined(HAVE_MPI)
  MPI_Barrier(MPI_COMM_WORLD);
#endif
  for(i = extpot.initpot; i <= extpot.finalpot; i++)
    {
      sprintf(output_string, "%s%d", "out.exact.",i);
      sprintf(dirname, "exact.%d",i);
      mkdir(dirname, 0775);
      chdir(dirname);
      inpSCH(gsl_vector_ptr(x, 0), i, 0);
      run_octopus(octopus, output_string, 1, 0);
      leeSCH(&n_0[grid.N * i], NULL);
      chdir("..");
    }
  if(myid == 0) printf("Done.\n\n");

#if defined(HAVE_MPI)
  mpi_err = MPI_Allgather( &n_0[grid.N * extpot.npotex_proc*myid], grid.N * extpot.npotex_proc, 
              MPI_DOUBLE, &n_0[0], grid.N * extpot.npotex_proc, MPI_DOUBLE, MPI_COMM_WORLD);
  if( mpi_err != 0){
    if(myid == 0) printf("MPI_Allgather failed.");
    parallel_end(EXIT_FAILURE);
  }
#endif 


  return 0;
}
