/*

Copyright 2015, Adrián Gómez Pueyo and Alberto Castro

This file is part of maxdft.

maxdft is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

maxdft is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
maxdft.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdio.h>
#include <stdlib.h>

#include "parallel.h"

void messages_welcome(){
  if (myid == 0){

    printf("================================================================================\n");
    printf("Running maxdft version %s\n", VERSION);
    printf("\n");
    printf("Copyright 2015, Adrián Gómez Pueyo and Alberto Castro\n");
    printf("\n");
    printf("maxdft is free software: you can redistribute it and/or modify it under the\n");
    printf("terms of the GNU General Public License as published by the Free Software\n");
    printf("Foundation, either version 3 of the License, or (at your option) any later\n");
    printf("version.\n");
    printf("\n");
    printf("maxdft is distributed in the hope that it will be useful, but WITHOUT ANY\n");
    printf("WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\n");
    printf("A PARTICULAR PURPOSE.  See the GNU General Public License for more details.\n");
    printf("\n");
    printf("You should have received a copy of the GNU General Public License along with\n");
    printf("maxdft.  If not, see <http://www.gnu.org/licenses/>.\n");
    printf("================================================================================\n");
    printf("\n");
  }
  fflush(stdout);
}


void messages_basic(char *string){
  if (myid == 0) printf("%s", string);
  fflush(stdout); 
#if defined(HAVE_MPI)
  MPI_Barrier(MPI_COMM_WORLD);
#endif
}
