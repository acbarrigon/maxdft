/*

Copyright 2015, Adrián Gómez Pueyo and Alberto Castro

This file is part of maxdft.

maxdft is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

maxdft is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
maxdft.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>

#include "input.h"
#include "parallel.h"

xmlDocPtr doc;
xmlNodePtr cur;


void parse_double (char * keyword, double * value) {
  xmlChar *key;
  xmlNodePtr current;

  if(myid == 0){
    current = cur->xmlChildrenNode;
    while (current != NULL) {
      if ((!xmlStrcmp(current->name, (const xmlChar *)keyword))) {
        key = xmlNodeListGetString(doc, current->xmlChildrenNode, 1);
        *value = atof((char *)key);
        xmlFree(key);
      }
      current = current->next;
    }
  }

#if defined(HAVE_MPI)
  MPI_Bcast(value, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
#endif
  return;
}


void parse_double_array (char * keyword, int size, double * value) {
  xmlChar *key;
  xmlNodePtr current;
  int i;
  char * word;
  char * line;

  if(myid == 0){
    current = cur->xmlChildrenNode;
    while (current != NULL) {
      if ((!xmlStrcmp(current->name, (const xmlChar *)keyword))) {
        key = xmlNodeListGetString(doc, current->xmlChildrenNode, 1);
        line = (char *)key;
	word = strtok(line, " ");
        value[0] = atof(word);
        for(i = 1; i < size; i++){
          word = strtok(NULL, " ");
          value[i] = atof(word);
        }

        xmlFree(key);
      }
      current = current->next;
    }
  }

#if defined(HAVE_MPI)
  MPI_Bcast(value, size, MPI_DOUBLE, 0, MPI_COMM_WORLD);
#endif
  return;
}


void parse_int (char * keyword, int * value) {
  xmlChar *key;
  xmlNodePtr current;

  if(myid == 0){
    current = cur->xmlChildrenNode;
    while (current != NULL) {
      if ((!xmlStrcmp(current->name, (const xmlChar *)keyword))) {
        key = xmlNodeListGetString(doc, current->xmlChildrenNode, 1);
        *value = atoi((char *)key);
        xmlFree(key);
      }
      current = current->next;
    }
  }

#if defined(HAVE_MPI)
  MPI_Bcast(value, 1, MPI_INTEGER, 0, MPI_COMM_WORLD);
#endif
  return;
}


void parse_string (char * keyword, char ** value) {
  xmlChar *key;
  xmlNodePtr current;
  int len;

  len = 0;
  if(myid == 0){
    current = cur->xmlChildrenNode;
    while (current != NULL) {
      if ((!xmlStrcmp(current->name, (const xmlChar *)keyword))) {
        key = xmlNodeListGetString(doc, current->xmlChildrenNode, 1);
        /* For some reason, withouth the "+1", the following line may lead to a buffer overrun when doing the strcpy */
        len = (int)strlen((char *)key)+1;
        *value = malloc(len);
        strcpy(*value, (char *)key);
        xmlFree(key);
      }
      current = current->next;
    }
  }

#if defined(HAVE_MPI)
  MPI_Bcast(&len, 1, MPI_INTEGER, 0, MPI_COMM_WORLD);
  if(len > 0){
    if(myid != 0) *value = malloc(len);
    MPI_Bcast(*value, len * sizeof(char), MPI_CHAR, 0, MPI_COMM_WORLD);
  }
#endif
  return;
}


void readinput_init(){
  if(myid == 0) {

  doc = xmlParseFile("input");
  if (doc == NULL ) {
    fprintf(stderr,"input file not parsed successfully. \n");
#if defined(HAVE_MPI)
    MPI_Finalize();
#endif
    exit(0);
  }
  cur = xmlDocGetRootElement(doc);

  if (cur == NULL) {
    fprintf(stderr,"input file is empty\n");
    xmlFreeDoc(doc);
#if defined(HAVE_MPI)
    MPI_Finalize();
#endif
    exit(0);
  }

}
}


void readinput_end(){if(myid == 0) xmlFreeDoc(doc);}
