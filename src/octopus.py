#!/usr/bin/env python
# -*- coding: utf-8 -*- 
# Copyright 2015, Adrián Gómez Pueyo and Alberto Castro

# This file is part of maxdft.

# maxdft is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# maxdft is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.

# You should have received a copy of the GNU General Public License
# along with maxdft.  If not, see <http://www.gnu.org/licenses/>.

import sys
import os
import signal


def run_octopus(octopus_command, output):
    """The function that runs octopus

    This function recieves both the command to call octopus
    octopus and the desired directory where the output will
    be placed output, and then runs octopus with whatever
    input file inp is in the current working directory.
    """

    if output is not None:
        system_command = octopus_command + " > " + output + " 2>&1 "
        #print "    Running octopus. Output to " + output + "."
    else:
        system_command = octopus_command

    sys.stdout.flush()

    status = os.system(system_command)

    # The following code allows the runs to be interrumpted with
    # SIGINT or SIGQUI#T (Control-C, typically)

    if (os.WIFSIGNALED(status) and
        (os.WTERMSIG(status) == signal.SIGINT or os.WTERMSIG(status)
        == signal.SIGQUIT)):
        sys.exit(0)
