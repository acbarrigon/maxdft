/*

Copyright 2015, Adrián Gómez Pueyo and Alberto Castro

This file is part of maxdft.

maxdft is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

maxdft is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
maxdft.  If not, see <http://www.gnu.org/licenses/>.

*/


#ifndef OCTOPUS_H
#define OCTOPUS_H


/* Octopus call and output */
char *octopus;

/* File to record octopus runs. */
FILE *octopus_record_file;
/* Counter, to keep track of how many times octopus was called. */
int octopus_runs_counter;

int octopus_init(int argc, char* argv[]);
void octopus_end();
void run_octopus(char *octopus_command, char *output, int scf_check_convergence, int sternheimer_check_convergence);
int octopus_check_convergence(char *output_file);
int octopus_check_sternheimer_convergence(char *output_file);
int search_in_file(char *fname, char *str);


#endif
