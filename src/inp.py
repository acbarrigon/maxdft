#!/usr/bin/env python
# -*- coding: utf-8 -*- 
# Copyright 2015, Adrián Gómez Pueyo and Alberto Castro

# This file is part of maxdft.

# maxdft is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# maxdft is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.

# You should have received a copy of the GNU General Public License
# along with maxdft.  If not, see <http://www.gnu.org/licenses/>.

import math

def sch(l, step, alpha, lmin, lmax, delta, npotex,
        npar, poten_selector, gamma):
    """Creation of the inp file for the SCH density calculation

    In this function the parameters defining the external
    potential, IPF and simulation box are used to create the inp file
    for the computation of the electronic density calculated using
    the Schrodinger equation.
    """

    finp = open('inp', 'w')
    #if(inpstring_SCH != NULL) fprintf(Finp, "\n%s\n", inpstring_SCH);

    sep = lmin + delta*npotex


    # Calculation mode and units used
    finp.write("ExperimentalFeatures = yes\n")
    finp.write("CalculationMode = gs\n")
    finp.write("NTypeParticleModelMB = 1\n")
    finp.write("NParticleModelMB = 2\n")
    finp.write("NDimModelMB = 1\n")
    finp.write("Dimensions = 2\n")
    finp.write("%DescribeParticlesModelMB\n")
    finp.write(" \"electron\" | 1 | 1. | 1. | fermion\n")
    finp.write(" \"electron\" | 1 | 1. | 1. | fermion\n")
    finp.write("%\n")
    finp.write("%DensitytoCalc\n")
    finp.write("\"electron\" | 1 | 0\n")
    finp.write("%\n")
    finp.write("FromScratch = yes\n")
    finp.write("TheoryLevel = independent_particles\n\n")

    # Spatial parameters
    line = "BoxShape = parallelepiped\nlsize = " + str(l)\
    + "\nSpacing = " + str(step) + "\n\n"
    finp.write(line)

    finp.write("LCAOStart = lcao_none\n\n")

    finp.write("%Species\n")
    line = "\"elec\"| species_usdefspecies " "| " + str(poten_selector)\
    + " | 1 "
    finp.write(line)

    if poten_selector != 1:
        for i in range(npar):
            line = " | "+ str(gamma[i])
            finp.write(line)
        finp.write(" | 1")
        #for i in nextrapars:
        #    finp.write(" | %.12g ", gsl_vector_get(extrapars, i))
    else:
        for i in range(npar-1):
            line = " | " + str(gamma[i])
            finp.write(line)
        line = " | " + str(math.fabs(gamma[npar-1]))
        finp.write(line)
        finp.write(" | 1")
        #for i in nextrapars:
        #    finp.write(" | %.12g ", gsl_vector_get(ipf.extrapars, i))

    finp.write(" \n")
    line = "\"Hydrogen\" | species_usdefspecies | -1 | 0 | "\
    + str(alpha) +" | " + str(0.0) + "\n"
    finp.write(line)
    finp.write("%\n\n")

    # Coordinates
    finp.write("%Coordinates\n")
    finp.write("\"elec\" | 0 | 0 | \n")
    line = "\"Hydrogen\" | " + str(sep/2.0) + " | " + str(sep/2.0) + "\n"
    finp.write(line)
    line = "\"Hydrogen\" |  " + str(-sep/2.0) + " | " + str(-sep/2.0) + "\n"
    finp.write(line)
    finp.write("%\n")

    #Eigensolver iteration controller
    finp.write("EigenSolverMaxIter = 200\n\n")

    finp.close()
