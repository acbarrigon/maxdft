#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright 2015, Adrián Gómez Pueyo and Alberto Castro

# This file is part of maxdft.

# maxdft is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# maxdft is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.

# You should have received a copy of the GNU General Public License
# along with maxdft.  If not, see <http://www.gnu.org/licenses/>.

def obj_fun(nsch, ndft, step):
    """This function computes the objective function G[gamma]

    We integrate numerically the squared difference between
    the reference electronic density (ndft) and the electronic
    density computed using the Schrodinger equation (ndft) with
    our trial IPFs. The integration is carried over the whole
    simulation box that octopus uses for its calculations.
    """

    ad, length = ndft.buffer_info()
    a = 0.5 * step * (nsch[0] - ndft[0])**2
    for i in range(1, length-1):
        a += step * (nsch[i] - ndft[i])**2
    a += 0.5 * step * (nsch[length-1] - ndft[length-1])**2

    return a
