var menudata={children:[
{text:'Main Page',url:'index.html'},
{text:'Modules',url:'namespaces.html',children:[
{text:'Modules List',url:'namespaces.html'},
{text:'Module Members',url:'namespacemembers.html',children:[
{text:'All',url:'namespacemembers.html'},
{text:'Functions/Subroutines',url:'namespacemembers_func.html'},
{text:'Variables',url:'namespacemembers_vars.html'}]}]},
{text:'Data Types List',url:'annotated.html',children:[
{text:'Data Types List',url:'annotated.html'},
{text:'Data Types',url:'classes.html'},
{text:'Data Fields',url:'functions.html',children:[
{text:'All',url:'functions.html'},
{text:'Variables',url:'functions_vars.html'}]}]}]}
