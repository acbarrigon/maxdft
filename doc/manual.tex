\documentclass[twoside,a4paper]{refart}
\usepackage{makeidx}
\usepackage{amsfonts}
\usepackage{ifthen}

% ifthen wird vom Bild von N.Beebe gebraucht!

\def\bs{\char'134 } % backslash in \tt font.
\newcommand{\ie}{i.\,e.,}
\newcommand{\eg}{e.\,g..}
\DeclareRobustCommand\cs[1]{\texttt{\char`\\#1}}
\newcommand{\maxdft}{{\tt maxdft} }

\title{\maxdft}
\author{Adri{\'{a}}n G{\'{o}}mez Pueyo \\
Alberto Castro \\
}

\date{}
\emergencystretch1em  %

\pagestyle{myfootings}
\markboth{\maxdft manual}%
         {\maxdft manual}

\makeindex 

\setcounter{tocdepth}{2}

\begin{document}

\maketitle

\begin{abstract}
        This document describes the \maxdft program.
\end{abstract}


\maxdft is a program for basic research about density-functional theory~\cite{Hohenberg1964}.


\tableofcontents

\newpage


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Basic theory}

\subsection{Electron-electron interaction potential functionals, and exchange
  and correlation functionals}

We consider $N$-fermion non-relativistic systems, characterized by
Hamiltonians in the form:
\begin{equation}
\hat{H} = \hat{T} + \hat{V} + \hat{W}\,,
\end{equation}
where:
\begin{equation}
\hat{T} = \sum_{i=1}^N \frac{1}{2}\hat{\vec{p}}^2
\end{equation}
is the kinetic energy term, a one-body operator that depends on the electronic
momenta $\hat{\vec{p}}_i$;
\begin{equation}
\hat{V} = \sum_{i=1}^N v(\hat{\vec{r}}_i)
\end{equation}
is the external potential operator, also a one-body operator, in this case a
\emph{local} one, meaning that only depends on the electronic position
operators $\hat{\vec{r}}_i$; and
\begin{equation}
\hat{W} = \sum_{i<j} w(\vert \hat{\vec{r}}_i - \hat{\vec{r}}_j \vert )
\end{equation}
is the electron interaction potential, a two-body operator whose precise
expression is determined by the the function $w$, which has for electrons in
vacuum a Coulomb form, i.e.:
\begin{equation}
w(r) = \frac{1}{r}\,.
\end{equation}
However, one may consider different forms for the interaction, for example a
Yukawa potential:
\begin{equation}
w(r) = \frac{e^{-\alpha r}}{r}\,.
\end{equation}
\index{IPFs}\marginlabel{IPFs:} The basic theorems of Density Functional Theory (DFT) do not prescribe a
particular form for the interaction potential function (IPF) $w$, and therefore there are in
principle a wide set of \emph{admissible} interaction potential functions (IPFs) over which DFT can be
constructed. We will represent all these potentials on a set $\Gamma$, such
that to each $\gamma \in \Gamma$ corresponds an IPF
$w^\gamma$ and viceversa.





\index{XCF}\marginlabel{XCF:}
If we choose one IPF $\gamma$, we may apply DFT
theorems, and will arrive to the definition of an \emph{exchange and correlation
energy functional} (XCF). Therefore, the consideration of all possible interaction
potentials represented in $\Gamma$ leads to a set of possible exchange and
correlation energy functionals, that we can also represent on a set $\Lambda$,
such that to each $\lambda \in \Lambda$ corresponds one xc energy
functional $E_{\rm xc}^\lambda$, and
viceversa.

The previous discussion has in fact defined a map:
\begin{eqnarray}
l:\Gamma & \longrightarrow & \Lambda
\\\nonumber
  \gamma & \longrightarrow & l(\gamma) = \lambda
\end{eqnarray}
I.e.: once we choose an IPF $w^\gamma$, it leads us to one
XCF $E_{\rm xc}^{l(\gamma)}$. We are concerned here with the
opposite question: given an xc energy functional $E_{\rm xc}^\lambda$, can we
find the correspoding electron-electron interaction $w^{\gamma_0}$, so that
the xc energy functional induced by $w^{\gamma_0}$ is precisely $E_{\rm
  xc}^\lambda$?


In other words, we are looking for at least one member of the set
$l^{-1}(\lambda)$. By definition, $l^{-1}(\lambda)$ is not empty: every $\lambda \in \Lambda$
corresponds at least to one IPF in $\Gamma$. However, it may
very well be that $l^{-1}(\lambda)$ is not one unique $\gamma_0$: various
different interacton potentials could lead to the same xc energy functional.

\begin{itemize}

\item For a given interaction potential $\gamma$, and external potential $v$, 
$n_\gamma[v]$ is the ground state density (we will assume only non-degenerate
ground states). 

\item For a given xc energy functional $\lambda$, and external potential $v$,
$n^{\rm DFT}_\lambda$ is the ground state density that can be obtained by
solving the corresponding Kohn-Sham equations.

\end{itemize}

If $\gamma_0 \in l^{-1}(\lambda)$, both densities coincide:
\begin{equation}
n_{\gamma_0}[v] = n_\lambda^{\rm DFT}[v]\,.
\end{equation}
for any choice of the external potential $v$. This is also a sufficient
condition: if those densities coincide for any choice of external potential
$v$, then $\gamma_0 \in l^{-1}(\lambda)$.

We can transform the previous equation into a minimization problem: we define,
for any $\gamma,\lambda$ and external potential $v$:
\begin{equation}
G[\gamma;\lambda,v] = \int\!\!{\rm d}^3r\;(n_\gamma[v](\vec{r}) - n^{\rm DFT}_\lambda[v](\vec{r}))^2\,.
\end{equation}
Then, if $\gamma_0 \in l^{-1}(\lambda)$:
\begin{equation}
G[\gamma_0;\lambda,v] = 0 = \min_{\gamma \in \Gamma} G[\gamma;\lambda,v]
\end{equation}

Therefore, one may think that $\gamma_0$ could be computed by minimization of
$G[\gamma;\lambda,v]$ over $\Gamma$. However, this property should hold for
any external potential $v$. Indeed, it may happen that the set of $\gamma$
points that nullify $G[\gamma;\lambda,v]$ for one particular $v$ includes
points that are not in $l^{-1}(\lambda)$. In other words, there may be
interaction potentials which, \emph{for the particular external potential}
$v$, leads to a density that equals the one produced by the xc energy
functional $E_{\rm xc}^\lambda$, but which may not behave in the same way for
a different external potential. The key characteristic of a $\gamma_0 \in
l^{-1}(\lambda)$ is the fact that it leads to the ground state density
prescribed by the xc functional $\lambda$
\emph{for any external potential} $v$.

We cannot ensure the simultaneous minimization of the functional $G$ for all
possible interaction potential $v$. However, if the we ensure that
minimization for a sufficiently ``significant'' collection of external
potentials $\lbrace v_k\rbrace$, the obtained xc energy functional will
probably not differ much from the exact one. In order to achieve this goal,
one may define a new functional:
\begin{equation}
\tilde{G}[\gamma;\lambda,\lbrace v_k\rbrace] = \sum_k G[\gamma;\lambda,v_k]\,.
\end{equation}
If we minimize $\tilde{G}$ so that we find a $\gamma_0$ that makes it null:
\begin{equation}
\tilde{G}[\gamma_0;\lambda,\lbrace v_k\rbrace] = 0\,,
\end{equation}
then all
$G[\gamma_0;\lambda,v_k]=0$.

\subsection{\label{subsec:numproc} Numerical procedure}

Suppose we are given a particular xc energy functional, for example LDA. The
first question that one could think of is: is there any electron-electron
interaction potential for which LDA is exact? We have no theoretical answer
for this. In other words, there may be no $\lambda \in \Lambda$ for that
particular xc energy functional. In this case, our objective would be to
obtain the best possible electron-electron interaction potential.

Next, we must define our search space for the electron-electron interaction
potential. In the previous section, we have identified $\Gamma$ with the set of
\emph{admissible} functions $w$, i.e. those ones for which the DFT theorems
can be applied with no difficulties. Now, we will adopt a pragmatic approach,
and we will consider a set of electron-electron interaction functions,
parameterised by a set $\gamma_1,\dots,\gamma_N \equiv \gamma$, so that the set
$\Gamma$ will in fact be the parameter set. We will hope that a judicious
election of the parameterisation will ensure that the seeked function will be
contained in the search space.

We confront therefore the problem of minimizing the functional:
\begin{equation}
\tilde{G}[\gamma;\lambda,\lbrace v_k\rbrace_{k=1}^m] = \sum_k^m G[\gamma;\lambda,v_k]
= \sum_k^m \int\!\!{\rm d}^3r\;(n_\gamma[v](\vec{r}) - n^{\rm DFT}_\lambda[v](\vec{r}))^2\,.
\end{equation}
for some collection of possible external potentials $\lbrace
v_k\rbrace_{k=1}^m$. Note that we use $\lambda$ to denote the exchange and
correlation potential functional of choice, even if we do not know if it
is the one that corresponds to any possible electron-electron interaction
functional.

The computation of $G[\gamma;\lambda,v_k]$ must be done by 
\begin{itemize}
\item computing $n^{\rm DFT}_\lambda[v](\vec{r})$, which entails solving the
  Kohn-Sham equations for the external potential $v$ and the xc potential
  derived from the xc energy functional $\lambda$; and

\item computing $n_\gamma[v](\vec{r})$, which entails solving
  Schr{\"{o}}dinger's equation for the external potential $v$ and the
electron-electron interaction potential $\gamma$.

\end{itemize}

There are many minimization algorithms that have been proposed over the
years. A wide class of them only require of a means of computing the value of
the function to be minimized (\emph{gradient-free algorithms}; another class
also require the gradient of the function (\emph{gradient-based
  algorithms}). If we choose one gradient-free algorithm, in fact no more
theory is required; if we choose one gradient-based algorithm, we need of a
means to compute the gradient of $G$ with respect to the $\gamma$
parameters. This could be worked out by making use of perturbation
theory.

To ease the notation, let us use hereafter: $\tilde{G}(\gamma) =
\tilde{G}[\gamma;\lambda,\lbrace v_k\rbrace_{k=1}^m]$. The gradient is given by:
\begin{equation}
\nonumber
\frac{\partial \tilde{G}}{\partial \gamma_k}(\gamma) 
 = 2 \sum_k^m \int\!\!{\rm d}^3r\;(n_\gamma[v_k](\vec{r}) - n^{\rm
  DFT}_\lambda[v_k](\vec{r}))
\frac{\partial n_\gamma[v_k](\vec{r})}{\partial \gamma_k}\,.
\end{equation}
We need the derivatives of the density with respect to the parameters
$\gamma$. Let us restrict the discussion to two-electron systems. The density
is given, in terms of the wave function, by:
\begin{equation}
n_\gamma[v_k](\vec{r}) = 2\int\!\!{\rm d}^3r'\;\vert \varphi_\gamma[v_k](\vec{r},\vec{r}')\vert^2\,,
\end{equation}
and therefore, assuming a real ground state wave function:
\begin{equation}
\frac{\partial n_\gamma[v_k](\vec{r})}{\partial \gamma_k} = 
4 \int\!\!{\rm d}^3r'\; \varphi_\gamma[v_k](\vec{r},\vec{r}')
\frac{\partial \varphi_\gamma[v_k](\vec{r},\vec{r}')}{\partial \gamma_k}
\end{equation}
It all boils down to the computation of the derivatives of the wave function,
i.e.:
\begin{equation}
\varphi_{\gamma + he_k} = \varphi_\gamma + h \frac{\partial
  \varphi_\gamma}{\partial \gamma_k} + \mathcal{O}(h^2)\,,
\end{equation}
where $e_k$ is the $k$-th unit vector. To obtain them, we may use
Sternheimer's formulation of perturbation theory, in the following manner:

Let us call $\hat{W}_\gamma$ and $\hat{H}_\gamma$ the electron-electron
interaction and Hamiltonian defined by $\gamma$:
\begin{equation}
\hat{H}_\gamma = \hat{T} + \hat{V} + \hat{W}_\gamma\,.
\end{equation}
The required wave functions and ground state energies are then given by:
\begin{eqnarray}
\hat{H}_{\gamma}\varphi_{\gamma} & = & \varepsilon_{\gamma}\varphi_{\gamma}\,,
\\
\hat{H}_{\gamma + he_k}\varphi_{\gamma + he_k} & = & \varepsilon_{\gamma +
  he_k}\varphi_{\gamma + he_k}\,,
\end{eqnarray}
To first order in $h$:
\begin{eqnarray}
\hat{H}_{\gamma + he_k} & = & \hat{H}_\gamma + h\frac{\partial
  \hat{W}_\gamma}{\partial \gamma_k}\,,
\\
\varphi_{\gamma + he_k} & = & \varphi_\gamma + h\frac{\partial
  \varphi_\gamma}{\partial \gamma_k}\,,
\\
\varepsilon_{\gamma + he_k} & = & \varepsilon_\gamma + h\frac{\partial
  \varepsilon_\gamma}{\partial \gamma_k}\,,
\end{eqnarray}
and therefore:
\begin{equation}
\left[\hat{H}_\gamma + h\frac{\partial \hat{W}_\gamma}{\partial
    \gamma_k}\right]\left[\varphi_\gamma + h\frac{\partial
    \varphi_\gamma}{\partial \gamma_k}\right]  = 
\left[\varepsilon_\gamma + h\frac{\partial
    \varepsilon_\gamma}{\partial \gamma_k}\right]
\left[\varphi_\gamma + h\frac{\partial
    \varphi_\gamma}{\partial \gamma_k}\right]
\end{equation}
Keeping only terms to second
order in $h$:
\begin{equation}
\left[ \hat{H}_\gamma - \varepsilon_\gamma \right]
\vert\frac{\partial
    \varphi_\gamma}{\partial \gamma_k}\rangle = - \hat{P}_0^\perp 
\frac{\partial \hat{W}_\gamma}{\partial
    \gamma_k}\vert\frac{\partial
    \varphi_\gamma}{\partial \gamma_k}\rangle\,,
\end{equation}
where $\hat{P}_0^\perp$ is the projection onto the orthogonal subspace of the
ground state $\varphi_\gamma$, which we consider to be non-degenerate:
\begin{equation}
\hat{P}_0^\perp = \hat{I} - \vert\varphi_\gamma\rangle\langle\varphi_\gamma\vert\,.
\end{equation}
This is a linear equation that provides the solution wave function
derivative. Given that this solution belongs to the orthogonal subspace
projected by $\hat{P}_0^\perp$, it may also be written as:\begin{equation}
\hat{P}_0^\perp\left[ \hat{H}_\gamma - \varepsilon_\gamma \right]\hat{P}_0^\perp
\vert\frac{\partial
    \varphi_\gamma}{\partial \gamma_k}\rangle = - \hat{P}_0^\perp 
\frac{\partial \hat{W}_\gamma}{\partial
    \gamma_k}\vert\frac{\partial
    \varphi_\gamma}{\partial \gamma_k}\rangle\,,
\end{equation}
which is a form that avoids numerical difficulties.


\subsection{Basic run modes}
\maxdft has three different run modes:

\subsubsection{{\bf RUNMODE\_CHECK\_GRAD} (0)}
  In this run mode \maxdft will perform the basic computations for the
  electronic densities via the Konh-Sham equations and the Schrödinger
  equation, and then it will compute and return the gradient with respect to
  the parameters of the density we are minimizing.

\subsubsection{{\bf RUNMODE\_MINIMIZE\_IPF} (1)}
  In this run mode \maxdft will look for the   IPF in the Schrödinger equation
  that generates an electronic density as close as possible to the reference
  density computed using the Kohn-Sham equations for a determined XC
  functional. In order to do this, we have to choose a parametrization for the
  IPF (the avaible parametrizations are explained in the \emph{usdefspecies.h}
  file, and succinctly in the \emph{input} file) and the minimization method,
  i. e., wether we will be using a gradient based or a gradient-free
  minimization method.

  For the gradient-based methods we use the BFGS algorithm provided by the
  \textsf{GSL}~\cite{GSL} library, and to compute the gradient of the
  functional to minimize we have two different methods: a numerical
  computation of the gradient using the \textsf{GSL} library ({\bf
    GRADIENT\_PRECISE}) and the perturbative approximation using the
  Sternheimer method ({\bf GRADIENT\_STERNHEIMER}) that has been explained
  in section~\ref{subsec:numproc}.

  For the gradient-free minimization methods, we have two options: use the
  SIMPLEX algorithm provided by the \textsf{GSL} library ({\bf SIMPLEX}) or
  use a Genetic Algorithm (GA) provided by the \textsf{pyevolve} library ({\bf
    GENETIC\_ALGORITHM}).

\subsubsection{{\bf RUNMODE\_MINIMIZE\_EXC} (2)}
  In this run mode \maxdft will look for the XC functional that, via the
  Kohn-Sham equations, results in an electronic density for the defined system
  that is as close as possible to the reference density computed via the
  Schrödinger equation.


\subsection{\dots}
%\index{rules}


\attention
\emph{This is reserved for specially relevant paragraphs}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Compilation}
\label{section:compilation}

\subsection{The {\tt usdefspecies} library}



\subsection{Patching the {\tt octopus} program}




\subsection{The testsuite}


\printindex

\bibliographystyle{abbrv}
\bibliography{manual.bib}

\end{document}
