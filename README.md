# maxdft

Experiments on density-functional theory, a method to tackle the quantum
many-electron problem.

## Directories

<b>TAD</b> => TAD document (tex, figures, pdf, etc.)<br />
<b>TFM</b> => TFM document (tex, figures, pdf, etc.)<br />
<b>doc</b> => papers, notes, etc.<br />
<b>octopus-patch</b> => patch necessary in order to run maxdft for the octopus
software.<br />
<b>results</b> => preliminary results.<br />
<b>sample</b> => sample for maxdft's input file.<br />
<b>src-tad</b> => sources of the code written for the TAD.<br />
<b>src</b> => sources of the code written for the TFM.<br />
<b>testsuite</b> => set of tests for maxdft.

