#include <stdio.h>
#include <stdlib.h>
#include <gsl/gsl_multimin.h>
#include <math.h>

//Definimos los parametros del programa
#define l 15  //Tamaño de la lattice
#define npar 10  //Numero de parametros de la aproximacion
#define step 0.2  //Graduacion de la grid
#define N (int) (l/step)*2+1  //Tamaño de los vectores de almacenamiento
#define alpha 1.0  //Interaccion de soft-Coulomb
#define beta 1.0  //Interaccion entre electrones
#define L 5.0  //Distancia entre nucleos


void inpSCH(double *gamma);  //Crea el input de Octopus para Schrodinger
void inpDFT(void);  //Crea el input de Octopus para DFT
void leeSCH(double *nSCH);  //Lee los datos para la densidad de Schrodinger
void leeDFT(double *nDFT);  //Lee los datos para la densidad DFT
double integral(double *nSCH, double *nDFT);  //Calculamos la integral a minimizar
double my_f (const gsl_vector *params, void *nSCH);  //Funcion a minimizar
void poten(const gsl_vector *a);  //Escribe el potencial en un archivo

FILE *Finp;
FILE *Fdata;

int main(void)
{

  int i;
  double nDFT[N];

  inpDFT();  //Sacamos el archivo para la densidad de Schrodinger
  system("./octopus");  //Calculamos la densidad de Schrodinger

  //Lectura de los datos y almacenamiento para el calculo exacto
  leeDFT(nDFT);


  //Comenzamos el proceso variacional
  //Inicializamos el algoritmo de minimizacion multidimensional nsimplex
  const gsl_multimin_fminimizer_type *T = gsl_multimin_fminimizer_nmsimplex;
  gsl_multimin_fminimizer *s = NULL;
  gsl_vector *ss, *x;
  gsl_multimin_function my_func;

  size_t iter = 0;
  int status;
  double size;

  //Inicializamos la funcion
  x = gsl_vector_alloc (npar);
  for(i=0;i<npar;i++) gsl_vector_set (x, i, 0.5);

  // Hacemos los pasos de tamaño 0.1
  ss = gsl_vector_alloc (npar);
  gsl_vector_set_all (ss, 0.1);

  // Inicializamos el metodo e iteramos
  my_func.n = npar;
  my_func.f = my_f;
  my_func.params = (void *)nDFT;

  s = gsl_multimin_fminimizer_alloc (T, npar);
  gsl_multimin_fminimizer_set (s, &my_func, x, ss);

  do{
      iter++;
      status = gsl_multimin_fminimizer_iterate(s);

      if (status) 
        break;

      size = gsl_multimin_fminimizer_size (s);
      status = gsl_multimin_test_size (size, 1e-2);

      if (status == GSL_SUCCESS)
        {
          printf ("Ha convergido a un minimo en ");
        }

      printf ("%5zu iteraciones\n",iter);
      for(i=0;i<npar;i++)  printf ("%10.3e ", gsl_vector_get (s->x, i));
      printf("\nf() = %7.3f size = %.3f\n",s->fval, size);
  }
  while(iter < 1000 && status == GSL_CONTINUE);

  //double gamma[npar];

  //for(i=0;i<npar;i++)  gamma[i]=gsl_vector_get (s->x,i);
 
  poten(s->x);

  gsl_vector_free(ss);
  gsl_multimin_fminimizer_free (s);

  return status;
}


void leeSCH(double *n)
{

  int i;
  double tirar;

  if((Fdata=fopen("static/modelmb/density_ip001_imb01","rt"))==NULL){
    printf("Error al abrir un fichero");
    exit(0);
  }

  for(i=0;i<N;i++)  fscanf(Fdata," %lf  %lf\n", &tirar, &n[i]);

  fclose(Fdata);
}


void leeDFT(double *n)
{

  char tirar[8];
  int i;
  double a;

  if((Fdata=fopen("static/density.y=0,z=0","rt"))==NULL){
    printf("Error al abrir un fichero");
    exit(0);
  }

  fscanf(Fdata,"%s         %s                     %s                     %s\n",tirar,tirar,tirar,tirar);
  for(i=0;i<N;i++)  fscanf(Fdata," %lf  %lf  %lf\n", &a, &n[i], &a);

  fclose(Fdata);
}

void inpSCH(double *gamma)
{

  int i;

  if((Finp=fopen("inp","wt"))==NULL){
    printf("Error al abrir un fichero");
    exit(1);
    }

  //Modo de calculo y unidades utilizadas
  fprintf(Finp,"ExperimentalFeatures = yes\n\nCalculationMode = gs\n\nNTypeParticleModelMB = 1\nNParticleModelMB = 2\nNDimModelMB = 1\n\nDimensions = 2\n\n%%DescribeParticlesModelMB\n \"electron\" | 1 | 1. | 1. | fermion\n \"electron\" | 1 | 1. | 1. | fermion\n\%%\n\n%%DensitytoCalc\n \"electron\" | 1 | 0\n%%\n\nFromScratch = yes\n\nTheoryLevel = independent_particles\n\n");

  //Parametros del espacio
  fprintf(Finp,"BoxShape = parallelepiped\nlsize = %d\nSpacing = %f\n\n", l, step);

  //Output
  fprintf(Finp,"Output = mmb + mmb_den\nOutputHow = axis_x\n\n");

  //Parametros del potencial
  fprintf(Finp,"alpha = %f\n",alpha);  //Soft Coulomb
  for(i=0;i<npar;i++)  fprintf(Finp,"gamma%d = %lf\n",i, gamma[i]);  //Interaccion entre electrones
  fprintf(Finp,"L = %f\n\n",L);  //Distancia entre los nucleos

  //Definicion de los electrones
  fprintf(Finp,"%%Species\n\"elec\"| 2 | spec_user_defined | 1 | \"");
  fprintf(Finp,"(gamma0");
  for(i=1;i<npar-1;i++)  fprintf(Finp,"+gamma%d*(x-y)^%d",i, i);
  fprintf(Finp,")*e^(-gamma%d*(x-y)^2)",npar-1);
  fprintf(Finp,"/(alpha+(x-y)^2)^(1/2) -1/sqrt(alpha+(x-L/2)^2) -1/sqrt(alpha+(x+L/2)^2) -1/sqrt(alpha+(y-L/2)^2) -1/sqrt(alpha+(y+L/2)^2)\"\n%%\n\n");

  //Coordenadas 
  fprintf(Finp,"%%Coordinates\n \"elec\" | 0 | 0 | 0\n%%\n\n");

  //Control de las iteraciones del solucionador de autovalores
  fprintf(Finp,"EigenSolverMaxIter = 200");

  fclose(Finp);
}


void inpDFT(void)
{

  if((Finp=fopen("inp","wt"))==NULL){
    printf("Error al abrir un fichero");
    exit(1);
    }

  //Modo de calculo y unidades utilizadas
  fprintf(Finp,"CalculationMode = gs\n\nDimensions = 1\nFromScratch = yes\n\n");

  //Parametros del espacio
  fprintf(Finp,"BoxShape = parallelepiped\nlsize = %d\nSpacing = %f\n\n", l, step);

  //Output
  fprintf(Finp,"Output = wfs_sqmod + density + potential\nOutputHow = axis_x\n\n");

  //Parametros del potencial
  fprintf(Finp,"L = %f\nalpha = %f", L, alpha);

  //Definicion de los electrones
  fprintf(Finp,"\n\n%%Species\n\"elec\" | 2 | spec_user_defined | 2 | \"-1/sqrt(alpha+(x-L/2)^2) -1/sqrt(alpha+(x+L/2)^2)\"\n%%\n\n");

  //Coordenadas 
  fprintf(Finp,"%%Coordinates\n\"elec\" | 0\n%%\n");

  fclose(Finp);
}


double integral(double *nSCH, double *nDFT)
{

  int i;
  double a = 0;

  a += 0.5*step*(nSCH[0]-nDFT[0])*(nSCH[0]-nDFT[0]);
  for(i=1;i<N-1;i++)  a += step*(nSCH[i]-nDFT[i])*(nSCH[i]-nDFT[i]);
  a += 0.5*step*(nSCH[N-1]-nDFT[N-1])*(nSCH[N-1]-nDFT[N-1]);

  return(a);
}


double my_f (const gsl_vector *params, void *nDFT)
{

  int i;
  double nSCH[N], var[npar];

  for(i=0;i<npar;i++)  var[i] = gsl_vector_get (params, i);

  inpSCH(var);  //Sacamos el archivo para la densidad DFT en primer lugar
  system("./octopus");  //Calculamos la densidad DFT

  //Lectura de los datos y almacenamiento para el calculo DFT
  leeSCH(nSCH);

  return integral(nSCH,(double *)nDFT);
}

void poten(const gsl_vector *a)
{

  int i,j;
  double r,poten;

  if((Fdata=fopen("pot","wt"))==NULL){
    printf("Error al abrir un fichero");
    exit(1);
  }

  for(i=0;i<751;i++){
    r=(double) i/50.0;
    poten=gsl_vector_get(a,0);
    for(j=1;j<npar-1;j++)  poten+=gsl_vector_get(a,j)*pow(r,j);
    poten*=exp(-gsl_vector_get(a,npar-1)*r*r);
    poten/=sqrt(1+r*r);
    fprintf(Fdata,"%lf\t%lf\n",r,poten);
  }

  fclose(Fdata);
}
